-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lifestylemonthlydifferencereport`
--

DROP TABLE IF EXISTS `lifestylemonthlydifferencereport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `lifestylemonthlydifferencereport` (
  `locationID` text,
  `employeeID` text,
  `employeeName` text,
  `previousScore` double DEFAULT NULL,
  `currentScore` double DEFAULT NULL,
  `percentageChange` double DEFAULT NULL,
  `branchID` varchar(45) DEFAULT NULL,
  `projectID` varchar(45) DEFAULT NULL,
  `teamID` varchar(45) DEFAULT NULL,
  `groupPathID` varchar(45) DEFAULT NULL,
  `testDate` varchar(45) DEFAULT NULL,
  `clientID` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lifestylemonthlydifferencereport`
--

LOCK TABLES `lifestylemonthlydifferencereport` WRITE;
/*!40000 ALTER TABLE `lifestylemonthlydifferencereport` DISABLE KEYS */;
INSERT INTO `lifestylemonthlydifferencereport` VALUES ('553471','Emp1','Pramodh',7.6,7.6,0,'M742187','B1744','AT77S','IF51J','2019-12-10','AD163P'),('553471','Emp2','Rajesh',5.6,5.6,0,'M742187','B1744','AT77S','IF51J','2019-12-10','AD163P'),('553471','Emp3','Puneeth',6.6,6.6,0,'M742187','B1744','AT77S','IF51J','2019-12-10','AD163P'),('553471','Emp4','Chadan',8.6,8.6,0,'M742187','B1744','AT77S','IF51J','2019-12-10','AD163P'),('553471','Emp5','Shivaraj',10.6,10.6,0,'M742187','B1744','AT77S','IF51J','2019-12-10','AD163P'),('553471','Emp6','Kiran',9.6,9.6,0,'M742187','B1744','AT77S','IF51J','2019-12-10','AD163P'),('553471','Emp7','Siddu',4.8,4.9,0.1,'M742187','B1744','AT77S','IF51J','2019-12-10','AD163P'),('553471','Emp8','Suresh',6.9,6.9,0,'M742187','B1744','AT77S','IF51J','2019-12-10','AD163P'),('553471','Emp9','Anil',7.9,7.9,0,'M742187','B1744','AT77S','IF51J','2019-12-10','AD163P'),('553471','Emp10','Ravi',10.9,10.9,0,'M742187','B1744','AT77S','IF51J','2019-12-10','AD163P'),('553471','Emp11','Aravind',11,11.1,0.1,'M742187','B1744','AT97I','ASP32K','2019-12-11','AD163P'),('553471','Emp12','Hari ',5,5.5,0.5,'M742187','B1744','AT97I','ASP32K','2019-12-11','AD163P'),('553471','Emp13','Sachin',5.5,5.6,0.2,'M742187','B1744','AT97I','ASP32K','2019-12-11','AD163P'),('553471','Emp14','Arun',6.2,6.3,0.1,'M742187','B1744','AT97I','ASP32K','2019-12-11','AD163P'),('553471','Emp15','Aditya',4.8,4.9,0.1,'M742187','B1744','AT97I','ASP32K','2019-12-11','AD163P'),('553471','Emp16','Santhosh',4.9,5.1,0.2,'M742187','B1744','AT97I','ASP32K','2019-12-11','AD163P'),('553471','Emp17','Vinod',3.9,4.2,0.3,'M742187','B1744','AT97I','ASP32K','2019-12-11','AD163P'),('553471','Emp18','Laxmikanth',5.2,5.4,0.2,'M742187','B1744','AT97I','ASP32K','2019-12-11','AD163P'),('553471','Emp19','Vijay',6.5,5.7,0.2,'M742187','B1744','AT97I','ASP32K','2019-12-11','AD163P'),('553471','Emp20','Karthik',7.5,7.5,0,'M742187','B1744','AT97I','ASP32K','2019-12-11','AD163P');
/*!40000 ALTER TABLE `lifestylemonthlydifferencereport` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:46:49
