-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `masteremployeeprescription`
--

DROP TABLE IF EXISTS `masteremployeeprescription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `masteremployeeprescription` (
  `clientID` varchar(45) DEFAULT NULL,
  `employeeID` varchar(45) DEFAULT NULL,
  `prescriptionID` varchar(45) DEFAULT NULL,
  `prescriptionName` varchar(100) DEFAULT NULL,
  `prescriptionDescription` varchar(1000) DEFAULT NULL,
  `languageID` varchar(45) DEFAULT NULL,
  `imageDetails` blob
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `masteremployeeprescription`
--

LOCK TABLES `masteremployeeprescription` WRITE;
/*!40000 ALTER TABLE `masteremployeeprescription` DISABLE KEYS */;
INSERT INTO `masteremployeeprescription` VALUES ('AD163P','emp1','ps1','Blood pressure','Blood pressure is the force that moves blood through our circulatory system.It is an important force because oxygen and nutrients would not be pushed around our circulatory system to nourish tissues and organs without blood pressure.','en',NULL),('AD163P','emp1','ps2','Heart failure','If you have high blood pressure, this means that your heart has to work harder to push blood round your body. To cope with this extra effort, your heart becomes thicker and stiffer, which makes it less able to do its job.\n\n','en',NULL),('AD163P','emp1','ps4','Stroke','High blood pressure is a major cause of strokes in the UK. By lowering your blood pressure, you can reduce your risk of developing a stroke','en',NULL),('AD163P','emp1','ps3','Dementia ','Uncontrolled high blood pressure can cause problems by damaging and narrowing the blood vessels in your brain ','en',NULL),('AD163P','emp1','ps5','Low blood pressure ','Low blood pressure is sometimes referred to as hypotension and typically describes blood pressures of 90/60mmHg, or below.Â  ','en',NULL),('AD163P','emp1','ps7','Pregnancy','Around 1 in every 20 women will develop high blood pressure during their pregnancy.','en',NULL),('AD163P','emp1','ps6','Kidney disease','Kidney disease is a term that describes any problems you may have when your kidneys are not working as well as they should.','en',NULL),('AD163P','emp1','ps8','Arteries ','The arteries are the large blood vessels that carry blood from the heart to all the organs and muscles of the body, to give them the energy and oxygen they need.','en',NULL),('AD163P','emp1','ps9','Swollen ankles','High blood pressure makes your heart worker harder than it needed to before. Over the space of many years, this extra effort can lead to the heart muscle becoming thicker and less effective at pushing the blood round.Â  ','en',NULL),('AD163P','emp1','ps10','Menopause','Hormone replacement therapy (HRT) treatments include the hormone oestrogen. Oestrogens can cause a rise in blood pressure, which is why some forms of the contraceptive pill can raise blood pressure.','en',NULL),('AD163P','emp1','ps1','ರಕ್ತದೊತ್ತಡ','ರಕ್ತದೊತ್ತಡವು ನಮ್ಮ ರಕ್ತಪರಿಚಲನಾ ವ್ಯವಸ್ಥೆಯ ಮೂಲಕ ರಕ್ತವನ್ನು ಚಲಿಸುವ ಶಕ್ತಿಯಾಗಿದೆ.ಇದು ಒಂದು ಪ್ರಮುಖ ಶಕ್ತಿಯಾಗಿದೆ ಏಕೆಂದರೆ ರಕ್ತದೊತ್ತಡವಿಲ್ಲದೆ ಅಂಗಾಂಶಗಳು ಮತ್ತು ಅಂಗಗಳನ್ನು ಪೋಷಿಸಲು ಆಮ್ಲಜನಕ ಮತ್ತು ಪೋಷಕಾಂಶಗಳನ್ನು ನಮ್ಮ ರಕ್ತಪರಿಚಲನಾ ವ್ಯವಸ್ಥೆಯ ಸುತ್ತಲೂ ತಳ್ಳಲಾಗುವುದಿಲ್ಲ.','kn',NULL),('AD163P','emp1','ps2','ಹೃದಯಾಘಾತ','ನೀವು ಅಧಿಕ ರಕ್ತದೊತ್ತಡ ಹೊಂದಿದ್ದರೆ, ನಿಮ್ಮ ದೇಹದ ಸುತ್ತಲೂ ರಕ್ತವನ್ನು ತಳ್ಳಲು ನಿಮ್ಮ ಹೃದಯ ಹೆಚ್ಚು ಶ್ರಮಿಸಬೇಕು ಎಂದರ್ಥ. ಈ ಹೆಚ್ಚುವರಿ ಪ್ರಯತ್ನವನ್ನು ನಿಭಾಯಿಸಲು, ನಿಮ್ಮ ಹೃದಯವು ದಪ್ಪವಾಗಿರುತ್ತದೆ ಮತ್ತು ಗಟ್ಟಿಯಾಗುತ್ತದೆ, ಇದರಿಂದಾಗಿ ಅದು ತನ್ನ ಕೆಲಸವನ್ನು ಮಾಡಲು ಸಾಧ್ಯವಾಗುವುದಿಲ್ಲ.','kn',NULL),('AD163P','emp1','ps4','ಪಾರ್ಶ್ವವಾಯು','ಅಧಿಕ ರಕ್ತದೊತ್ತಡವು ಯುಕೆ ನಲ್ಲಿ ಪಾರ್ಶ್ವವಾಯುವಿಗೆ ಪ್ರಮುಖ ಕಾರಣವಾಗಿದೆ. ನಿಮ್ಮ ರಕ್ತದೊತ್ತಡವನ್ನು ಕಡಿಮೆ ಮಾಡುವ ಮೂಲಕ, ಪಾರ್ಶ್ವವಾಯು ಬರುವ ಅಪಾಯವನ್ನು ನೀವು ಕಡಿಮೆ ಮಾಡಬಹುದು','kn',NULL),('AD163P','emp1','ps3','ಬುದ್ಧಿಮಾಂದ್ಯತೆ','ಅನಿಯಂತ್ರಿತ ಅಧಿಕ ರಕ್ತದೊತ್ತಡವು ನಿಮ್ಮ ಮೆದುಳಿನಲ್ಲಿರುವ ರಕ್ತನಾಳಗಳಿಗೆ ಹಾನಿ ಮತ್ತು ಕಿರಿದಾಗುವ ಮೂಲಕ ಸಮಸ್ಯೆಗಳನ್ನು ಉಂಟುಮಾಡುತ್ತದೆ','kn',NULL),('AD163P','emp1','ps5','ಕಡಿಮೆ ರಕ್ತದೊತ್ತಡ','ಕಡಿಮೆ ರಕ್ತದೊತ್ತಡವನ್ನು ಕೆಲವೊಮ್ಮೆ ಹೈಪೊಟೆನ್ಷನ್ ಎಂದು ಕರೆಯಲಾಗುತ್ತದೆ ಮತ್ತು ಸಾಮಾನ್ಯವಾಗಿ 90/60 ಎಂಎಂಹೆಚ್ಜಿ ಅಥವಾ ಕೆಳಗಿನ ರಕ್ತದೊತ್ತಡವನ್ನು ವಿವರಿಸುತ್ತದೆ.','kn',NULL),('AD163P','emp1','ps7','ಗರ್ಭಧಾರಣೆ','ಪ್ರತಿ 20 ಮಹಿಳೆಯರಲ್ಲಿ 1 ಮಹಿಳೆಯರು ತಮ್ಮ ಗರ್ಭಾವಸ್ಥೆಯಲ್ಲಿ ಅಧಿಕ ರಕ್ತದೊತ್ತಡವನ್ನು ಬೆಳೆಸುತ್ತಾ','kn',NULL),('AD163P','emp1','ps6','ಮೂತ್ರಪಿಂಡ ರೋಗ','ಮೂತ್ರಪಿಂಡ ಕಾಯಿಲೆ ಎನ್ನುವುದು ನಿಮ್ಮ ಮೂತ್ರಪಿಂಡಗಳು ಕಾರ್ಯನಿರ್ವಹಿಸದಿದ್ದಾಗ ನೀವು ಹೊಂದಿರಬಹುದಾದ ಯಾವುದೇ ಸಮಸ್ಯೆಗಳನ್ನು ವಿವರಿಸುವ ಪದವಾಗಿದೆ.','kn',NULL),('AD163P','emp1','ps8','ಅಪಧಮನಿಗಳು','ಅಪಧಮನಿಗಳು ಹೃದಯದಿಂದ ದೇಹದ ಎಲ್ಲಾ ಅಂಗಗಳಿಗೆ ಮತ್ತು ಸ್ನಾಯುಗಳಿಗೆ ರಕ್ತವನ್ನು ಸಾಗಿಸುವ ದೊಡ್ಡ ರಕ್ತನಾಳಗಳಾಗಿವೆ, ಅವುಗಳಿಗೆ ಅಗತ್ಯವಾದ ಶಕ್ತಿ ಮತ್ತು ಆಮ್ಲಜನಕವನ್ನು ನೀಡುತ್ತವೆ.','kn',NULL),('AD163P','emp1','ps9','ಕಣಕಾಲುಗಳು ದಿಕೊಂಡವು','ಅಧಿಕ ರಕ್ತದೊತ್ತಡವು ನಿಮ್ಮ ಹೃದಯ ಕೆಲಸಗಾರನನ್ನು ಮೊದಲಿಗಿಂತಲೂ ಕಠಿಣಗೊಳಿಸುತ್ತದೆ. ಅನೇಕ ವರ್ಷಗಳ ಅವಧಿಯಲ್ಲಿ, ಈ ಹೆಚ್ಚುವರಿ ಪ್ರಯತ್ನವು ಹೃದಯ ಸ್ನಾಯು ದಪ್ಪವಾಗಲು ಮತ್ತು ರಕ್ತದ ಸುತ್ತನ್ನು ತಳ್ಳುವಲ್ಲಿ ಕಡಿಮೆ ಪರಿಣಾಮಕಾರಿಯಾಗಲು ಕಾರಣವಾಗಬಹುದು.','kn',NULL),('AD163P','emp1','ps10','ರಜೋನಿವೃತ್ತಿ ','ಹಾರ್ಮೋನ್ ರಿಪ್ಲೇಸ್ಮೆಂಟ್ ಥೆರಪಿ (ಎಚ್‌ಆರ್‌ಟಿ) ಚಿಕಿತ್ಸೆಗಳಲ್ಲಿ ಈಸ್ಟ್ರೊಜೆನ್ ಎಂಬ ಹಾರ್ಮೋನ್ ಸೇರಿದೆ. ಈಸ್ಟ್ರೊಜೆನ್ ರಕ್ತದೊತ್ತಡದ ಹೆಚ್ಚಳಕ್ಕೆ ಕಾರಣವಾಗಬಹುದು, ಅದಕ್ಕಾಗಿಯೇ ಕೆಲವು ರೀತಿಯ ಗರ್ಭನಿರೋಧಕ ಮಾತ್ರೆ ರಕ್ತದೊತ್ತಡವನ್ನು ಹೆಚ್ಚಿಸುತ್ತದೆ.','kn',NULL),('AD163P','emp1','ps11','p111','pd1','en',NULL),('AD163P','emp1','ps11','p111','pd1','en',NULL);
/*!40000 ALTER TABLE `masteremployeeprescription` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:46:09
