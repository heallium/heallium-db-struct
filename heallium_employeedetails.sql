-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employeedetails`
--

DROP TABLE IF EXISTS `employeedetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `employeedetails` (
  `SINO` int(11) DEFAULT NULL,
  `Location` text,
  `EmployeeID` text,
  `EmployeeName` text,
  `PreviousScore` int(11) DEFAULT NULL,
  `CurrentScore` int(11) DEFAULT NULL,
  `PercentageChange` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employeedetails`
--

LOCK TABLES `employeedetails` WRITE;
/*!40000 ALTER TABLE `employeedetails` DISABLE KEYS */;
INSERT INTO `employeedetails` VALUES (1,'Banglore','78ER489','Vinod',89,68,-7),(2,'Chennai','48ER488','Vinay',70,75,5),(3,'Mysore','45ER487','Vijay',65,60,-5),(4,'Coimbatore','46ER486','Prachiee',45,50,5),(5,'Vizag','72ER485','Shenha',55,50,-5),(6,'Delhi','73ER484','LaxmiKanth',75,75,0),(7,'Pune','72ER483','Robert',50,45,-5),(8,'Pandicherry','55ER482','Virat',85,70,-10),(9,'Mumbai','45ER481','Sandeep',70,65,-5),(10,'Alahabad','40ER480','Rajsh',65,70,5),(11,'Hyderabad','32ER479','Anil',60,70,10),(12,'Bhuvenshwar','20ER478','Aravind',55,50,-5),(13,'Ranchi','25ER477','Puneeth',65,60,-5),(14,'Vijayawada','32ER476','Ravi',55,60,5),(15,'Goa','42ER475','Suresh',60,65,5),(16,'Manglore','55ER474','Patil',75,75,0),(17,'Banglore','62ER473','Mahadev',70,75,5),(18,'Ranchi','72ER472','Chandan',65,75,10),(19,'Hubli','82ER471','Hari',60,55,-5),(20,'Dharavada','62ER470','Chiranjivee',55,55,0),(21,'Bangloe','60ER469','Pavan',65,70,5),(22,'Manglore','72ER468','Pradeep',60,60,0),(23,'Ranchi','27ER467','Vadiraj',55,65,5),(24,'Hyderbad','23ER466','Latha',65,60,-5),(25,'Chennai','45ER465','Navya',60,60,0),(26,'Vizag','54ER464','Ravi kumar',68,63,-5),(27,'Hyderbad','50ER463','Satish',65,65,0),(28,'Banglore','39ER462','Megana',55,45,-10),(29,'Manglore','93ER461','Shruthi',65,65,0),(30,'Hubli','40ER460','Nisssar',60,55,-5),(31,'Goa','43ER459','Pramodh',66,63,-3),(32,'Ranchi','22ER458','Anil',65,60,-5),(33,'Vijayawada','25ER457','Prakash',60,60,0),(34,'Ongolu','33ER455','Bobby',55,60,5),(35,'Hyderbad','69ER454','Yaji',65,65,0),(36,'Banglore','79ER453','Shivanna',65,75,10),(37,'Banglore','89ER452','Shashank',65,55,-10),(38,'Banglore','80ER451','Sidram',65,60,-5),(39,'Hubli','70ER450','Tejash',60,65,5),(40,'Manglore','68ER449','sriranga',55,65,10),(41,'Chennai','45ER448','Depika',65,65,0),(42,'Banglore','66ER447','Shashi',65,55,-10),(43,'Vizag','67ER446','Namitha',55,60,5),(44,'Hyderbad','29ER445','Savitha',65,60,-5),(45,'Manglore','31ER444','Shilpa',66,62,-3),(46,'Banglore','41ER443','Karthik',60,62,2),(47,'Chennai','43ER442','Arun',65,65,0),(48,'Vijayawada','49ER441','Chandan',65,55,-10),(49,'Hubli','68ER440','Kasim',60,60,0),(50,'Banglore','66ER439','Brijesh',55,50,-5);
/*!40000 ALTER TABLE `employeedetails` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:46:53
