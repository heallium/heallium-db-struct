-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employeephysiotheraphyactionoptionlist`
--

DROP TABLE IF EXISTS `employeephysiotheraphyactionoptionlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `employeephysiotheraphyactionoptionlist` (
  `actionID` int(11) NOT NULL,
  `actionOptionID` varchar(45) NOT NULL,
  `clientID` varchar(45) NOT NULL,
  KEY `fk_clientID-phyList_idx` (`clientID`),
  KEY `fk_actionOptionID_actionMasterListtt_idx` (`actionOptionID`),
  KEY `fk_selectdeoptionID_aa_idx` (`actionOptionID`),
  KEY `fk_actionID_masterlisttt_idx` (`actionID`),
  CONSTRAINT `fk_actionID_masterlisttt` FOREIGN KEY (`actionID`) REFERENCES `employeephysiotheraphyactionmasterlist` (`actionID`),
  CONSTRAINT `fk_actionOptionID_employee` FOREIGN KEY (`actionOptionID`) REFERENCES `employeephysiotherapyactionmaster` (`actionOptionID`),
  CONSTRAINT `fk_clientID-phyList` FOREIGN KEY (`clientID`) REFERENCES `masterclient` (`clientID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employeephysiotheraphyactionoptionlist`
--

LOCK TABLES `employeephysiotheraphyactionoptionlist` WRITE;
/*!40000 ALTER TABLE `employeephysiotheraphyactionoptionlist` DISABLE KEYS */;
INSERT INTO `employeephysiotheraphyactionoptionlist` VALUES (1,'1','AD163P'),(1,'2','AD163P'),(2,'3','AD163P'),(2,'4','AD163P'),(3,'5','AD163P'),(3,'6','AD163P'),(4,'7','AD163P'),(4,'8','AD163P'),(5,'9','AD163P'),(5,'10','AD163P'),(6,'11','AD163P'),(6,'12','AD163P'),(7,'13','AD163P'),(7,'14','AD163P'),(8,'15','AD163P'),(8,'16','AD163P'),(9,'17','AD163P'),(9,'18','AD163P'),(10,'19','AD163P'),(10,'20','AD163P'),(11,'21','AD163P'),(11,'22','AD163P'),(12,'23','AD163P'),(12,'24','AD163P'),(13,'25','AD163P'),(13,'26','AD163P'),(14,'27','AD163P'),(14,'28','AD163P'),(15,'29','AD163P'),(15,'30','AD163P'),(16,'31','AD163P'),(16,'32','AD163P'),(17,'33','AD163P'),(17,'34','AD163P'),(18,'35','AD163P'),(18,'36','AD163P'),(19,'37','AD163P'),(19,'38','AD163P'),(20,'39','AD163P'),(20,'40','AD163P'),(21,'41','AD163P'),(21,'42','AD163P'),(22,'43','AD163P'),(22,'44','AD163P'),(23,'45','AD163P'),(23,'46','AD163P'),(24,'47','AD163P'),(24,'48','AD163P'),(25,'49','AD163P'),(25,'50','AD163P'),(26,'51','AD163P'),(26,'52','AD163P'),(27,'53','AD163P'),(27,'54','AD163P'),(28,'55','AD163P'),(28,'56','AD163P'),(29,'57','AD163P'),(29,'58','AD163P'),(30,'59','AD163P'),(30,'60','AD163P'),(31,'61','AD163P'),(31,'62','AD163P'),(32,'63','AD163P'),(32,'64','AD163P'),(33,'65','AD163P'),(33,'66','AD163P'),(34,'67','AD163P'),(34,'68','AD163P'),(35,'69','AD163P'),(35,'70','AD163P'),(36,'71','AD163P'),(36,'72','AD163P'),(37,'73','AD163P'),(37,'74','AD163P'),(38,'75','AD163P'),(38,'76','AD163P'),(39,'77','AD163P'),(39,'78','AD163P'),(40,'79','AD163P'),(40,'80','AD163P'),(41,'81','AD163P'),(41,'82','AD163P'),(42,'83','AD163P'),(42,'84','AD163P'),(43,'85','AD163P'),(43,'86','AD163P'),(44,'87','AD163P'),(44,'88','AD163P'),(45,'89','AD163P'),(45,'90','AD163P'),(46,'91','AD163P'),(46,'92','AD163P'),(47,'93','AD163P'),(47,'94','AD163P'),(48,'95','AD163P'),(48,'96','AD163P'),(49,'97','AD163P'),(49,'98','AD163P'),(50,'99','AD163P'),(50,'100','AD163P'),(51,'101','AD163P'),(51,'102','AD163P'),(52,'103','AD163P'),(52,'104','AD163P'),(53,'105','AD163P'),(53,'106','AD163P'),(54,'107','AD163P'),(54,'108','AD163P'),(55,'109','AD163P'),(55,'110','AD163P'),(56,'111','AD163P'),(56,'112','AD163P'),(57,'113','AD163P'),(57,'114','AD163P'),(58,'115','AD163P'),(58,'116','AD163P'),(59,'117','AD163P'),(59,'118','AD163P'),(60,'119','AD163P'),(60,'120','AD163P');
/*!40000 ALTER TABLE `employeephysiotheraphyactionoptionlist` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:45:44
