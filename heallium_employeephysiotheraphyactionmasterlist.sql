-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employeephysiotheraphyactionmasterlist`
--

DROP TABLE IF EXISTS `employeephysiotheraphyactionmasterlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `employeephysiotheraphyactionmasterlist` (
  `actionID` int(11) NOT NULL AUTO_INCREMENT,
  `clientID` varchar(45) NOT NULL,
  `actionName` varchar(500) NOT NULL,
  PRIMARY KEY (`actionID`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employeephysiotheraphyactionmasterlist`
--

LOCK TABLES `employeephysiotheraphyactionmasterlist` WRITE;
/*!40000 ALTER TABLE `employeephysiotheraphyactionmasterlist` DISABLE KEYS */;
INSERT INTO `employeephysiotheraphyactionmasterlist` VALUES (1,'AD163P','Did your weight change during the last 12 months'),(2,'AD163P','How did your food intake change during the last 12 months'),(3,'AD163P','Did you had 3liters of water yesterday'),(4,'AD163P','Did you slept 8hours yesterday'),(5,'AD163P','Did you had brocoli in your lunch in this week'),(6,'AD163P','Did you haing orange juice once in a day'),(7,'AD163P','Did you had  fruits in your lunch yesterday'),(8,'AD163P','Did you had any junk food yesterday'),(9,'AD163P','Did you sleeping everyday late night'),(10,'AD163P','Did you walking 400 steps everyday'),(11,'AD163P','Did you had banana after the lunch yesterday'),(12,'AD163P','Did you slept without pillow last night'),(13,'AD163P','Are you taking a nap after lunch'),(14,'AD163P','Are you taking a nap after lunch everyday'),(15,'AD163P','Are you feeling lonely '),(16,'AD163P','Are you feeling stress'),(17,'AD163P','Are you doing excessize everday in the morning'),(18,'AD163P','Are you having milk in the breakfast '),(19,'AD163P','Are you sleeping late night everyday'),(20,'AD163P','Are you drinking daily 3liters of water'),(21,'AD163P','Are you sleeping without pillow everyday'),(22,'AD163P','Are you having non-veg everday'),(23,'AD163P','Did you had egg in your breakfast  yesterday'),(24,'AD163P','Did you missing breakfat everyday'),(25,'AD163P','Did you traveled yesterday'),(26,'AD163P','Did you missing lunch everyday'),(27,'AD163P','Are you feeling bored'),(28,'AD163P','Are you using social media most time'),(29,'AD163P','Did you had junk food yesterday'),(30,'AD163P','Are you traveling on bike everyday'),(31,'AD163P','Are you having snacks yesterday'),(32,'AD163P','Did you missed your dinner yesterday'),(33,'AD163P','Are you having break in your work '),(34,'AD163P','Did you sleeping without pillow'),(35,'AD163P','Did you had fruits in your dinner yesterday'),(36,'AD163P','Are you having 3 cups of tea everyday'),(37,'AD163P','Are you having coffee everday'),(38,'AD163P','Did you had milk in the breakfast yesterday'),(39,'AD163P','Are you  drinking cold water everyday'),(40,'AD163P','Are you had ghee in your dinner yesterday'),(41,'AD163P','Did you slept yesterday latenight'),(42,'AD163P','Are you watching movies every night'),(43,'AD163P','Did you done excessize yesterday'),(44,'AD163P','Did you walk 400 feets yesterday'),(45,'AD163P','Did you had oily food yesterday'),(46,'AD163P','Are you taking oily food everday'),(47,'AD163P','Are you missed dinner yesterday'),(48,'AD163P','Are you having fruits everyday'),(49,'AD163P','Did you had apple  yesterday'),(50,'AD163P','Did you felt work pressure yesterday'),(51,'AD163P','Are you in bed right now'),(52,'AD163P','Are you engaged with your work'),(53,'AD163P','Did you had milk yesterday'),(54,'AD163P','Did you stuck yesterday in traffic'),(55,'AD163P','Did you had late night party yesterday'),(56,'AD163P','Are you drinking warm water after you wake up in the morning'),(57,'AD163P','Did you try to have a calm breakfast'),(58,'AD163P','Did you try to spend time outdoors'),(59,'AD163P','Did you try to meditate twice today'),(60,'AD163P','Did you try to spend time outdoors');
/*!40000 ALTER TABLE `employeephysiotheraphyactionmasterlist` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:44:59
