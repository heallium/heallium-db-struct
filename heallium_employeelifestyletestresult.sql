-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employeelifestyletestresult`
--

DROP TABLE IF EXISTS `employeelifestyletestresult`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `employeelifestyletestresult` (
  `employeeID` varchar(45) NOT NULL,
  `clientID` varchar(45) NOT NULL,
  `lifestyleTestStatus` varchar(45) NOT NULL,
  `lifeStyleTestscore` double NOT NULL,
  `lifeStyleTestDate` date NOT NULL,
  `locationID` varchar(45) NOT NULL,
  `branchID` varchar(45) NOT NULL,
  `teamID` varchar(45) NOT NULL,
  `projectID` varchar(45) NOT NULL,
  `groupPathID` varchar(45) NOT NULL,
  KEY `fk_clientID_lifeStyle_idx` (`clientID`),
  CONSTRAINT `fk_clientID_lifeStyle` FOREIGN KEY (`clientID`) REFERENCES `masterclient` (`clientID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employeelifestyletestresult`
--

LOCK TABLES `employeelifestyletestresult` WRITE;
/*!40000 ALTER TABLE `employeelifestyletestresult` DISABLE KEYS */;
INSERT INTO `employeelifestyletestresult` VALUES ('Emp2','AD163P','Compeleted',5,'2019-06-10','553471','M742187','AT77S','B1744','IF51J'),('Emp3','AD163P','Compeleted',6,'2019-06-10','553471','M742187','AT77S','B1744','IF51J'),('Emp4','AD163P','Compeleted',8,'2019-06-10','553471','M742187','AT77S','B1744','IF51J'),('Emp5','AD163P','Compeleted',10,'2019-06-10','553471','M742187','AT77S','B1744','IF51J'),('Emp6','AD163P','Compeleted',9,'2019-06-10','553471','M742187','AT77S','B1744','IF51J'),('Emp7','AD163P','Compeleted',4,'2019-06-10','553471','M742187','AT77S','B1744','IF51J'),('Emp8','AD163P','Compeleted',6,'2019-06-10','553471','M742187','AT77S','B1744','IF51J'),('Emp9','AD163P','Compeleted',7,'2019-06-10','553471','M742187','AT77S','B1744','IF51J'),('Emp10','AD163P','Compeleted',10,'2019-06-10','553471','M742187','AT77S','B1744','IF51J'),('Emp1','AD163P','completed',7,'2019-09-24','553471','M742187','B1744','AT77S','IF51J');
/*!40000 ALTER TABLE `employeelifestyletestresult` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:46:31
