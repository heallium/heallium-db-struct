-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employeephysiotherapytesttransaction`
--

DROP TABLE IF EXISTS `employeephysiotherapytesttransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `employeephysiotherapytesttransaction` (
  `transactionID` int(11) NOT NULL AUTO_INCREMENT,
  `employeeID` varchar(45) NOT NULL,
  `clientID` varchar(45) NOT NULL,
  `questionID` int(11) NOT NULL,
  `selectedOptionID` varchar(45) NOT NULL,
  PRIMARY KEY (`transactionID`),
  KEY `fk_clientID_physioTrans_idx` (`clientID`),
  CONSTRAINT `fk_clientID_physioTrans` FOREIGN KEY (`clientID`) REFERENCES `masterclient` (`clientID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employeephysiotherapytesttransaction`
--

LOCK TABLES `employeephysiotherapytesttransaction` WRITE;
/*!40000 ALTER TABLE `employeephysiotherapytesttransaction` DISABLE KEYS */;
INSERT INTO `employeephysiotherapytesttransaction` VALUES (1,'Emp1','AD163P',1,'1'),(2,'Emp1','AD163P',2,'7'),(3,'Emp1','AD163P',3,'10'),(4,'Emp1','AD163P',4,'15'),(5,'Emp1','AD163P',5,'19'),(6,'Emp1','AD163P',6,'21'),(7,'Emp1','AD163P',7,'25'),(8,'Emp1','AD163P',8,'30'),(9,'Emp1','AD163P',9,'33'),(10,'Emp1','AD163P',10,'39'),(11,'Emp1','AD163P',11,'41'),(12,'Emp1','AD163P',12,'47'),(13,'Emp1','AD163P',13,'50'),(14,'Emp1','AD163P',14,'53'),(15,'Emp1','AD163P',15,'57');
/*!40000 ALTER TABLE `employeephysiotherapytesttransaction` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:46:43
