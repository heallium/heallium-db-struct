-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employeeactionmasterlist`
--

DROP TABLE IF EXISTS `employeeactionmasterlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `employeeactionmasterlist` (
  `actionID` int(11) NOT NULL AUTO_INCREMENT,
  `clientID` varchar(45) NOT NULL,
  `actionName` varchar(500) NOT NULL,
  `actionOptionFormat` int(11) DEFAULT NULL,
  `actionFrequency` int(11) DEFAULT NULL,
  `languageID` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`actionID`),
  UNIQUE KEY `actionID_UNIQUE` (`actionID`),
  KEY `fk_clientID_actionMasterList_idx` (`clientID`),
  CONSTRAINT `fk_clientID_actionMasterList` FOREIGN KEY (`clientID`) REFERENCES `masterclient` (`clientID`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employeeactionmasterlist`
--

LOCK TABLES `employeeactionmasterlist` WRITE;
/*!40000 ALTER TABLE `employeeactionmasterlist` DISABLE KEYS */;
INSERT INTO `employeeactionmasterlist` VALUES (1,'AD163P','Did your weight change during the last 12 months',0,7,'en'),(2,'AD163P','How did your food intake change during the last 12 months',0,7,'en'),(3,'AD163P','How many liters of water having every day',2,7,'en'),(4,'AD163P','Did you slept 8hours yesterday',0,7,'en'),(5,'AD163P','Did you had brocoli in your lunch in this week',0,7,'en'),(6,'AD163P','Did you having which type of fruits juice everyday in your lunch',1,7,'en'),(7,'AD163P','Did you had  fruits in your lunch yesterday',0,7,'en'),(8,'AD163P','Did you had any junk food yesterday',0,7,'en'),(9,'AD163P','Every day at what time you are sleeping',1,7,'en'),(10,'AD163P','How many steps you are walking everyday',1,7,'en'),(11,'AD163P','How many bananas you are having every day',2,7,'en'),(12,'AD163P','Did you slept without pillow last night',0,7,'en'),(13,'AD163P','Are you taking a nap after lunch',0,7,'en'),(14,'AD163P','Are you taking a nap after lunch everyday',0,7,'en'),(15,'AD163P','Are you feeling lonely ',0,7,'en'),(16,'AD163P','Are you feeling stress',0,7,'en'),(17,'AD163P','Are you doing excessize everday in the morning',0,7,'en'),(18,'AD163P','Are you having milk in the breakfast ',0,7,'en'),(19,'AD163P','Are you sleeping late night everyday',0,7,'en'),(20,'AD163P','Did you take daily 3liters of water',0,7,'en'),(21,'AD163P','Did you sleeping without pillow everyday',0,7,'en'),(22,'AD163P','Did you having non-veg everday',0,7,'en'),(23,'AD163P','Did you had egg in your breakfat  yesterday',0,7,'en'),(24,'AD163P','Did you missing breakfat everyday',0,7,'en'),(25,'AD163P','Did you traveled yesterday',0,7,'en'),(26,'AD163P','In a week how many days you are mising lunch',2,7,'en'),(27,'AD163P','Are you feeling bored',0,7,'en'),(28,'AD163P','Are you using social media most time',0,7,'en'),(29,'AD163P','Did you had junk food yesterday',0,7,'en'),(30,'AD163P','Daily how many hours travelling on bike',1,7,'en'),(31,'AD163P','Are you having snacks yesterday',0,7,'en'),(32,'AD163P','Did you missed your dinner yesterday',0,7,'en'),(33,'AD163P','Are you having break in your work ',0,7,'en'),(34,'AD163P','Did you sleeping without pillow',0,7,'en'),(35,'AD163P','Did you had fruits in your dinner yesterday',0,7,'en'),(36,'AD163P','How many cups of tea having everyday',2,7,'en'),(37,'AD163P','Are you having coffee everday',0,7,'en'),(38,'AD163P','Did you had milk in the breakfast yesterday',0,7,'en'),(39,'AD163P','Are you  drinking cold water everyday',0,7,'en'),(40,'AD163P','Are you had ghee in your dinner yesterday',0,7,'en'),(41,'AD163P','Did you slept yesterday latenight',0,7,'en'),(42,'AD163P','Are you watching movies every night',0,7,'en'),(43,'AD163P','Did you done excessize yesterday',0,7,'en'),(44,'AD163P','Did you walk 400 feets yesterday',0,7,'en'),(45,'AD163P','Did you had oily food yesterday',0,7,'en'),(46,'AD163P','Did you taking oily food everday',0,7,'en'),(47,'AD163P','Are you missed dinner yesterday',0,7,'en'),(48,'AD163P','Did you having fruits everyday',0,7,'en'),(49,'AD163P','How many apples having everyday',1,7,'en'),(50,'AD163P','Did you felt work pressure yesterday',0,7,'en'),(51,'AD163P','Are you in bed right now',0,7,'en'),(52,'AD163P','Are you engaged with your work',0,7,'en'),(53,'AD163P','Did you had milk yesterday',0,7,'en'),(54,'AD163P','Daily how many hours stuck in traffic',1,7,'en'),(55,'AD163P','Did you had late night party yesterday',0,7,'en'),(56,'AD163P','Are you drinking warm water after you wake up in the morning',0,7,'en'),(57,'AD163P','Did you try to have a calm breakfast',0,7,'en'),(58,'AD163P','Daily how much  time spends  outdoors',2,7,'en'),(59,'AD163P','How many times meditate everyday',1,7,'en'),(60,'AD163P','In a week how many time spends outdoor',2,7,'en'),(61,'AD163P','ಕಳೆದ 12 ತಿಂಗಳುಗಳಲ್ಲಿ ನಿಮ್ಮ ತೂಕ ಬದಲಾಗಿದೆಯೇ?',0,7,'kn'),(62,'AD163P','ಕಳೆದ 12 ತಿಂಗಳುಗಳಲ್ಲಿ ನಿಮ್ಮ ಆಹಾರ ಸೇವನೆಯು ಹೇಗೆ ಬದಲಾಗಿದೆ',0,7,'kn'),(63,'AD163P','ಪ್ರತಿದಿನ ಎಷ್ಟು ಲೀಟರ್ ನೀರು ಇದೆ',2,7,'kn'),(64,'AD163P','ನೀವು ನಿನ್ನೆ 8 ಗಂಟೆ ಮಲಗಿದ್ದೀರಾ',0,7,'kn'),(65,'AD163P','ಈ ವಾರದಲ್ಲಿ ನಿಮ್ಮ ಮದ್ಯಾನ್ಹ ಊಟ ಟದಲ್ಲಿ ನೀವು ಬ್ರೊಕೊಲಿ ಹೊಂದಿದ್ದೀರಾ?',0,7,'kn'),(66,'AD163P','ನಿಮ್ಮ .ಟದಲ್ಲಿ ಪ್ರತಿದಿನ ಯಾವ ರೀತಿಯ ಹಣ್ಣಿನ ರಸವನ್ನು ಹೊಂದಿದ್ದೀರಾ?',1,7,'kn'),(67,'AD163P','ನಿನ್ನೆ ನಿಮ್ಮ ಮದ್ಯಾನ್ಹ ಊಟ ಟದಲ್ಲಿ ನೀವು ಹಣ್ಣುಗಳನ್ನು ಹೊಂದಿದ್ದೀರಾ?',0,7,'kn'),(68,'AD163P','ನಿನ್ನೆ ನೀವು ಏನಾದರೂ ಜಂಕ್ ಫುಡ್ ಹೊಂದಿದ್ದೀರಾ',0,7,'kn'),(69,'AD163P','ಪ್ರತಿದಿನ ನೀವು ಯಾವ ಸಮಯದಲ್ಲಿ ಮಲಗಿದ್ದೀರಿ',1,7,'kn'),(70,'AD163P','ನೀವು ಪ್ರತಿದಿನ ಎಷ್ಟು ಹೆಜ್ಜೆಗಳನ್ನು ನಡೆಸುತ್ತಿದ್ದೀರಿ',1,7,'kn'),(71,'AD163P','ನೀವು ಪ್ರತಿದಿನ ಎಷ್ಟು ಬಾಳೆಹಣ್ಣುಗಳನ್ನು ಹೊಂದಿದ್ದೀರಿ',2,7,'kn'),(72,'AD163P','ನಿನ್ನೆ ರಾತ್ರಿ ನೀವು ದಿಂಬು ಇಲ್ಲದೆ ಮಲಗಿದ್ದೀರಾ',0,7,'kn'),(73,'AD163P','ಮದ್ಯಾನ್ಹ ಊಟ ಟದ ನಂತರ ನೀವು ಚಿಕ್ಕನಿದ್ರೆ ತೆಗೆದುಕೊಳ್ಳುತ್ತೀರಾ?',0,7,'kn'),(74,'AD163P','ಪ್ರತಿದಿನ ಮದ್ಯಾನ್ಹ ಊಟ ಟದ ನಂತರ ನೀವು ಚಿಕ್ಕನಿದ್ರೆ ತೆಗೆದುಕೊಳ್ಳುತ್ತೀರಾ?',0,7,'kn'),(75,'AD163P','ನೀವು ಒಂಟಿತನ ಅನುಭವಿಸುತ್ತಿದ್ದೀರಾ',0,7,'kn'),(76,'AD163P','ನೀವು ಒತ್ತಡವನ್ನು ಅನುಭವಿಸುತ್ತಿದ್ದೀರಾ',0,7,'kn'),(77,'AD163P','ನೀವು ಬೆಳಿಗ್ಗೆ ಎಂದೆಂದಿಗೂ ಮಿತಿಮೀರಿ ಮಾಡುತ್ತಿದ್ದೀರಾ',0,7,'kn'),(78,'AD163P','ನೀವು ಉಪಾಹಾರದಲ್ಲಿ ಹಾಲು ಸೇವಿಸುತ್ತಿದ್ದೀರಾ',0,7,'kn'),(79,'AD163P','ನೀವು ಪ್ರತಿದಿನ ತಡರಾತ್ರಿ ಮಲಗಿದ್ದೀರಾ',0,7,'kn'),(80,'AD163P','ನೀವು ಪ್ರತಿದಿನ 3 ಲೀಟರ್ ನೀರನ್ನು ತೆಗೆದುಕೊಂಡಿದ್ದೀರಾ',0,7,'kn'),(81,'AD163P','ನೀವು ಪ್ರತಿದಿನ ದಿಂಬು ಇಲ್ಲದೆ ಮಲಗಿದ್ದೀರಾ',0,7,'kn'),(82,'AD163P','ನೀವು ಎಂದೆಂದಿಗೂ ಸಸ್ಯಾಹಾರಿ ಹೊಂದಿದ್ದೀರಾ?',0,7,'kn'),(83,'AD163P','ನಿನ್ನೆ ನಿಮ್ಮ ಬ್ರೇಕ್‌ಫ್ಯಾಟ್‌ನಲ್ಲಿ ನೀವು ಮೊಟ್ಟೆ ಹೊಂದಿದ್ದೀರಾ?',0,7,'kn'),(84,'AD163P','ನೀವು ಪ್ರತಿದಿನ ಬ್ರೇಕ್‌ಫ್ಯಾಟ್ ಅನ್ನು ಕಳೆದುಕೊಂಡಿದ್ದೀರಾ',0,7,'kn'),(85,'AD163P','ನೀವು ನಿನ್ನೆ ಪ್ರಯಾಣಿಸಿದ್ದೀರಾ',0,7,'kn'),(86,'AD163P','ಒಂದು ವಾರದಲ್ಲಿ ನೀವು ಎಷ್ಟು ದಿನಗಳಲ್ಲಿ .ಟವನ್ನು ಕಳೆಯುತ್ತಿದ್ದೀರಿ',2,7,'kn'),(87,'AD163P','ನಿಮಗೆ ಬೇಸರವಾಗುತ್ತಿದೆ',0,7,'kn'),(88,'AD163P','ನೀವು ಹೆಚ್ಚು ಸಮಯ ಸಾಮಾಜಿಕ ಮಾಧ್ಯಮವನ್ನು ಬಳಸುತ್ತಿರುವಿರಾ',0,7,'kn'),(89,'AD163P','ನಿನ್ನೆ ನೀವು ಜಂಕ್ ಫುಡ್ ಮಾಡಿದ್ದೀರಾ',0,7,'kn'),(90,'AD163P','ಪ್ರತಿದಿನ ಬೈಕ್‌ನಲ್ಲಿ ಎಷ್ಟು ಗಂಟೆಗಳ ಪ್ರಯಾಣ',1,7,'kn'),(91,'AD163P','ನೀವು ನಿನ್ನೆ ತಿಂಡಿ ಮಾಡುತ್ತಿದ್ದೀರಾ',0,7,'kn'),(92,'AD163P','ನಿನ್ನೆ ನಿಮ್ಮ ಭೋಜನವನ್ನು ನೀವು ತಪ್ಪಿಸಿಕೊಂಡಿದ್ದೀರಾ',0,7,'kn'),(93,'AD163P','ನಿಮ್ಮ ಕೆಲಸದಲ್ಲಿ ನೀವು ವಿರಾಮವನ್ನು ಹೊಂದಿದ್ದೀರಾ?',0,7,'kn'),(94,'AD163P','ನೀವು ದಿಂಬು ಇಲ್ಲದೆ ಮಲಗಿದ್ದೀರಾ',0,7,'kn'),(95,'AD163P','ನಿನ್ನೆ ನಿಮ್ಮ ಭೋಜನದಲ್ಲಿ ನೀವು ಹಣ್ಣುಗಳನ್ನು ಹೊಂದಿದ್ದೀರಾ?',0,7,'kn'),(96,'AD163P','ಪ್ರತಿದಿನ ಎಷ್ಟು ಕಪ್ ಚಹಾವನ್ನು ಹೊಂದಿರುತ್ತದೆ',2,7,'kn'),(97,'AD163P','ನೀವು ಎಂದೆಂದಿಗೂ ಕಾಫಿ ಸೇವಿಸುತ್ತಿದ್ದೀರಾ',0,7,'kn'),(98,'AD163P','ನಿನ್ನೆ ಉಪಾಹಾರದಲ್ಲಿ ನೀವು ಹಾಲು ಹೊಂದಿದ್ದೀರಾ?',0,7,'kn'),(99,'AD163P','ನೀವು ಪ್ರತಿದಿನ ತಣ್ಣೀರು ಕುಡಿಯುತ್ತಿದ್ದೀರಾ',0,7,'kn'),(100,'AD163P','ನಿನ್ನೆ ನಿಮ್ಮ ಭೋಜನ ಟಕ್ಕೆ ನೀವು ತುಪ್ಪ ಹೊಂದಿದ್ದೀರಾ?',0,7,'kn'),(101,'AD163P','ನೀವು ನಿನ್ನೆ ತಡರಾತ್ರಿ ಮಲಗಿದ್ದೀರಾ',0,7,'kn'),(102,'AD163P','ನೀವು ಪ್ರತಿ ರಾತ್ರಿ ಚಲನಚಿತ್ರಗಳನ್ನು ನೋಡುತ್ತಿದ್ದೀರಾ',0,7,'kn'),(103,'AD163P','ನೀವು ನಿನ್ನೆ ಮಿತಿಮೀರಿ ಮಾಡಿದ್ದೀರಾ',0,7,'kn'),(104,'AD163P','ನೀವು ನಿನ್ನೆ 400 ಫೀಟ್‌ಗಳನ್ನು ನಡೆದಿದ್ದೀರಾ',0,7,'kn'),(105,'AD163P','ನೀವು ನಿನ್ನೆ ಎಣ್ಣೆಯುಕ್ತ ಆಹಾರವನ್ನು ಹೊಂದಿದ್ದೀರಾ?',0,7,'kn'),(106,'AD163P','ನೀವು ಎಣ್ಣೆಯುಕ್ತ ಆಹಾರವನ್ನು ಎಂದಾದರೂ ತೆಗೆದುಕೊಳ್ಳುತ್ತೀರಾ',0,7,'kn'),(107,'AD163P','ನೀವು ನಿನ್ನೆ ಭೋಜನವನ್ನು ತಪ್ಪಿಸಿಕೊಂಡಿದ್ದೀರಾ',0,7,'kn'),(108,'AD163P','ನೀವು ಪ್ರತಿದಿನ ಹಣ್ಣುಗಳನ್ನು ಹೊಂದಿದ್ದೀರಾ',0,7,'kn'),(109,'AD163P','ಪ್ರತಿದಿನ ಎಷ್ಟು ಸೇಬುಗಳು',1,7,'kn'),(110,'AD163P','ನಿನ್ನೆ ನೀವು ಕೆಲಸದ ಒತ್ತಡವನ್ನು ಅನುಭವಿಸಿದ್ದೀರಾ',0,7,'kn'),(111,'AD163P','ನೀವು ಇದೀಗ ಹಾಸಿಗೆಯಲ್ಲಿದ್ದೀರಾ',0,7,'kn'),(112,'AD163P','ನಿಮ್ಮ ಕೆಲಸದಲ್ಲಿ ನೀವು ತೊಡಗಿಸಿಕೊಂಡಿದ್ದೀರಾ?',0,7,'kn'),(113,'AD163P','ನಿನ್ನೆ ನೀವು ಹಾಲು ಹೊಂದಿದ್ದೀರಾ',0,7,'kn'),(114,'AD163P','ಪ್ರತಿದಿನ ಎಷ್ಟು ಗಂಟೆಗಳ ಸಂಚಾರ ದಟ್ಟಣೆಯಲ್ಲಿದೆ',1,7,'kn'),(115,'AD163P','ನೀವು ನಿನ್ನೆ ತಡರಾತ್ರಿ ಪಾರ್ಟಿ ಮಾಡಿದ್ದೀರಾ?',0,7,'kn'),(116,'AD163P','ನೀವು ಬೆಳಿಗ್ಗೆ ಎದ್ದ ನಂತರ ಬೆಚ್ಚಗಿನ ನೀರನ್ನು ಕುಡಿಯುತ್ತೀರಾ?',0,7,'kn'),(117,'AD163P','ನೀವು ಶಾಂತ ಉಪಹಾರವನ್ನು ಮಾಡಲು ಪ್ರಯತ್ನಿಸಿದ್ದೀರಾ',0,7,'kn'),(118,'AD163P','ದೈನಂದಿನ ಹೊರಾಂಗಣದಲ್ಲಿ ಎಷ್ಟು ಸಮಯ ಕಳೆಯುತ್ತದೆ',2,7,'kn'),(119,'AD163P','ಪ್ರತಿದಿನ ಎಷ್ಟು ಬಾರಿ ಧ್ಯಾನ ಮಾಡಿ',1,7,'kn'),(120,'AD163P','ಒಂದು ವಾರದಲ್ಲಿ ಎಷ್ಟು ಸಮಯ ಹೊರಾಂಗಣದಲ್ಲಿ ಕಳೆಯುತ್ತದೆ',2,7,'kn');
/*!40000 ALTER TABLE `employeeactionmasterlist` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:46:10
