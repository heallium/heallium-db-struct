-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `enrolement`
--

DROP TABLE IF EXISTS `enrolement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `enrolement` (
  `registered` int(11) DEFAULT NULL,
  `registeredNotCompletedTest` int(11) DEFAULT NULL,
  `registeredandCompletedTest` int(11) NOT NULL,
  `clientID` varchar(45) NOT NULL,
  `locationID` varchar(45) NOT NULL,
  `branchID` varchar(45) NOT NULL,
  `projectID` varchar(45) NOT NULL,
  `teamID` varchar(45) NOT NULL,
  `groupPathID` varchar(45) NOT NULL,
  `testDate` date DEFAULT NULL,
  KEY `fk_clientID_ce_idx` (`clientID`),
  KEY `fk_team_id_ec_idx` (`teamID`),
  CONSTRAINT `fk_clientID_ce` FOREIGN KEY (`clientID`) REFERENCES `masterclient` (`clientID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enrolement`
--

LOCK TABLES `enrolement` WRITE;
/*!40000 ALTER TABLE `enrolement` DISABLE KEYS */;
INSERT INTO `enrolement` VALUES (0,0,10,'AD163P','553471','M742187','B1744','AT77S','IF51J','2019-01-10'),(0,0,10,'AD163P','553471','M742187','B1744','AT97I','ASP32K','2019-01-11');
/*!40000 ALTER TABLE `enrolement` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:44:56
