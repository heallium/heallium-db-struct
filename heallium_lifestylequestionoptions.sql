-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lifestylequestionoptions`
--

DROP TABLE IF EXISTS `lifestylequestionoptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `lifestylequestionoptions` (
  `optionID` varchar(45) NOT NULL,
  `optionText` varchar(250) NOT NULL,
  `points` int(11) NOT NULL,
  `questionID` int(11) NOT NULL,
  `actionID` int(11) DEFAULT NULL,
  PRIMARY KEY (`optionID`),
  UNIQUE KEY `optionID_UNIQUE` (`optionID`),
  KEY `fk_q_li_jjfsh_idx` (`questionID`),
  CONSTRAINT `fk_q_li_jjfsh` FOREIGN KEY (`questionID`) REFERENCES `masterlifestylequestion` (`questionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lifestylequestionoptions`
--

LOCK TABLES `lifestylequestionoptions` WRITE;
/*!40000 ALTER TABLE `lifestylequestionoptions` DISABLE KEYS */;
INSERT INTO `lifestylequestionoptions` VALUES ('1','Most of the time',0,1,1),('10','Almost never',0,2,10),('100','ಬಹುತೇಕ ಎಂದಿಗೂ',1,20,100),('101','ಹೆಚ್ಚಿನ ಸಮಯ',0,21,101),('102','ಆಗಾಗ್ಗೆ',1,21,102),('103','ಕೆಲವೊಮ್ಮೆ',0,21,103),('104','ಅಪರೂಪ',1,21,104),('105','ಬಹುತೇಕ ಎಂದಿಗೂ',0,21,105),('106','ಹೆಚ್ಚಿನ ಸಮಯ',1,22,106),('107','ಆಗಾಗ್ಗೆ',0,22,107),('108','ಕೆಲವೊಮ್ಮೆ',1,22,108),('109','ಅಪರೂಪ',0,22,109),('11','Most of the time',1,3,11),('110','ಬಹುತೇಕ ಎಂದಿಗೂ',1,22,110),('111','ಹೆಚ್ಚಿನ ಸಮಯ',0,23,111),('112','ಆಗಾಗ್ಗೆ',1,23,112),('113','ಕೆಲವೊಮ್ಮೆ',0,23,113),('114','ಅಪರೂಪ',1,23,114),('115','ಬಹುತೇಕ ಎಂದಿಗೂ',0,23,115),('116','ಹೆಚ್ಚಿನ ಸಮಯ',1,24,116),('117','ಆಗಾಗ್ಗೆ',0,24,117),('118','ಕೆಲವೊಮ್ಮೆ',1,24,118),('119','ಅಪರೂಪ',0,24,119),('12','Often',0,3,12),('120','ಬಹುತೇಕ ಎಂದಿಗೂ',1,24,120),('121','ಹೆಚ್ಚಿನ ಸಮಯ',1,25,121),('122','ಆಗಾಗ್ಗೆ',0,25,122),('123','ಕೆಲವೊಮ್ಮೆ',1,25,123),('124','ಅಪರೂಪ',0,25,124),('125','ಬಹುತೇಕ ಎಂದಿಗೂ',1,25,125),('126','ಹೆಚ್ಚಿನ ಸಮಯ',0,26,126),('127','ಆಗಾಗ್ಗೆ',1,26,127),('128','ಕೆಲವೊಮ್ಮೆ',0,26,128),('129','ಅಪರೂಪ',1,26,129),('13','Sometimes',1,3,13),('130','ಬಹುತೇಕ ಎಂದಿಗೂ',0,26,130),('131','ಹೆಚ್ಚಿನ ಸಮಯ',1,27,131),('132','ಆಗಾಗ್ಗೆ',0,27,132),('133','ಕೆಲವೊಮ್ಮೆ',1,27,133),('134','ಅಪರೂಪ',0,27,134),('135','ಬಹುತೇಕ ಎಂದಿಗೂ',1,27,135),('136','ಹೆಚ್ಚಿನ ಸಮಯ',0,28,136),('137','ಆಗಾಗ್ಗೆ',1,28,137),('138','ಕೆಲವೊಮ್ಮೆ',0,28,138),('139','ಅಪರೂಪ',1,28,139),('14','Rarely',1,3,14),('140','ಬಹುತೇಕ ಎಂದಿಗೂ',0,28,140),('141','ಹೆಚ್ಚಿನ ಸಮಯ',1,29,141),('142','ಆಗಾಗ್ಗೆ',0,29,142),('143','ಕೆಲವೊಮ್ಮೆ',1,29,143),('144','ಅಪರೂಪ',0,29,144),('145','ಬಹುತೇಕ ಎಂದಿಗೂ',1,29,145),('146','ಹೆಚ್ಚಿನ ಸಮಯ',0,30,146),('147','ಆಗಾಗ್ಗೆ',1,30,147),('148','ಕೆಲವೊಮ್ಮೆ',1,30,148),('149','ಅಪರೂಪ',0,30,149),('15','Almost never',0,3,15),('150','ಬಹುತೇಕ ಎಂದಿಗೂ ',1,30,150),('16','Most of the time',1,4,16),('17','Often',0,4,17),('18','Sometimes',1,4,18),('19','Rarely',0,4,19),('2','Often',1,1,2),('20','Almost never',1,4,20),('21','Most of the time',0,5,21),('22','Often',1,5,22),('23','Sometimes',0,5,23),('24','Rarely',1,5,24),('25','Almost never',1,5,25),('26','Most of the time',0,6,26),('27','Often',1,6,27),('28','Sometimes',0,6,28),('29','Rarely',1,6,29),('3','Sometimes',0,1,3),('30','Almost never',0,6,30),('31','Most of the time',1,7,31),('32','Often',0,7,26),('33','Sometimes',1,7,33),('34','Rarely',0,7,34),('35','Almost never',1,7,35),('36','Most of the time',0,8,36),('37','Often',1,8,37),('38','Sometimes',0,8,38),('39','Rarely',1,8,39),('4','Rarely',1,1,4),('40','Almost never',0,8,40),('41','Most of the time',1,9,41),('42','Often',0,9,42),('43','Sometimes',1,9,43),('44','Rarely',0,9,44),('45','Almost never',1,9,45),('46','Most of the time',1,10,46),('47','Often',0,10,47),('48','Sometimes',1,10,48),('49','Rarely',0,10,49),('5','Almost never',0,1,5),('50','Almost never',1,10,50),('51','Most of the time',0,11,51),('52','Often',1,11,52),('53','Sometimes',0,11,53),('54','Rarely',1,11,54),('55','Almost never',0,11,55),('56','Most of the time',1,12,56),('57','Often',0,12,57),('58','Sometimes',1,12,58),('59','Rarely',0,12,59),('6','Most of the time',0,2,6),('60','Almost never',1,12,60),('61','Most of the time',0,13,61),('62','Often',1,13,62),('63','Sometimes',0,13,63),('64','Rarely',1,13,64),('65','Almost never',0,13,65),('66','Most of the time',1,14,66),('67','Often',0,14,67),('68','Sometimes',1,14,68),('69','Rarely',0,14,69),('7','Often',1,2,7),('70','Almost never',1,14,70),('71','Most of the time',0,15,71),('72','Often',1,15,72),('73','Sometimes',1,15,73),('74','Rarely',0,15,74),('75','Almost never',1,15,75),('76','ಹೆಚ್ಚಿನ ಸಮಯ',0,16,76),('77','ಆಗಾಗ್ಗೆ',1,16,77),('78','ಕೆಲವೊಮ್ಮೆ',0,16,78),('79','ಅಪರೂಪ',1,16,79),('8','Sometimes',0,2,8),('80','ಬಹುತೇಕ ಎಂದಿಗೂ',0,16,80),('81','ಹೆಚ್ಚಿನ ಸಮಯ',0,17,81),('82','ಆಗಾಗ್ಗೆ',1,17,82),('83','ಕೆಲವೊಮ್ಮೆ',0,17,83),('84','ಅಪರೂಪ',1,17,84),('85','ಬಹುತೇಕ ಎಂದಿಗೂ',0,17,85),('86','ಹೆಚ್ಚಿನ ಸಮಯ',1,18,86),('87','ಆಗಾಗ್ಗೆ',0,18,87),('88','ಕೆಲವೊಮ್ಮೆ',1,18,88),('89','ಅಪರೂಪ',1,18,89),('9','Rarely',1,2,9),('90','ಬಹುತೇಕ ಎಂದಿಗೂ',0,18,90),('91','ಹೆಚ್ಚಿನ ಸಮಯ',1,19,91),('92','ಆಗಾಗ್ಗೆ',0,19,92),('93','ಕೆಲವೊಮ್ಮೆ',1,19,93),('94','ಅಪರೂಪ',0,19,94),('95','ಬಹುತೇಕ ಎಂದಿಗೂ',1,19,95),('96','ಹೆಚ್ಚಿನ ಸಮಯ',0,20,96),('97','ಆಗಾಗ್ಗೆ',1,20,97),('98','ಕೆಲವೊಮ್ಮೆ',0,20,98),('99','ಅಪರೂಪ',1,20,99);
/*!40000 ALTER TABLE `lifestylequestionoptions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:45:20
