-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `masteremployeereminders`
--

DROP TABLE IF EXISTS `masteremployeereminders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `masteremployeereminders` (
  `employeeID` varchar(45) DEFAULT NULL,
  `clientID` varchar(45) DEFAULT NULL,
  `reminderID` varchar(45) DEFAULT NULL,
  `reminderName` varchar(45) DEFAULT NULL,
  `reminderDate` varchar(45) DEFAULT NULL,
  `reminderTime` varchar(45) DEFAULT NULL,
  `languageID` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `masteremployeereminders`
--

LOCK TABLES `masteremployeereminders` WRITE;
/*!40000 ALTER TABLE `masteremployeereminders` DISABLE KEYS */;
INSERT INTO `masteremployeereminders` VALUES ('emp1','AD163P','RM1','You have to drink 8 liters of water.','2019-05-19','10:45:00','en'),('emp1','AD163P','RM2','Walk a 4000 steps','2019-06-25','10:28:00','en'),('emp1','AD163P','RM3','Have a glass of milk','2019-07-15','06:45:00','en'),('emp1','AD163P','RM4','Drink a apple juice','2019-07-30','10:12:00','en'),('emp1','AD163P','RM5','Do cardio','2019-04-15','01:45:00','en'),('emp1','AD163P','RM6','Sleep for minimum 8 hours','2019-07-19','05:45:00','en'),('emp1','AD163P','RM7','Have vegetables','2019-07-15','12:45:00','en'),('emp1','AD163P','RM8','Have fruits its good for health','2019-08-15','10:05:00','en'),('emp1','AD163P','RM9','Don\'t drink sugar calories','2019-09-15','18:25:00','en'),('emp1','AD163P','RM10','Eat fatty fish ','2019-03-15','10:55:00','en'),('emp1','AD163P','RM1','ನೀವು 8 ಲೀಟರ್ ನೀರನ್ನು ಕುಡಿಯಬೇಕು.','2019-05-19','10:45:00','kn'),('emp1','AD163P','RM2','4000 ಮೆಟ್ಟಿಲುಗಳು ನಡೆಯಿರಿ','2019-06-25','10:28:00','kn'),('emp1','AD163P','RM3','ಒಂದು ಲೋಟ ಹಾಲು ಸೇವಿಸಿ','2019-07-15','06:45:00','kn'),('emp1','AD163P','RM4','ಸೇಬಿನ ರಸವನ್ನು ಕುಡಿಯಿರಿ','2019-07-30','10:12:00','kn'),('emp1','AD163P','RM5','ಕಾರ್ಡಿಯೋ ಮಾಡಿ','2019-04-15','01:45:00','kn'),('emp1','AD163P','RM6','ಕನಿಷ್ಠ 8 ಗಂಟೆಗಳ ಕಾಲ ನಿದ್ರೆ ಮಾಡಿ','2019-07-19','05:45:00','kn'),('emp1','AD163P','RM7','ತರಕಾರಿಗಳನ್ನು ಹೊಂದಿರಿ','2019-07-15','12:45:00','kn'),('emp1','AD163P','RM8','ಹಣ್ಣುಗಳು ಆರೋಗ್ಯಕ್ಕೆ ಒಳ್ಳೆಯದು','2019-08-15','10:05:00','kn'),('emp1','AD163P','RM9','ಸಕ್ಕರೆ ಕ್ಯಾಲೊರಿಗಳನ್ನು ಕುಡಿಯಬೇಡಿ','2019-09-15','18:25:00','kn'),('emp1','AD163P','RM10','ಕೊಬ್ಬಿನ ಮೀನು ತಿನ್ನಿರಿ','2019-03-15','10:55:00','kn');
/*!40000 ALTER TABLE `masteremployeereminders` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:46:44
