-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `enrolled`
--

DROP TABLE IF EXISTS `enrolled`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `enrolled` (
  `employeeName` varchar(45) NOT NULL,
  `employeeID` varchar(45) NOT NULL,
  `clientID` varchar(45) NOT NULL,
  `locationID` varchar(45) NOT NULL,
  `branchID` varchar(45) NOT NULL,
  `projectID` varchar(45) NOT NULL,
  `teamID` varchar(45) NOT NULL,
  `groupPathID` varchar(45) NOT NULL,
  `testDate` date DEFAULT NULL,
  KEY `fk_c_iiii_idx` (`clientID`),
  KEY `fk_b_iiiiii_idx` (`branchID`),
  KEY `fk_p_iiii_idx` (`projectID`),
  KEY `fk_t_iiiii_idx` (`teamID`),
  KEY `fk_g_iiiiii_idx` (`groupPathID`),
  CONSTRAINT `fk_b_iiiiii` FOREIGN KEY (`branchID`) REFERENCES `masterclientbranch` (`branchID`),
  CONSTRAINT `fk_c_iiii` FOREIGN KEY (`clientID`) REFERENCES `masterclient` (`clientID`),
  CONSTRAINT `fk_g_iiiiii` FOREIGN KEY (`groupPathID`) REFERENCES `mastergrouppath` (`groupPathID`),
  CONSTRAINT `fk_p_iiii` FOREIGN KEY (`projectID`) REFERENCES `masterproject` (`projectID`),
  CONSTRAINT `fk_t_iiiii` FOREIGN KEY (`teamID`) REFERENCES `masterteam` (`teamID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enrolled`
--

LOCK TABLES `enrolled` WRITE;
/*!40000 ALTER TABLE `enrolled` DISABLE KEYS */;
/*!40000 ALTER TABLE `enrolled` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:47:08
