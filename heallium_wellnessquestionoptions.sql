-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wellnessquestionoptions`
--

DROP TABLE IF EXISTS `wellnessquestionoptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wellnessquestionoptions` (
  `optionID` varchar(45) NOT NULL,
  `optionText` varchar(250) NOT NULL,
  `points` int(11) NOT NULL,
  `questionID` int(11) NOT NULL,
  `actionID` int(11) NOT NULL,
  PRIMARY KEY (`optionID`),
  KEY `fk_actionID_wellnessquestion_idx` (`actionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wellnessquestionoptions`
--

LOCK TABLES `wellnessquestionoptions` WRITE;
/*!40000 ALTER TABLE `wellnessquestionoptions` DISABLE KEYS */;
INSERT INTO `wellnessquestionoptions` VALUES ('1','Most of the time',1,1,1),('10','1 – 3 ',1,3,10),('100','ಶಾಂತಿಯುತವಾಗಿ ಬದುಕುವುದು, ನೀವು ಪ್ರೀತಿಸುವವರೊಂದಿಗೆ ಆನಂದದಾಯಕ ಕ್ಷಣಗಳನ್ನು ಹಂಚಿಕೊಳ್ಳುವುದು',0,26,100),('101','ಕೆಲವರು ಅನುಸರಿಸುತ್ತಿರುವ ಕನಸು ಅಥವಾ ಭ್ರಮೆ',1,26,101),('102','ನಿಮಗೆ ಸಾಧ್ಯವಾದಾಗ ಉತ್ತಮ ಜೀವನವನ್ನು ಆನಂದಿಸಿ!',0,26,102),('103','0 - 3',1,26,103),('104','4 - 6',0,27,104),('105','7 - 9',1,27,105),('106','10 - 12',0,27,106),('107','1 - 2',1,27,107),('108','3 - 5',0,28,108),('109','6 - 8',1,28,109),('11','4 – 7 ',1,3,11),('110','9 - 12',0,28,110),('111','ಮನಸ್ಸು, ದೇಹ ಮತ್ತು ಚೇತನದೊಳಗಿನ ಅತ್ಯುತ್ತಮ ಆರೋಗ್ಯದ ಸ್ಥಿತಿ',1,28,111),('112','ದೈಹಿಕವಾಗಿ ಅದ್ಭುತ ಭಾವನೆ',0,29,112),('113','ಅನಾರೋಗ್ಯಕ್ಕೆ ಒಳಗಾಗದ ಸ್ಥಿತಿ',1,29,113),('114','ಉತ್ತಮ ದೈಹಿಕ ಆರೋಗ್ಯದ ಸ್ಥಿತಿ',0,29,114),('115','ಧ್ಯಾನ',1,29,115),('116','ಅಡುಗೆ',0,30,116),('117','ಓದುವಿಕೆ ಮತ್ತು ಬರವಣಿಗೆ',1,30,117),('118','ಸ್ವಯಂಸೇವಕ ಕೆಲಸ',0,30,118),('12','More ',1,3,12),('13','0 – 2 ',0,4,13),('14','2 – 4 ',1,4,14),('15','4 – 6 ',0,4,15),('16','More ',1,4,16),('17','Almost always ',1,5,17),('18','Sometimes ',0,5,18),('19','Occasionally ',0,5,19),('2','Sometimes',0,1,2),('20','Almost never ',1,5,20),('21','Almost always ',1,6,21),('22','Sometimes ',0,6,22),('23','Occasionally ',1,6,23),('24','Almost never ',0,6,24),('25','Emotion ',1,7,25),('26','Attitude ',0,7,26),('27','Anxiety ',1,7,27),('28','Running, to feel your muscles in action with a good sweat ',0,8,28),('29','The swimming-pool, to float & swim at your own pace ',1,8,29),('3','Rarely',0,1,3),('30','A long walk on a mountainside, to get some oxygen & peaceful, away time ',0,8,30),('31','A stroll in a nearby forest, with a family picnic or with friends ',1,8,31),('32','Agitated: so many emails to follow up onâ€',0,9,32),('33','Fresh : ready to start the week after your meditation session',0,9,33),('34','Depressed: Monday is so far away from the week-end!',1,9,34),('35','Focused: not to miss any appointment, since people are depending on you!',0,9,35),('36','I am proud of what I have accomplished so far',1,10,36),('37','I am happy that I could help people around me',0,10,37),('38','I did not have as much luck in life as I hoped',1,10,38),('39','I have enjoyed some great moments, but what have I really accomplished',0,10,39),('4','Almost never ',1,1,4),('40','Feeling useful and finding a way to help those you love',1,11,40),('41','Living peacefully, sharing blissful moments with those you love',0,11,41),('42','A dream or illusion that some are pursuing',1,11,42),('43','Enjoying the good life while you can!',0,11,43),('44','0 â€“ 3',1,12,44),('45','4 â€“ 6',0,12,45),('46','7 â€“ 9',1,12,46),('47','10 â€“ 12',0,12,47),('48','1 â€“ 2',1,13,48),('49','3 â€“ 5',0,13,49),('5','Family/Friends',1,2,5),('50','6 â€“ 8',1,13,50),('51','9 â€“ 12',0,13,51),('52','The state of optimal health within the mind, body, and spirit',1,14,52),('53','Feeling fantastic physically',0,14,53),('54','The state of not being ill',1,14,54),('55','The state of great physical health',0,14,55),('56','Meditation',1,15,56),('57','Cooking',0,15,57),('58','Reading & Writing',1,15,58),('59','Volunteer work',0,15,59),('6','Relationship Issue',0,2,6),('60','ಹೆಚ್ಚಿನ ಸಮಯ',1,16,60),('61','ಕೆಲವೊಮ್ಮೆ',0,16,61),('62','ಅಪರೂಪ',0,16,62),('63','ಬಹುತೇಕ ಎಂದಿಗೂ',1,16,63),('64','ಕುಟುಂಬ / ಸ್ನೇಹಿತರು',1,17,64),('65','ಸಂಬಂಧ ಸಂಚಿಕೆ',0,17,65),('66','ಹಣಕಾಸು',0,17,66),('67','ಕೆಲಸ',1,17,67),('68','0',0,18,68),('69','1 - 3',1,18,69),('7','Financial',0,2,7),('70','4 - 7',1,18,70),('71','ಇನ್ನಷ್ಟು',1,18,71),('72','0 - 2',0,19,72),('73','2 - 4',1,19,73),('74','4 - 6',0,19,74),('75','ಇನ್ನಷ್ಟು',1,19,75),('76','ಬಹುತೇಕ ಯಾವಾಗಲೂ',1,20,76),('77','ಕೆಲವೊಮ್ಮೆ',0,20,77),('78','ಕೆಲವೊಮ್ಮೆ',0,20,78),('79','ಬಹುತೇಕ ಎಂದಿಗೂ',1,20,79),('8','Work ',1,2,8),('80','ಬಹುತೇಕ ಯಾವಾಗಲೂ',1,21,80),('81','ಕೆಲವೊಮ್ಮೆ',0,21,81),('82','ಕೆಲವೊಮ್ಮೆ',1,21,82),('83','ಬಹುತೇಕ ಎಂದಿಗೂ',0,21,83),('84','ಭಾವನೆ',1,22,84),('85','ವರ್ತನೆ',0,22,85),('86','ಆತಂಕ',1,22,86),('87','ಚಾಲನೆಯಲ್ಲಿದೆ, ಉತ್ತಮ ಬೆವರಿನೊಂದಿಗೆ ನಿಮ್ಮ ಸ್ನಾಯುಗಳನ್ನು ಅನುಭವಿಸಲು',0,22,87),('88','ನಿಮ್ಮ ಸ್ವಂತ ವೇಗದಲ್ಲಿ ತೇಲುವ ಮತ್ತು ಈಜಲು ಈಜುಕೊಳ',1,23,88),('89','ಸ್ವಲ್ಪ ಆಮ್ಲಜನಕ ಮತ್ತು ಶಾಂತಿಯುತ ಸಮಯವನ್ನು ಪಡೆಯಲು ಪರ್ವತದ ಪಕ್ಕದಲ್ಲಿ ಸುದೀರ್ಘ ನಡಿಗೆ',0,23,89),('9','0',0,3,9),('90','ಕುಟುಂಬ ಪಿಕ್ನಿಕ್ ಅಥವಾ ಸ್ನೇಹಿತರೊಂದಿಗೆ ಹತ್ತಿರದ ಕಾಡಿನಲ್ಲಿ ಅಡ್ಡಾಡು',1,23,90),('91','ಆಕ್ರೋಶಗೊಂಡಿದೆ: ಅನುಸರಿಸಲು ಹಲವು ಇಮೇಲ್‌ಗಳು',0,23,91),('92','ತಾಜಾ: ನಿಮ್ಮ ಧ್ಯಾನ ಅಧಿವೇಶನದ ನಂತರ ವಾರವನ್ನು ಪ್ರಾರಂಭಿಸಲು ಸಿದ್ಧವಾಗಿದೆ',0,24,92),('93','ಖಿನ್ನತೆಗೆ ಒಳಗಾದವರು: ಸೋಮವಾರವು ವಾರಾಂತ್ಯದಿಂದ ದೂರವಿದೆ!',1,24,93),('94','ಕೇಂದ್ರೀಕರಿಸಲಾಗಿದೆ: ಜನರು ನಿಮ್ಮನ್ನು ಅವಲಂಬಿಸಿರುವುದರಿಂದ ಯಾವುದೇ ನೇಮಕಾತಿಯನ್ನು ತಪ್ಪಿಸಿಕೊಳ್ಳಬಾರದು!',0,24,94),('95','ನಾನು ಇಲ್ಲಿಯವರೆಗೆ ಸಾಧಿಸಿದ್ದಕ್ಕೆ ಹೆಮ್ಮೆ ಇದೆ',1,24,95),('96','ನನ್ನ ಸುತ್ತಮುತ್ತಲಿನ ಜನರಿಗೆ ನಾನು ಸಹಾಯ ಮಾಡಬಹುದೆಂದು ನನಗೆ ಸಂತೋಷವಾಗಿದೆ',0,25,96),('97','ನಾನು ಅಂದುಕೊಂಡಷ್ಟು ಜೀವನದಲ್ಲಿ ನನಗೆ ಅದೃಷ್ಟ ಇರಲಿಲ್ಲ',1,25,97),('98','ನಾನು ಕೆಲವು ಉತ್ತಮ ಕ್ಷಣಗಳನ್ನು ಆನಂದಿಸಿದೆ, ಆದರೆ ನಾನು ನಿಜವಾಗಿಯೂ ಏನು ಸಾಧಿಸಿದ್ದೇನೆ',0,25,98),('99','ಉಪಯುಕ್ತವೆಂದು ಭಾವಿಸುವುದು ಮತ್ತು ನೀವು ಪ್ರೀತಿಸುವವರಿಗೆ ಸಹಾಯ ಮಾಡುವ ಮಾರ್ಗವನ್ನು ಕಂಡುಕೊಳ್ಳುವುದು',1,25,99);
/*!40000 ALTER TABLE `wellnessquestionoptions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:45:10
