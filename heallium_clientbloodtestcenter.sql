-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clientbloodtestcenter`
--

DROP TABLE IF EXISTS `clientbloodtestcenter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `clientbloodtestcenter` (
  `clientID` varchar(45) NOT NULL,
  `centerID` varchar(45) NOT NULL,
  `languageID` varchar(45) DEFAULT NULL,
  KEY `fk_client_centerID_idx` (`centerID`),
  KEY `fk_clientID_centeID_idx` (`clientID`),
  CONSTRAINT `fk_client_blood_center` FOREIGN KEY (`clientID`) REFERENCES `masterclient` (`clientID`),
  CONSTRAINT `fk_client_centerID` FOREIGN KEY (`centerID`) REFERENCES `masterbloodtestcenter` (`centerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientbloodtestcenter`
--

LOCK TABLES `clientbloodtestcenter` WRITE;
/*!40000 ALTER TABLE `clientbloodtestcenter` DISABLE KEYS */;
INSERT INTO `clientbloodtestcenter` VALUES ('AD163P','2','en'),('AD163P','1','en'),('AD163P','3','en'),('AD163P','4','en'),('AD163P','5','en'),('AD163P','6','en'),('AD163P','1a','kn'),('AD163P','1b','kn'),('AD163P','1c','kn'),('AD163P','1d','kn'),('AD163P','1e','kn'),('AD163P','1f','kn');
/*!40000 ALTER TABLE `clientbloodtestcenter` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:45:56
