-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employermasterlabels`
--

DROP TABLE IF EXISTS `employermasterlabels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `employermasterlabels` (
  `languageID` varchar(45) NOT NULL,
  `labelName` varchar(200) NOT NULL,
  `labelValue` varchar(500) NOT NULL,
  `clientID` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employermasterlabels`
--

LOCK TABLES `employermasterlabels` WRITE;
/*!40000 ALTER TABLE `employermasterlabels` DISABLE KEYS */;
INSERT INTO `employermasterlabels` VALUES ('en','heallium','Heallium','AD163P'),('en','filterby','Filter by:','AD163P'),('en','home','HOME','AD163P'),('en','aboutUs','ABOUT US','AD163P'),('en','support','SUPPORT','AD163P'),('en','search','SEARCH','AD163P'),('en','leaderBoard','LEADERBOARD','AD163P'),('en','employee','Employee','AD163P'),('en','score','Score','AD163P'),('en','project','Project','AD163P'),('en','team','Team','AD163P'),('en','healthIndexReport','Health Index Report','AD163P'),('en','stressIndecReport','Stress Index Report','AD163P'),('en','lifestyleReport','LifeStyle Report','AD163P'),('en','wellnessReport','Wellness Report','AD163P'),('en','physicalReport','Physical Report','AD163P'),('en','employeeEnrollment','Employee-Enrollment','AD163P'),('en','highestRecordedHIS','Highest Recorded HIS ','AD163P'),('en','averageHIS','Avarage HIS ','AD163P'),('en','lowestRecordeHIS','Lowest Recorded HIS ','AD163P'),('en','highestRecordedStress','Highest Recorded Stress ','AD163P'),('en','averageStress','Average Stress ','AD163P'),('en','lowestRecordeStress','Lowest Recorded Stress ','AD163P'),('en','highestRecordedLifestyle','Highest Recorded Lifestyle ','AD163P'),('en','averageLifestyle','Average Lifestyle ','AD163P'),('en','lowestRecordeLifestyle','Lowest Recorded Lifestyle ','AD163P'),('en','highestRecordedWellness','Highest Recorded Wellness ','AD163P'),('en','averageWellness','Average Wellness ','AD163P'),('en','lowestRecordedWellness','Lowest Recorded Wellness ','AD163P'),('en','highestRecordedScore','Highest Recorded Score ','AD163P'),('en','averageScore','Average Score ','AD163P'),('en','lowestRecordeScore','Lowest Recorded Score ','AD163P'),('en','notYetEnrolled','Not Yet Enrolled ','AD163P'),('en','enrolled','Enrolled ','AD163P'),('en','testCompleted','Test Completed ','AD163P'),('en','location','LOCATION','AD163P'),('en','employeeID','EMPLOYEE ID','AD163P'),('en','employeeName','EMPLOYEE NAME','AD163P'),('en','previousScore','PREVIOUS SCORE ','AD163P'),('en','currentSScore','CURRENT SCORE','AD163P'),('en','percentageChange','PERCENTAGE CHANGE(%) ','AD163P'),('en','sendMessage','SEND MESSAGE ','AD163P'),('en','send','Send ','AD163P'),('en','pushNotification','Push Notification ','AD163P'),('en','e_mail','E-mail ','AD163P'),('en','reSet','RESET','AD163P'),('en','setting','SETTING','AD163P'),('en','Log-Out','Log-Out','AD163P'),('en','account','Account','AD163P'),('en','editAccount','Edit your account settings and change your password here.','AD163P'),('en','firstName','First Name ','AD163P'),('en','lastName','Last Name ','AD163P'),('en','contactNumber','Conct Number ','AD163P'),('en','oldPassword','Old Password ','AD163P'),('en','newPassword','New Password','AD163P'),('en','confirmPassword','Confirm Password','AD163P'),('en','save','SAVE','AD163P'),('en','photo','PHOTO','AD163P'),('en','addProfile','Add a photo of yourself for profile','AD163P'),('en','saveImage','SAVE IMAGE','AD163P'),('en','address','Address','AD163P'),('en','sendTo','Send To','AD163P'),('en','meassage','Message','AD163P'),('en','ok','OK','AD163P'),('en','cancel','CANCEL','AD163P'),('en','withoutFilter','Without Filter','AD163P'),('en','withFilter','With Filter','AD163P'),('en','hideReport','HideReport','AD163P'),('en','showReport','ShowReport','AD163P');
/*!40000 ALTER TABLE `employermasterlabels` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:45:29
