-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `masterphysiotherapyquestions`
--

DROP TABLE IF EXISTS `masterphysiotherapyquestions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `masterphysiotherapyquestions` (
  `questionID` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(300) NOT NULL,
  `optionFormat` int(11) NOT NULL,
  PRIMARY KEY (`questionID`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `masterphysiotherapyquestions`
--

LOCK TABLES `masterphysiotherapyquestions` WRITE;
/*!40000 ALTER TABLE `masterphysiotherapyquestions` DISABLE KEYS */;
INSERT INTO `masterphysiotherapyquestions` VALUES (1,'How would you describe your body weight',0),(2,'How many days a week do you do some form of exercise',0),(3,'Can you touch your toes',0),(4,'Could you walk along a straight line, like the \"Walk and Turn\"\" field sobriety test\"',0),(5,'If you went out for a jog, how far do you think you could go before you had to stop for a rest',0),(6,'How many push-ups do you think you could do',0),(7,'How would you go if you had to move some heavy furniture around the house',0),(8,'What could you jump over',0),(9,'If you had your purse/wallet stolen, would you be able to chase down the robber',0),(10,'Confronted with a flight of stairs, would you ...',0),(11,'Do you suffer from',0),(12,'Running, swimming, and using an elliptical machine are all ways to improve your',0),(13,'The best fitness foods include all except               ',0),(14,'To make your muscles strong, you must           every day.',0),(15,'Exercise is good for',0),(16,'ನಿಮ್ಮ ದೇಹದ ತೂಕವನ್ನು ನೀವು ಹೇಗೆ ವಿವರಿಸುತ್ತೀರಿ',0),(17,'ವಾರದಲ್ಲಿ ಎಷ್ಟು ದಿನ ನೀವು ಕೆಲವು ರೀತಿಯ ವ್ಯಾಯಾಮ ಮಾಡುತ್ತೀರಿ',0),(18,'ನಿಮ್ಮ ಕಾಲ್ಬೆರಳುಗಳನ್ನು ಸ್ಪರ್ಶಿಸಬಹುದೇ?',0),(19,'ವಾಕ್ ಅಂಡ್ ಟರ್ನ್ ಕ್ಷೇತ್ರ ಸಮಚಿತ್ತತೆ ಪರೀಕ್ಷೆಯಂತೆ ನೀವು ಸರಳ ರೇಖೆಯಲ್ಲಿ ನಡೆಯಬಹುದೇ?',0),(20,'ನೀವು ಜೋಗಕ್ಕಾಗಿ ಹೊರಗೆ ಹೋದರೆ, ನೀವು ವಿಶ್ರಾಂತಿಗಾಗಿ ನಿಲ್ಲುವ ಮೊದಲು ನೀವು ಎಷ್ಟು ದೂರ ಹೋಗಬಹುದು ಎಂದು ನೀವು ಭಾವಿಸುತ್ತೀರಿ',0),(21,'ನೀವು ಎಷ್ಟು ಪುಷ್-ಅಪ್‌ಗಳನ್ನು ಮಾಡಬಹುದು ಎಂದು ನೀವು ಭಾವಿಸುತ್ತೀರಿ',0),(22,'ನೀವು ಮನೆಯ ಸುತ್ತ ಕೆಲವು ಹೆವಿ ಪೀಠೋಪಕರಣಗಳನ್ನು ಸ್ಥಳಾಂತರಿಸಬೇಕಾದರೆ ನೀವು ಹೇಗೆ ಹೋಗುತ್ತೀರಿ',0),(23,'ನೀವು ಏನು ಜಿಗಿಯಬಹುದು',0),(24,'ನಿಮ್ಮ ಪರ್ಸ್/ವ್ಯಾಲೆಟ್ ಕದ್ದಿದ್ದರೆ, ನೀವು ದರೋಡೆಕೋರನನ್ನು ಓಡಿಸಲು ಸಾಧ್ಯವಾಗುತ್ತದೆ',0),(25,'ಮೆಟ್ಟಿಲುಗಳ ಹಾರಾಟವನ್ನು ಎದುರಿಸಿದರೆ, ನೀವು ...',0),(26,'ನೀವು ಬಳಲುತ್ತಿರುವಿರಾ',0),(27,'ಚಾಲನೆಯಲ್ಲಿರುವ, ಈಜುವ ಮತ್ತು ಅಂಡಾಕಾರದ ಯಂತ್ರವನ್ನು ಬಳಸುವುದು ನಿಮ್ಮ ಸುಧಾರಣೆಯ ಎಲ್ಲಾ ಮಾರ್ಗಗಳು',0),(28,'ಅತ್ಯುತ್ತಮ ಫಿಟ್‌ನೆಸ್ ಆಹಾರಗಳು ಹೊರತುಪಡಿಸಿ ಎಲ್ಲವನ್ನೂ ಒಳಗೊಂಡಿವೆ',0),(29,'ನಿಮ್ಮ ಸ್ನಾಯುಗಳನ್ನು ಬಲಪಡಿಸಲು, ನೀವು ಪ್ರತಿದಿನವೂ ಮಾಡಬೇಕು.',0),(30,'ವ್ಯಾಯಾಮ ಒಳ್ಳೆಯದು',0);
/*!40000 ALTER TABLE `masterphysiotherapyquestions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:45:43
