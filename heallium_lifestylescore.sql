-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lifestylescore`
--

DROP TABLE IF EXISTS `lifestylescore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `lifestylescore` (
  `lifeMonth` varchar(45) NOT NULL,
  `maxLifeScore` int(11) NOT NULL,
  `minLifeScore` int(11) NOT NULL,
  `avgLifeScore` int(11) NOT NULL,
  `clientID` varchar(45) NOT NULL,
  `locationID` varchar(45) NOT NULL,
  `branchID` varchar(45) NOT NULL,
  `projectID` varchar(45) NOT NULL,
  `teamID` varchar(45) NOT NULL,
  `groupPathID` varchar(45) NOT NULL,
  `testDate` date DEFAULT NULL,
  KEY `fk_clientID_lifescore_idx` (`clientID`),
  KEY `fk_b_l_idx` (`branchID`),
  KEY `fk_p_l_idx` (`projectID`),
  KEY `fk_t_l_idx` (`teamID`),
  KEY `fk_g_l_idx` (`groupPathID`),
  CONSTRAINT `fk_b_l` FOREIGN KEY (`branchID`) REFERENCES `masterclientbranch` (`branchID`),
  CONSTRAINT `fk_clientID_lifescore` FOREIGN KEY (`clientID`) REFERENCES `masterclient` (`clientID`),
  CONSTRAINT `fk_g_l` FOREIGN KEY (`groupPathID`) REFERENCES `mastergrouppath` (`groupPathID`),
  CONSTRAINT `fk_p_l` FOREIGN KEY (`projectID`) REFERENCES `masterproject` (`projectID`),
  CONSTRAINT `fk_t_l` FOREIGN KEY (`teamID`) REFERENCES `masterteam` (`teamID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lifestylescore`
--

LOCK TABLES `lifestylescore` WRITE;
/*!40000 ALTER TABLE `lifestylescore` DISABLE KEYS */;
INSERT INTO `lifestylescore` VALUES ('JAN-FEB',5,10,5,'AD163P','553471','M742187','B1744','AT77S','IF51J','2019-02-10'),('MAR-APR',10,7,3,'AD163P','553471','M742187','B1744','AT77S','IF51J','2019-04-10'),('MAY-JUN',3,7,10,'AD163P','553471','M742187','B1744','AT77S','IF51J','2019-06-10'),('JUL-AUG',10,3,7,'AD163P','553471','M742187','B1744','AT77S','IF51J','2019-08-10'),('SEP-OCT',5,10,5,'AD163P','553471','M742187','B1744','AT77S','IF51J','2019-10-10'),('NOV-DEC',4,6,10,'AD163P','553471','M742187','B1744','AT77S','IF51J','2019-12-10'),('JAN-FEB',10,4,6,'AD163P','553471','M742187','B1744','AT97I','ASP32K','2019-02-11'),('MAR-APR',3,10,7,'AD163P','553471','M742187','B1744','AT97I','ASP32K','2019-04-11'),('MAY-JUN',6,7,7,'AD163P','553471','M742187','B1744','AT97I','ASP32K','2019-06-11'),('JUL-AUG',6,8,6,'AD163P','553471','M742187','B1744','AT97I','ASP32K','2019-08-11'),('SEP-OCT',5,9,6,'AD163P','553471','M742187','B1744','AT97I','ASP32K','2019-10-11'),('NOV-DEC',10,2,8,'AD163P','553471','M742187','B1744','AT97I','ASP32K','2019-12-11');
/*!40000 ALTER TABLE `lifestylescore` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:46:14
