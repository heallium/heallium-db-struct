-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `masterbloodtestcenter`
--

DROP TABLE IF EXISTS `masterbloodtestcenter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `masterbloodtestcenter` (
  `centerID` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `city` varchar(45) NOT NULL,
  `phoneNumber` varchar(20) NOT NULL,
  `workingDay` varchar(100) NOT NULL,
  PRIMARY KEY (`centerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `masterbloodtestcenter`
--

LOCK TABLES `masterbloodtestcenter` WRITE;
/*!40000 ALTER TABLE `masterbloodtestcenter` DISABLE KEYS */;
INSERT INTO `masterbloodtestcenter` VALUES ('1','On Scans and Labs','Bengaluru','7878292625','Sunday-Tuesday'),('1a','ಸ್ಕ್ಯಾನ್‌ಗಳು ಮತ್ತು ಲ್ಯಾಬ್‌ಗಳಲ್ಲಿ','ಬೆಂಗಳೂರು ','7878292625','ಭಾನುವಾರ-ಮಂಗಳವಾರ'),('1b','ಥೈರೋಕೇರ್ ಸಂಗ್ರಹ ಕೇಂದ್ರ','ಬೆಂಗಳೂರು ','7676542542','ಸೋಮವಾರ-ಗುರುವಾರ'),('1c','ರವಿ ಲ್ಯಾಬ್ಸ್ ಮತ್ತು ಸಂಶೋಧನಾ ಕೇಂದ್ರ','ಬೆಂಗಳೂರು ','7575422648','ಶುಕ್ರವಾರ-ಭಾನುವಾರ'),('1d','ದಯಾ ಲ್ಯಾಬ್ಸ್','ಬೆಂಗಳೂರು ','7825261213','ಭಾನುವಾರ-ಬುಧವಾರ'),('1e','ಕ್ವೆಸ್ಟ್ ಡಯಾಗ್ನೋಸ್ಟಿಕ್ಸ್','ಬೆಂಗಳೂರು ','080 4150 422','ಗುರುವಾರ-ಶನಿವಾರ'),('1f','ಥೈರೋಕೇರ್ ಹೋಮ್ ಸರ್ವಿಸ್ ','ಬೆಂಗಳೂರು ','8095514400','ಶನಿವಾರ-ಮಂಗಳವಾರ'),('2','Thyrocare Collection center','Bengaluru','7676542542','Monday-Thursday'),('3','Ravi Labs and Research Center','Bengaluru','7575422648','Friday-Sunday'),('4','Daya Labs','Bengaluru','7825261213','Sunday-Wensday'),('5','Quest Diagnostics','Bengaluru','080 4150 422','Thursday-Saturday'),('6','Thyrocare Home Service','Bengaluru','08095514400','Satuarday-Tuesday');
/*!40000 ALTER TABLE `masterbloodtestcenter` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:45:04
