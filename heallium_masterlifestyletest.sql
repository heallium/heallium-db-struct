-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `masterlifestyletest`
--

DROP TABLE IF EXISTS `masterlifestyletest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `masterlifestyletest` (
  `lifeStyleTestID` varchar(45) NOT NULL,
  `lifeStyleTestQuestion` varchar(150) NOT NULL,
  `lifeStylleTestQuestionOption1` varchar(45) NOT NULL,
  `lifeStylleTestQuestionOption2` varchar(45) NOT NULL,
  `lifeStylleTestQuestionOption3` varchar(45) NOT NULL,
  `lifeStylleTestQuestionOption4` varchar(45) NOT NULL,
  `lifeStylleTestQuestionOption5` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `masterlifestyletest`
--

LOCK TABLES `masterlifestyletest` WRITE;
/*!40000 ALTER TABLE `masterlifestyletest` DISABLE KEYS */;
INSERT INTO `masterlifestyletest` VALUES ('LFID1','Having a daily routine is','Oppressive and stifling','Annoying and limiting','Sometimes a good thing, sometimes not','Helpful and comforting','Totally necessary'),('LFID2','How long will this incident linger in your mind','I let it go immediately','Less than an hour','A couple of hours','More than one day','More than one week'),('LFID3','How would you feel?','I wouldnt feel angry at all','I would feel slightly annoyed','I would feel a little angry','I would feel moderately angry','I would feel very angry'),('LFID4','How long will this incident linger in your mind after its all over','I dont think about it again','Less than 1 hour','A couple of hours','The rest of the day','A day or two'),('LFID5','How would you feel','I wouldnt feel angry at all','I would feel slightly annoyed','I would feel a little angry','I would feel moderately angry','I would feel very angry'),('LFID6','You overhear a friend badmouthing you. How angry does that make you feel','I dont feel angry at all','I feel slightly annoyed','I feel a little angry','I feel moderately angry','I feel very angry'),('LFID7','How often are you late for work or an appointment','Quite often','Often','Sometimes','Rarely','Almost never'),('LFID8','How often do you catch yourself daydreaming at work','Quite often','Often','Sometimes','Rarely','Almost never'),('LFID9','When reading a book or magazine, how often do you find yourself re-reading the same paragraph or skipping ahead','Quite often','Often','Sometimes','Rarely','Almost never'),('LFID10','How often do you interrupt people during conversations','Quite often','Often','Sometimes','Rarely','Almost never');
/*!40000 ALTER TABLE `masterlifestyletest` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:47:02
