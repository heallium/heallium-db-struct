-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wellnessmeasures`
--

DROP TABLE IF EXISTS `wellnessmeasures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wellnessmeasures` (
  `measuresID` varchar(45) DEFAULT NULL,
  `wellnessMeasuresDetails` varchar(850) DEFAULT NULL,
  `employeeID` varchar(45) DEFAULT NULL,
  `clientID` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wellnessmeasures`
--

LOCK TABLES `wellnessmeasures` WRITE;
/*!40000 ALTER TABLE `wellnessmeasures` DISABLE KEYS */;
INSERT INTO `wellnessmeasures` VALUES ('1','Use plenty of herbs and spices. ','emp1','AD163P'),('2','Give up soft drinks and avoid microwaved food.','emp1','AD163P'),('3','Go for Ginger,Garlic and Tulsi.','emp1','AD163P'),('4','Avoid packaged and frozen foods.','emp1','AD163P'),('5','Bring on the broccoli and green veggies.','emp1','AD163P'),('6','Prefer Green Tea. ','emp2','AD163P'),('7','Try having  Ashwagandha and Valerian supplements. ','emp2','AD163P'),('8','Schedule a wellness check and stick to it. ','emp2','AD163P'),('9','Schedule a dental exam or cleaning – and stick to it.','emp2','AD163P'),('10','Give up soft drinks – and opt for a healthier beverage option.','emp2','AD163P'),('11','Take a CPR or First Aid class.','emp3','AD163P'),('12','Set a quit date to stop smoking – and stick to it.','emp3','AD163P'),('13','Go for a brisk 20-minute walk every day for one week. Then try it again the next week.','emp3','AD163P'),('14','Ask your doctor about cholesterol and blood pressure screenings.','emp3','AD163P'),('15','Go an entire week without eating fast food. See if you can do it again the next week.','emp3','AD163P');
/*!40000 ALTER TABLE `wellnessmeasures` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:45:37
