-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `masterwellnessquestions`
--

DROP TABLE IF EXISTS `masterwellnessquestions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `masterwellnessquestions` (
  `questionID` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(250) NOT NULL,
  `optionFormat` int(11) NOT NULL,
  `languaeID` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`questionID`),
  UNIQUE KEY `questionID_UNIQUE` (`questionID`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `masterwellnessquestions`
--

LOCK TABLES `masterwellnessquestions` WRITE;
/*!40000 ALTER TABLE `masterwellnessquestions` DISABLE KEYS */;
INSERT INTO `masterwellnessquestions` VALUES (1,'During my usual day, I am physically active (I take the stairs instead of the elevator, I walk)',0,'en'),(2,'What do you consider the main source of your stress ',0,'en'),(3,'How many cups of coffee or caffeinated beverages do you normally consume daily ',0,'en'),(4,'A How many litres of water do you normally drink daily ',0,'en'),(5,'I try to be less judgmental whenÂ feeling angry at myself ',0,'en'),(6,'I try to become more peacefulÂ when Iâ€™m feeling anxious ',0,'en'),(7,'Feeling that results in both physical and mental reactions ',0,'en'),(8,'You are thinking of getting some exercise, you choose ',0,'en'),(9,'Monday morning, before you head to work, you feel ',0,'en'),(10,'When you reflect upon your life, you think ',0,'en'),(11,'In your opinion, happiness is ',0,'en'),(12,'On average, how many hours per day do you sit at your desk ',0,'en'),(13,'On a typical day, how many servings of fruits and vegetables do you eat ',0,'en'),(14,'The definition of wellness is:  The state of optimal health within the mind, body, and spirit ',0,'en'),(15,'A What activities do you do for fun or relaxation ',0,'en'),(16,'ನನ್ನ ಸಾಮಾನ್ಯ ದಿನದಲ್ಲಿ, ನಾನು ದೈಹಿಕವಾಗಿ ಸಕ್ರಿಯನಾಗಿರುತ್ತೇನೆ (ನಾನು ಲಿಫ್ಟ್‌ಗೆ ಬದಲಾಗಿ ಮೆಟ್ಟಿಲುಗಳನ್ನು ತೆಗೆದುಕೊಳ್ಳುತ್ತೇನೆ, ನಾನು ನಡೆಯುತ್ತೇನೆ)',0,'kn'),(17,'ನಿಮ್ಮ ಒತ್ತಡದ ಮುಖ್ಯ ಮೂಲವನ್ನು ನೀವು ಏನು ಪರಿಗಣಿಸುತ್ತೀರಿ',0,'kn'),(18,'ನೀವು ಸಾಮಾನ್ಯವಾಗಿ ಪ್ರತಿದಿನ ಎಷ್ಟು ಕಪ್ ಕಾಫಿ ಅಥವಾ ಕೆಫೀನ್ ಪಾನೀಯಗಳನ್ನು ಸೇವಿಸುತ್ತೀರಿ',0,'kn'),(19,'ಎ ನೀವು ಸಾಮಾನ್ಯವಾಗಿ ಪ್ರತಿದಿನ ಎಷ್ಟು ಲೀಟರ್ ನೀರನ್ನು ಕುಡಿಯುತ್ತೀರಿ',0,'kn'),(20,'ನನ್ನ ಮೇಲೆ ಕೋಪಗೊಂಡಾಗ ನಾನು ಕಡಿಮೆ ತೀರ್ಪು ನೀಡಲು ಪ್ರಯತ್ನಿಸುತ್ತೇನೆ',0,'kn'),(21,'ನಾನು ಹೆಚ್ಚು ಶಾಂತಿಯುತವಾಗಲು ಪ್ರಯತ್ನಿಸುತ್ತೇನೆ- ನಾನು ಆತಂಕಕ್ಕೊಳಗಾದಾಗ',0,'kn'),(22,'ದೈಹಿಕ ಮತ್ತು ಮಾನಸಿಕ ಪ್ರತಿಕ್ರಿಯೆಗಳಿಗೆ ಕಾರಣವಾಗುತ್ತದೆ ಎಂಬ ಭಾವನೆ',0,'kn'),(23,'ನೀವು ಸ್ವಲ್ಪ ವ್ಯಾಯಾಮವನ್ನು ಪಡೆಯುವ ಬಗ್ಗೆ ಯೋಚಿಸುತ್ತಿದ್ದೀರಿ, ನೀವು ಆರಿಸಿಕೊಳ್ಳಿ',0,'kn'),(24,'ಸೋಮವಾರ ಬೆಳಿಗ್ಗೆ, ನೀವು ಕೆಲಸಕ್ಕೆ ಹೋಗುವ ಮೊದಲು, ನಿಮಗೆ ಅನಿಸುತ್ತದೆ',0,'kn'),(25,'ನಿಮ್ಮ ಜೀವನವನ್ನು ನೀವು ಪ್ರತಿಬಿಂಬಿಸಿದಾಗ, ನೀವು ಯೋಚಿಸುತ್ತೀರಿ',0,'kn'),(26,'ನಿಮ್ಮ ಅಭಿಪ್ರಾಯದಲ್ಲಿ, ಸಂತೋಷ',0,'kn'),(27,'ನಿಮ್ಮ ಮೇಜಿನ ಬಳಿ ದಿನಕ್ಕೆ ಸರಾಸರಿ ಎಷ್ಟು ಗಂಟೆ ಕುಳಿತುಕೊಳ್ಳುತ್ತೀರಿ',0,'kn'),(28,'ಒಂದು ವಿಶಿಷ್ಟ ದಿನದಲ್ಲಿ, ನೀವು ಎಷ್ಟು ಹಣ್ಣುಗಳು ಮತ್ತು ತರಕಾರಿಗಳನ್ನು ಸೇವಿಸುತ್ತೀರಿ',0,'kn'),(29,'ಸ್ವಾಸ್ಥ್ಯದ ವ್ಯಾಖ್ಯಾನ ಹೀಗಿದೆ: ಮನಸ್ಸು, ದೇಹ ಮತ್ತು ಚೇತನದೊಳಗಿನ ಅತ್ಯುತ್ತಮ ಆರೋಗ್ಯದ ಸ್ಥಿತಿ',0,'kn'),(30,'ವಿನೋದ ಅಥವಾ ವಿಶ್ರಾಂತಿಗಾಗಿ ನೀವು ಯಾವ ಚಟುವಟಿಕೆಗಳನ್ನು ಮಾಡುತ್ತೀರಿ',0,'kn');
/*!40000 ALTER TABLE `masterwellnessquestions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:46:55
