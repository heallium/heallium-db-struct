-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `physiotherapyquestionoptions`
--

DROP TABLE IF EXISTS `physiotherapyquestionoptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `physiotherapyquestionoptions` (
  `optionID` varchar(45) NOT NULL,
  `optionText` varchar(250) NOT NULL,
  `points` int(11) NOT NULL,
  `questionID` int(11) NOT NULL,
  `actionID` int(11) DEFAULT NULL,
  PRIMARY KEY (`optionID`),
  KEY `fk_q_phy_idx` (`questionID`),
  CONSTRAINT `fk_q_phy` FOREIGN KEY (`questionID`) REFERENCES `masterphysiotherapyquestions` (`questionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `physiotherapyquestionoptions`
--

LOCK TABLES `physiotherapyquestionoptions` WRITE;
/*!40000 ALTER TABLE `physiotherapyquestionoptions` DISABLE KEYS */;
INSERT INTO `physiotherapyquestionoptions` VALUES ('1','Obese',0,1,1),('10','Nearly',1,3,10),('100','ನೋವು ಮತ್ತು ನೋವುಗಳು',0,26,100),('101','ಜುಮ್ಮೆನಿಸುವಿಕೆ',1,26,101),('102','ಚರ್ಮದ ಅಲರ್ಜಿಗಳು, ಎಸ್ಜಿಮಾ ಅಥವಾ ಡರ್ಮಟೈಟಿಸ್',1,26,102),('103','ಉಸಿರಾಟದ ತೊಂದರೆಗಳು, ಉದಾ. ಬಿಗಿಯಾದ ಎದೆ, ಆಸ್ತಮಾ',1,26,103),('104','ಹೊಂದಿಕೊಳ್ಳುವಿಕೆ',0,27,104),('105','ಸ್ನಾಯುವಿನ ಸಾಮರ್ಥ್ಯ',0,27,105),('106','ಹೃದಯರಕ್ತನಾಳದ ಸಹಿಷ್ಣುತೆ',1,27,106),('107','ಸ್ನಾಯು ಸಹಿಷ್ಣುತೆ',1,27,107),('108','ತರಕಾರಿಗಳು',0,28,108),('109','ಹಣ್ಣುಗಳು',1,28,109),('11','Just',1,3,11),('110','ತಂಪು ಪಾನೀಯಗಳು',0,28,110),('111','ಸಾಕಷ್ಟು ನೀರು',1,28,111),('112','ನಿದ್ರೆ',0,29,112),('113','ವ್ಯಾಯಾಮ',1,29,113),('114','ಮಾತು',0,29,114),('115','ನಗು',1,29,115),('116','ತೂಕ ನಷ್ಟ ಮತ್ತು ಒಟ್ಟಾರೆ ಆರೋಗ್ಯ',1,30,116),('117','ಸಕ್ಕರೆಯನ್ನು ಕೊಬ್ಬಿನೊಳಗೆ ಪರಿವರ್ತಿಸುವುದು',0,30,117),('118','ಕೂದಲು ಮತ್ತು ಉಗುರು ಬೆಳವಣಿಗೆ',1,30,118),('12','Easily',1,3,12),('13','Fail miserably',0,4,13),('14','I would step off the line a few times',1,4,14),('15','I might wobble a bit but I\'d make it',1,4,15),('16','No problem, give me a drink!',0,4,16),('17','I wouldn\'t make it to the mailbox',1,5,17),('18','To the end of the block',1,5,18),('19','About a kilometer or mile',1,5,19),('2','Very overweight',0,1,2),('20','A long way',0,5,20),('21','None',0,6,21),('22','A few',1,6,22),('23','Many',1,6,23),('24','Heaps',0,6,24),('25','I\'d be no help',0,7,25),('26','I\'d pitch in but need a few helpers',1,7,26),('27','I could carry one end of it myself',1,7,27),('28','I could possibly do it myself',1,7,28),('29','Nothing',0,8,29),('3','Slightly overweight',0,1,3),('30','A shoe box',0,8,30),('31','A low fence',1,8,31),('32','A high hurdle',1,8,32),('33','No way',0,9,33),('34','I\'d give it a go but probably not',0,9,34),('35','I could catch the thief but possibly not overpower them',1,9,35),('36','Easily',1,9,36),('37','Choose the lift every time',0,10,37),('38','Walk up, but be out of breath',1,10,38),('39','Stride up, but still be out of breath',1,10,39),('4','Healthy weight range',1,1,4),('40','Race up several flights no problem',1,10,40),('41','Aches & pains',0,11,41),('42','Tingling',1,11,42),('43','Skin allergies, eczema or dermatitis',0,11,43),('44','Breathing problems, e.g. Tight chest, asthma',1,11,44),('45','Flexibility',1,12,45),('46','Muscular Strength',1,12,46),('47','Cardiorespiratory Endurance',0,12,47),('48','Muscular Endurance',0,12,48),('49','Vegetables',1,13,49),('5','None',0,2,5),('50','Fruits',1,13,50),('51','Soft drinks',0,13,51),('52','Plenty of water',1,13,52),('53','Sleep',0,14,53),('54','Exercise',1,14,54),('55','Talk',0,14,55),('56','Laugh',1,14,56),('57','Weight Loss And Overall Health',0,15,57),('58','Converting Sugar Into Fat',1,15,58),('59','Hair And Nail Growth',1,15,59),('6','1 or 2 days',0,2,6),('60','ಬೊಜ್ಜು',0,16,60),('61','ತುಂಬಾ ಅಧಿಕ ತೂಕ',0,16,61),('62','ಸ್ವಲ್ಪ ಅಧಿಕ ತೂಕ',0,16,62),('63','ಆರೋಗ್ಯಕರ ತೂಕ ಶ್ರೇಣಿ',1,16,63),('64','ಯಾವುದೂ',0,17,64),('65','1 ಅಥವಾ 2 ದಿನಗಳು',0,17,65),('66','3 ರಿಂದ 5 ದಿನಗಳು',0,17,66),('67','6 ಅಥವಾ ಹೆಚ್ಚು',1,17,67),('68','ಹತ್ತಿರಕ್ಕೂ ಇಲ್ಲ',0,18,68),('69','ಸುಮಾರು',1,18,69),('7','3 to 5 days',0,2,7),('70','ಕೇವಲ',1,18,70),('71','ಸುಲಭವಾಗಿ',1,18,71),('72','ಶೋಚನೀಯವಾಗಿ ವಿಫಲವಾಗಿದೆ',0,19,72),('73','ನಾನು ಕೆಲವು ಬಾರಿ ಸಾಲಿನಿಂದ ಕೆಳಗಿಳಿಯುತ್ತೇನೆ',1,19,73),('74','ನಾನು ಸ್ವಲ್ಪ ನಡುಗಬಹುದು ಆದರೆ ನಾನು ಅದನ್ನು ಮಾಡುತ್ತೇನೆ',1,19,74),('75','ತೊಂದರೆ ಇಲ್ಲ, ನನಗೆ ಪಾನೀಯ ನೀಡಿ!',0,19,75),('76','ನಾನು ಅದನ್ನು ಅಂಚೆಪೆಟ್ಟಿಗೆಗೆ ಸೇರಿಸುವುದಿಲ್ಲ',1,20,76),('77','ಬ್ಲಾಕ್ನ ಕೊನೆಯಲ್ಲಿ',1,20,77),('78','ಸುಮಾರು ಒಂದು ಕಿಲೋಮೀಟರ್ ಅಥವಾ ಮೈಲಿ',1,20,78),('79','ಬಹಳ ದೂರ',0,20,79),('8','6 or more',1,2,8),('80','ಯಾವುದೂ',0,21,80),('81','ಕೆಲವು',1,21,81),('82','ಅನೇಕ',1,21,82),('83','ರಾಶಿ',0,21,83),('84','ನಾನು ಯಾವುದೇ ಸಹಾಯ ಮಾಡುವುದಿಲ್ಲ',0,22,84),('85','ನಾನು ಪಿಚ್ ಮಾಡುತ್ತೇನೆ ಆದರೆ ಕೆಲವು ಸಹಾಯಕರು ಬೇಕು',1,22,85),('86','ಅದರ ಒಂದು ತುದಿಯನ್ನು ನಾನೇ ಒಯ್ಯಬಲ್ಲೆ',1,22,86),('87','ನಾನು ಅದನ್ನು ನಾನೇ ಮಾಡಬಹುದು',1,22,87),('88','ಏನೂ ಇಲ್ಲ',0,23,88),('89','ಶೂ ಪೆಟ್ಟಿಗೆ',0,23,89),('9','Not even close',0,3,9),('90','ಕಡಿಮೆ ಬೇಲಿ',1,23,90),('91','ಹೆಚ್ಚಿನ ಅಡಚಣೆ',1,23,91),('92','ಆಗುವುದೇ ಇಲ್ಲ',0,24,92),('93','ನಾನು ಅದನ್ನು ಹೋಗಲಾಡಿಸುತ್ತೇನೆ ಆದರೆ ಬಹುಶಃ ಇಲ್ಲ',0,24,93),('94','ನಾನು ಕಳ್ಳನನ್ನು ಹಿಡಿಯಬಹುದು ಆದರೆ ಬಹುಶಃ ಅವರನ್ನು ಮೀರಿಸುವುದಿಲ್ಲ',1,24,94),('95','ಸುಲಭವಾಗಿ',1,24,95),('96','ಪ್ರತಿ ಬಾರಿಯೂ ಲಿಫ್ಟ್ ಆಯ್ಕೆಮಾಡಿ',0,25,96),('97','ಮೇಲಕ್ಕೆ ನಡೆ, ಆದರೆ ಉಸಿರಾಟದಿಂದ ಹೊರಗುಳಿಯಿರಿ',1,25,97),('98','ಮೇಲಕ್ಕೆತ್ತಿ, ಆದರೆ ಇನ್ನೂ ಉಸಿರಾಟದಿಂದ ದೂರವಿರಿ',1,25,98),('99','ಹಲವಾರು ವಿಮಾನಗಳನ್ನು ಓಡಿಸಿ ಯಾವುದೇ ತೊಂದರೆ ಇಲ್ಲ',1,25,99);
/*!40000 ALTER TABLE `physiotherapyquestionoptions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:47:06
