-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `masterbloodtestslots`
--

DROP TABLE IF EXISTS `masterbloodtestslots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `masterbloodtestslots` (
  `centerID` varchar(45) NOT NULL,
  `openTime` time NOT NULL,
  `closeTime` time NOT NULL,
  `availableSlotsPerHour` int(11) NOT NULL,
  `address` varchar(45) DEFAULT NULL,
  `availableTimeSlots` int(11) DEFAULT NULL,
  `languageID` varchar(45) DEFAULT NULL,
  KEY `fk_centeID_emp_idx` (`centerID`),
  CONSTRAINT `fk_centeID_emp` FOREIGN KEY (`centerID`) REFERENCES `masterbloodtestcenter` (`centerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `masterbloodtestslots`
--

LOCK TABLES `masterbloodtestslots` WRITE;
/*!40000 ALTER TABLE `masterbloodtestslots` DISABLE KEYS */;
INSERT INTO `masterbloodtestslots` VALUES ('1','09:30:00','10:30:00',2,'On Scans and Labs',2,'en'),('1','10:30:00','11:30:00',1,'On Scans and Labs',1,'en'),('1','11:30:00','12:30:00',3,'On Scans and Labs',3,'en'),('2','12:30:00','13:30:00',1,'Thyrocare Collection center',1,'en'),('2','13:30:00','14:30:00',2,'Thyrocare Collection center',2,'en'),('2','14:30:00','15:30:00',1,'Thyrocare Collection center',1,'en'),('3','15:30:00','16:30:00',1,'Ravi Labs and Research Center',1,'en'),('3','16:30:00','17:30:00',2,'Ravi Labs and Research Center',2,'en'),('3','17:30:00','18:30:00',3,'Ravi Labs and Research Center',3,'en'),('3','18:30:00','19:30:00',1,'Ravi Labs and Research Center',1,'en'),('4','15:30:00','16:30:00',2,'Daya Labs',2,'en'),('4','16:30:00','17:30:00',2,'Daya Labs',2,'en'),('4','17:30:00','18:30:00',3,'Daya Labs',3,'en'),('4','18:30:00','19:30:00',1,'Daya Labs',1,'en'),('5','14:30:00','15:30:00',2,'Quest Diagnostics',2,'en'),('5','15:30:00','16:30:00',2,'Quest Diagnostics',2,'en'),('5','16:30:00','17:30:00',3,'Quest Diagnostics',3,'en'),('6','08:30:00','09:30:00',2,'Thyrocare Home Service',2,'en'),('6','09:30:00','10:30:00',2,'Thyrocare Home Service',2,'en'),('6','10:30:00','11:30:00',3,'Thyrocare Home Service',3,'en'),('1a','09:30:00','10:30:00',2,'ಸ್ಕ್ಯಾನ್‌ಗಳು ಮತ್ತು ಲ್ಯಾಬ್‌ಗಳಲ್ಲಿ',2,'kn'),('1a','10:30:00','11:30:00',1,'ಸ್ಕ್ಯಾನ್‌ಗಳು ಮತ್ತು ಲ್ಯಾಬ್‌ಗಳಲ್ಲಿ',1,'kn'),('1a','11:30:00','12:30:00',3,'ಸ್ಕ್ಯಾನ್‌ಗಳು ಮತ್ತು ಲ್ಯಾಬ್‌ಗಳಲ್ಲಿ',3,'kn'),('1b','12:30:00','13:30:00',1,'ಥೈರೋಕೇರ್ ಸಂಗ್ರಹ ಕೇಂದ್ರ',1,'kn'),('1b','13:30:00','14:30:00',2,'ಥೈರೋಕೇರ್ ಸಂಗ್ರಹ ಕೇಂದ್ರ',2,'kn'),('1b','14:30:00','15:30:00',1,'ಥೈರೋಕೇರ್ ಸಂಗ್ರಹ ಕೇಂದ್ರ',1,'kn'),('1c','15:30:00','16:30:00',1,'ರವಿ ಲ್ಯಾಬ್ಸ್ ಮತ್ತು ಸಂಶೋಧನಾ ಕೇಂದ್ರ',1,'kn'),('1c','16:30:00','17:30:00',2,'ರವಿ ಲ್ಯಾಬ್ಸ್ ಮತ್ತು ಸಂಶೋಧನಾ ಕೇಂದ್ರ',2,'kn'),('1c','17:30:00','18:30:00',3,'ರವಿ ಲ್ಯಾಬ್ಸ್ ಮತ್ತು ಸಂಶೋಧನಾ ಕೇಂದ್ರ',3,'kn'),('1c','18:30:00','19:30:00',1,'ರವಿ ಲ್ಯಾಬ್ಸ್ ಮತ್ತು ಸಂಶೋಧನಾ ಕೇಂದ್ರ',1,'kn'),('1d','15:30:00','16:30:00',2,'ದಯಾ ಲ್ಯಾಬ್ಸ್',2,'kn'),('1d','16:30:00','17:30:00',2,'ದಯಾ ಲ್ಯಾಬ್ಸ್',2,'kn'),('1d','17:30:00','18:30:00',3,'ದಯಾ ಲ್ಯಾಬ್ಸ್',3,'kn'),('1d','18:30:00','19:30:00',1,'ದಯಾ ಲ್ಯಾಬ್ಸ್',1,'kn'),('1e','14:30:00','15:30:00',2,'ಕ್ವೆಸ್ಟ್ ಡಯಾಗ್ನೋಸ್ಟಿಕ್ಸ್',2,'kn'),('1e','15:30:00','16:30:00',2,'ಕ್ವೆಸ್ಟ್ ಡಯಾಗ್ನೋಸ್ಟಿಕ್ಸ್',2,'kn'),('1e','16:30:00','17:30:00',3,'ಕ್ವೆಸ್ಟ್ ಡಯಾಗ್ನೋಸ್ಟಿಕ್ಸ್',3,'kn'),('1f','08:30:00','09:30:00',2,'ಥೈರೋಕೇರ್ ಹೋಮ್ ಸರ್ವಿಸ್',2,'kn'),('1f','09:30:00','10:30:00',2,'ಥೈರೋಕೇರ್ ಹೋಮ್ ಸರ್ವಿಸ್',2,'kn'),('1f','10:30:00','11:30:00',3,'ಥೈರೋಕೇರ್ ಹೋಮ್ ಸರ್ವಿಸ್',3,'kn');
/*!40000 ALTER TABLE `masterbloodtestslots` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:45:39
