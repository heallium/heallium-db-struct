-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `labeltranslater`
--

DROP TABLE IF EXISTS `labeltranslater`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `labeltranslater` (
  `languageID` varchar(45) DEFAULT NULL,
  `labelNames` varchar(100) DEFAULT NULL,
  `labelValues` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `clientID` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `labeltranslater`
--

LOCK TABLES `labeltranslater` WRITE;
/*!40000 ALTER TABLE `labeltranslater` DISABLE KEYS */;
INSERT INTO `labeltranslater` VALUES ('kn','_task','ಕಾರ್ಯ','AD163P'),('kn','_stressTest','ಒತ್ತಡ ಪರೀಕ್ಷೆ','AD163P'),('kn','_lifestyleTest','ಲೈಫ್‌ಸ್ಟೈಲ್ ಟೆಸ್ಟ್','AD163P'),('kn','_physicalTest','ದೈಹಿಕ ಪರೀಕ್ಷೆ','AD163P'),('kn','_wellnessTest','ಸ್ವಾಸ್ಥ್ಯ ಪರೀಕ್ಷೆ','AD163P'),('kn','_appointment','ವಾರ್ಷಿಕ ತಪಾಸಣೆಗಾಗಿ ನೇಮಕಾತಿ','AD163P'),('kn','_heallium','ಹೀಲಿಯಂ','AD163P'),('kn','_rememberme','ನನ್ನನ್ನು ನೆನಪಿನಲ್ಲಿಡು','AD163P'),('kn','_signin','ಸೈನ್ ಇನ್ ಮಾಡಿ','AD163P'),('kn','_forgot_password','ಪಾಸ್ವರ್ಡ್ ಮರೆತಿರಾ','AD163P'),('kn','_labs','ಲ್ಯಾಬ್‌ಗಳು','AD163P'),('kn','_apCenter','ನೇಮಕಾತಿ ಕೇಂದ್ರಗಳು','AD163P'),('kn','_apDate','ನೇಮಕಾತಿ ದಿನಾಂಕ','AD163P'),('kn','_bookAppointmenet','ಪುಸ್ತಕ ನೇಮಕಾತಿ','AD163P'),('kn','_book ','ಪುಸ್ತಕ ನೇಮಕಾತಿ','AD163P'),('kn','_timeSlots','ಟೈಮ್‌ಸ್ಲಾಟ್‌ಗಳು','AD163P'),('kn','_slot/Hour','ಸ್ಲಾಟ್ಗಳು / ಗಂಟೆ','AD163P'),('kn','_chosseTimeSlot','ಅಪಾಯಿಂಟ್ಮೆಂಟ್ ಬುಕ್ ಮಾಡಲು ಟೈಮ್ಸ್ಲಾಟ್ ಆಯ್ಕೆಮಾಡಿ','AD163P'),('kn','_apSchedule','ನೇಮಕಾತಿ ವೇಳಾಪಟ್ಟಿ','AD163P'),('kn','_labName','ಲ್ಯಾಬ್‌ನೇಮ್','AD163P'),('kn','_apTime','ನೇಮಕಾತಿ ಸಮಯ','AD163P'),('kn','_done','ಮುಗಿದಿದೆ','AD163P'),('kn','_previous','ಹಿಂದಿನದು','AD163P'),('kn','_saveForLater','ನಂತರಕ್ಕೆ ಉಳಿಸು','AD163P'),('kn','_next','ಮುಂದೆ','AD163P'),('kn','_ok','ಸರಿ','AD163P'),('kn','_answerHaveBeenSaved','ಉತ್ತರಗಳನ್ನು ಉಳಿಸಲಾಗಿದೆ','AD163P'),('kn','_answerdSuccessfully','ನೀವು ಯಶಸ್ವಿಯಾಗಿ ಉತ್ತರಿಸಿದ್ದೀರಿ ಮತ್ತು ದಯವಿಟ್ಟು ಮುಂದಿನ ಪ್ರಕ್ರಿಯೆಗೆ ಮುಂದುವರಿಯಿರಿ','AD163P'),('kn','_scores','ಅಂಕಗಳು','AD163P'),('kn','_reports','ವರದಿಗಳು','AD163P'),('kn','_track','ಟ್ರ್ಯಾಕ್','AD163P'),('kn','_leaderBoard','ಲೀಡರ್ಬೋರ್ಡ್','AD163P'),('kn','_testName','ಟೆಸ್ಟ್ ಹೆಸರು','AD163P'),('kn','_taskDetails','ಟಾಸ್ಕ್ ವಿವರಗಳು','AD163P'),('kn','_overallRank','ಒಟ್ಟಾರೆ ಶ್ರೇಯಾಂಕ','AD163P'),('kn','_home','ಮನೆ','AD163P'),('kn','_profile','ವಿವರ','AD163P'),('kn','_heallium','ಹೀಲಿಯಂ','AD163P'),('kn','_myReports','ನನ್ನ ವರದಿಗಳು','AD163P'),('kn','_suggestions','ಸಲಹೆಗಳು','AD163P'),('kn','_myPrescription','ನನ್ನ ಪ್ರಿಸ್ಕ್ರಿಪ್ಷನ್','AD163P'),('kn','_setting','ಸಂಯೋಜನೆಗಳು','AD163P'),('kn','_supportAndFeedback','ಬೆಂಬಲ ಮತ್ತು ಪ್ರತಿಕ್ರಿಯೆ','AD163P'),('kn','_logOut','ಲಾಗ್ ಔಟ್','AD163P'),('kn','_AccountSeting','ಖಾತೆ ಸೆಟ್ಟಿಂಗ್‌ಗಳು','AD163P'),('kn','_profileSetting','ಪ್ರೊಫೈಲ್ ಸೆಟ್ಟಿಂಗ್‌ಗಳು','AD163P'),('kn','_NotificationSetting','ಅಧಿಸೂಚನೆ ಸೆಟ್ಟಿಂಗ್‌ಗಳು','AD163P'),('kn','_name','ಹೆಸರು','AD163P'),('kn','_changePassword','ಗುಪ್ತಪದವನ್ನು ಬದಲಿಸಿ','AD163P'),('kn','_language','ಭಾಷೆ','AD163P'),('kn','_selectOne','ಒಂದನ್ನು ಆರಿಸಿ','AD163P'),('kn','_cancle','ಕ್ಯಾನ್ಸರ್','AD163P'),('kn','_save','ಉಳಿಸಿ','AD163P'),('kn','_firstName','ಮೊದಲ ಹೆಸರು','AD163P'),('kn','_lastName','ಕೊನೆಯ ಹೆಸರು','AD163P'),('kn','_oldPassword','ಹಳೆಯ ಪಾಸ್‌ವರ್ಡ್','AD163P'),('kn','_newPassword','ಹೊಸ ಪಾಸ್‌ವರ್ಡ್','AD163P'),('kn','_confirmPassword','ಪಾಸ್ವರ್ಡ್ ದೃಢೀಕರಿಸಿ','AD163P'),('kn','_blockNotification','ಅಧಿಸೂಚನೆಯನ್ನು ನಿರ್ಬಂಧಿಸಿ','AD163P'),('kn','_accessories','ಪರಿಕರಗಳು','AD163P'),('kn','_suyncWithGoogleFit','Google ಫಿಟ್‌ನೊಂದಿಗೆ ಸಿಂಕ್ ಮಾಡಿ','AD163P'),('kn','_myReports','ನನ್ನ ವರದಿಗಳು','AD163P'),('kn','_myPrescription','ನನ್ನ ಪ್ರಿಸ್ಕ್ರಿಪ್ಷನ್','AD163P'),('kn','_suggestions','ಸಲಹೆಗಳು','AD163P'),('kn','_reminders','ಜ್ಞಾಪನೆಗಳು','AD163P'),('kn','_myHealthRecord','ನನ್ನ ಆರೋಗ್ಯ ದಾಖಲೆ','AD163P'),('kn','_notification','ಅಧಿಸೂಚನೆ','AD163P'),('kn','_settings','ಸಂಯೋಜನೆಗಳು','AD163P'),('kn','_support','ಬೆಂಬಲ','AD163P'),('kn','_reportAnIssue','ಸಮಸ್ಯೆಯನ್ನು ವರದಿ ಮಾಡಿ','AD163P'),('kn','_aboutUs','ನಮ್ಮ ಬಗ್ಗೆ','AD163P'),('kn','_account','ಖಾತೆ','AD163P'),('kn','_profile','ವಿವರ','AD163P'),('kn','_notification','ಅಧಿಸೂಚನೆ','AD163P'),('kn','_connectWith','ಇದರೊಂದಿಗೆ ಸಂಪರ್ಕ ಸಾಧಿಸಿ','AD163P'),('kn','_masterHealthCheckup','ಮಾಸ್ಟರ್ ಆರೋಗ್ಯ ತಪಾಸಣೆ','AD163P');
/*!40000 ALTER TABLE `labeltranslater` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:45:40
