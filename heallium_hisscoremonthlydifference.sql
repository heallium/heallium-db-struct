-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `hisscoremonthlydifference`
--

DROP TABLE IF EXISTS `hisscoremonthlydifference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `hisscoremonthlydifference` (
  `locationID` varchar(45) NOT NULL,
  `employeeID` varchar(45) NOT NULL,
  `employeeName` varchar(45) NOT NULL,
  `previousScore` double NOT NULL,
  `currentScore` double NOT NULL,
  `percentageChange` double NOT NULL,
  `branchID` varchar(45) NOT NULL,
  `projectID` varchar(45) NOT NULL,
  `teamID` varchar(45) NOT NULL,
  `clientID` varchar(45) DEFAULT NULL,
  `testDate` date DEFAULT NULL,
  `groupPathID` varchar(45) DEFAULT NULL,
  KEY `fk_c_h_idx` (`clientID`),
  KEY `fk_b_hh_idx` (`branchID`),
  KEY `fk_p_hh_idx` (`projectID`),
  KEY `fk_g_hh_idx` (`groupPathID`),
  KEY `fk_t_hh_idx` (`teamID`),
  CONSTRAINT `fk_b_hh` FOREIGN KEY (`branchID`) REFERENCES `masterclientbranch` (`branchID`),
  CONSTRAINT `fk_c_h` FOREIGN KEY (`clientID`) REFERENCES `masterclient` (`clientID`),
  CONSTRAINT `fk_g_hh` FOREIGN KEY (`groupPathID`) REFERENCES `mastergrouppath` (`groupPathID`),
  CONSTRAINT `fk_p_hh` FOREIGN KEY (`projectID`) REFERENCES `masterproject` (`projectID`),
  CONSTRAINT `fk_t_hh` FOREIGN KEY (`teamID`) REFERENCES `masterteam` (`teamID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hisscoremonthlydifference`
--

LOCK TABLES `hisscoremonthlydifference` WRITE;
/*!40000 ALTER TABLE `hisscoremonthlydifference` DISABLE KEYS */;
INSERT INTO `hisscoremonthlydifference` VALUES ('553471','Emp1','Pramodh',55.9,56,0.1,'M742187','B1744','AT77S','AD163P','2019-12-10','IF51J'),('553471','Emp2','Rajesh',45.9,46,0.1,'M742187','B1744','AT77S','AD163P','2019-12-10','IF51J'),('553471','Emp3','Puneeth',40,40.1,0.1,'M742187','B1744','AT77S','AD163P','2019-12-10','IF51J'),('553471','Emp4','Chadan',70,70.1,0.1,'M742187','B1744','AT77S','AD163P','2019-12-10','IF51J'),('553471','Emp5','Shivaraj',57.1,57.1,0.1,'M742187','B1744','AT77S','AD163P','2019-12-10','IF51J'),('553471','Emp6','Kiran',41,41.1,0.1,'M742187','B1744','AT77S','AD163P','2019-12-10','IF51J'),('553471','Emp7','Siddu',66,66.1,0.1,'M742187','B1744','AT77S','AD163P','2019-12-10','IF51J'),('553471','Emp8','Suresh',80,80.1,0.1,'M742187','B1744','AT77S','AD163P','2019-12-10','IF51J'),('553471','Emp9','Anil',63.9,64,0.1,'M742187','B1744','AT77S','AD163P','2019-12-10','IF51J'),('553471','Emp10','Ravi',33,33.1,0.1,'M742187','B1744','AT77S','AD163P','2019-12-10','IF51J'),('553471','Emp11','Aravind',44,44,0,'M742187','B1744','AT97I','AD163P','2019-12-11','ASP32K'),('553471','Emp12','Hari ',45,45.2,0.2,'M742187','B1744','AT97I','AD163P','2019-12-11','ASP32K'),('553471','Emp13','Sachin',58,58.1,0.1,'M742187','B1744','AT97I','AD163P','2019-12-11','ASP32K'),('553471','Emp14','Arun',60,60.1,0.1,'M742187','B1744','AT97I','AD163P','2019-12-11','ASP32K'),('553471','Emp15','Aditya',62,62.1,0.1,'M742187','B1744','AT97I','AD163P','2019-12-11','ASP32K'),('553471','Emp16','Santhosh',48,48,0,'M742187','B1744','AT97I','AD163P','2019-12-11','ASP32K'),('553471','Emp17','Vinod',52,52.1,0.1,'M742187','B1744','AT97I','AD163P','2019-12-11','ASP32K'),('553471','Emp18','Laxmikanth',53,53.3,0.3,'M742187','B1744','AT97I','AD163P','2019-12-11','ASP32K'),('553471','Emp19','Vijay',55,55.4,0.4,'M742187','B1744','AT97I','AD163P','2019-12-11','ASP32K'),('553471','Emp20','Karthik',56,56.5,0.5,'M742187','B1744','AT97I','AD163P','2019-12-11','ASP32K');
/*!40000 ALTER TABLE `hisscoremonthlydifference` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:44:58
