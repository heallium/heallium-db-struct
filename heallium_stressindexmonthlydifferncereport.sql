-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `stressindexmonthlydifferncereport`
--

DROP TABLE IF EXISTS `stressindexmonthlydifferncereport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `stressindexmonthlydifferncereport` (
  `locationID` varchar(45) DEFAULT NULL,
  `employeeID` varchar(45) DEFAULT NULL,
  `employeeName` varchar(45) DEFAULT NULL,
  `previousScore` double DEFAULT NULL,
  `currentScore` double DEFAULT NULL,
  `percentageChange` double DEFAULT NULL,
  `branchID` varchar(45) DEFAULT NULL,
  `projectID` varchar(45) DEFAULT NULL,
  `teamID` varchar(45) DEFAULT NULL,
  `groupPathID` varchar(45) DEFAULT NULL,
  `testDate` date DEFAULT NULL,
  `clientID` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stressindexmonthlydifferncereport`
--

LOCK TABLES `stressindexmonthlydifferncereport` WRITE;
/*!40000 ALTER TABLE `stressindexmonthlydifferncereport` DISABLE KEYS */;
INSERT INTO `stressindexmonthlydifferncereport` VALUES ('553471','Emp1','Pramodh',7.9,8,0.1,'M742187','B1744','AT77S','IF51J','2019-12-10','AD163P'),('553471','Emp2','Rajesh',6,6.1,0.1,'M742187','B1744','AT77S','IF51J','2019-12-10','AD163P'),('553471','Emp3','Puneeth',8,8,0,'M742187','B1744','AT77S','IF51J','2019-12-10','AD163P'),('553471','Emp4','Chadan',6.9,7,0.1,'M742187','B1744','AT77S','IF51J','2019-12-10','AD163P'),('553471','Emp5','Shivaraj',9,9,0,'M742187','B1744','AT77S','IF51J','2019-12-10','AD163P'),('553471','Emp6','Kiran',10.7,10.8,0.1,'M742187','B1744','AT77S','IF51J','2019-12-10','AD163P'),('553471','Emp7','Siddu',8,8.1,0.1,'M742187','B1744','AT77S','IF51J','2019-12-10','AD163P'),('553471','Emp8','Suresh',5.9,6,0.1,'M742187','B1744','AT77S','IF51J','2019-12-10','AD163P'),('553471','Emp9','Anil',4.8,4.9,0.1,'M742187','B1744','AT77S','IF51J','2019-12-10','AD163P'),('553471','Emp10','Ravi',12,12,0,'M742187','B1744','AT77S','IF51J','2019-12-10','AD163P'),('553471','Emp11','Aravind',8.3,8.4,0.1,'M742187','B1744','AT97I','ASP32K','2019-12-11','AD163P'),('553471','Emp12','Hari ',7.3,7.4,0.1,'M742187','B1744','AT97I','ASP32K','2019-12-11','AD163P'),('553471','Emp13','Sachin',5,5.4,0.4,'M742187','B1744','AT97I','ASP32K','2019-12-11','AD163P'),('553471','Emp14','Arun',4.1,4.1,0.1,'M742187','B1744','AT97I','ASP32K','2019-12-11','AD163P'),('553471','Emp15','Aditya',3.8,4,0.2,'M742187','B1744','AT97I','ASP32K','2019-12-11','AD163P'),('553471','Emp16','Santhosh',5,5.1,0.1,'M742187','B1744','AT97I','ASP32K','2019-12-11','AD163P'),('553471','Emp17','Vinod',7,7.1,0.1,'M742187','B1744','AT97I','ASP32K','2019-12-11','AD163P'),('553471','Emp18','Laxmikanth',6,6.2,0.2,'M742187','B1744','AT97I','ASP32K','2019-12-11','AD163P'),('553471','Emp19','Vijay',8,8.2,0.2,'M742187','B1744','AT97I','ASP32K','2019-12-11','AD163P'),('553471','Emp20','Karthik',7,7.1,0.1,'M742187','B1744','AT97I','ASP32K','2019-12-11','AD163P');
/*!40000 ALTER TABLE `stressindexmonthlydifferncereport` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:47:06
