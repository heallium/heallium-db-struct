-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employerlabeltranslater`
--

DROP TABLE IF EXISTS `employerlabeltranslater`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `employerlabeltranslater` (
  `languageID` varchar(45) NOT NULL,
  `labelName` varchar(100) NOT NULL,
  `labelValue` varchar(200) NOT NULL,
  `clientID` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employerlabeltranslater`
--

LOCK TABLES `employerlabeltranslater` WRITE;
/*!40000 ALTER TABLE `employerlabeltranslater` DISABLE KEYS */;
INSERT INTO `employerlabeltranslater` VALUES ('kn','heallium','ಹೀಲಿಯಂ','AD163P'),('kn','filterby','ಇವರಿಂದ ಫಿಲ್ಟರ್ ಮಾಡಿ:','AD163P'),('kn','home','ಮನೆ','AD163P'),('kn','aboutUs','ನಮ್ಮ ಬಗ್ಗೆ','AD163P'),('kn','support','ಬೆಂಬಲ','AD163P'),('kn','search','ಹುಡುಕಿ ','AD163P'),('kn','leaderBoard','ಲೀಡರ್‌ಬೋರ್ಡ್','AD163P'),('kn','employee','ಉದ್ಯೋಗಿ','AD163P'),('kn','score','ಸ್ಕೋರ್','AD163P'),('kn','project','ಯೋಜನೆ','AD163P'),('kn','team','ತಂಡ','AD163P'),('kn','healthIndexReport','ಆರೋಗ್ಯ ಸೂಚ್ಯಂಕ ವರದಿ','AD163P'),('kn','stressIndecReport','ಒತ್ತಡ ಸೂಚ್ಯಂಕ ವರದಿ','AD163P'),('kn','lifestyleReport','ಲೈಫ್‌ಸ್ಟೈಲ್ ವರದಿ','AD163P'),('kn','wellnessReport','ಸ್ವಾಸ್ಥ್ಯ ವರದಿ','AD163P'),('kn','physicalReport','ಭೌತಿಕ ವರದಿ','AD163P'),('kn','employeeEnrollment','ಉದ್ಯೋಗಿ-ದಾಖಲಾತಿ','AD163P'),('kn','highestRecordedHIS','ಅತ್ಯಧಿಕ ರೆಕಾರ್ಡ್ ಮಾಡಿದ ಎಚ್ಐಎಸ್','AD163P'),('kn','averageHIS','ಆವರೇಜ್ ಎಚ್ಐಎಸ್','AD163P'),('kn','lowestRecordeHIS','ಕಡಿಮೆ ರೆಕಾರ್ಡ್ ಮಾಡಿದ ಎಚ್ಐಎಸ್','AD163P'),('kn','highestRecordedStress','ಅತಿ ಹೆಚ್ಚು ದಾಖಲಾದ ಒತ್ತಡ','AD163P'),('kn','averageStress','ಸರಾಸರಿ ಒತ್ತಡ','AD163P'),('kn','lowestRecordeStress','ಕಡಿಮೆ ದಾಖಲಾದ ಒತ್ತಡ','AD163P'),('kn','highestRecordedLifestyle','ಅತ್ಯಧಿಕ ರೆಕಾರ್ಡ್ ಮಾಡಿದ ಜೀವನಶೈಲಿ','AD163P'),('kn','averageLifestyle','ಸರಾಸರಿ ಜೀವನಶೈಲಿ','AD163P'),('kn','lowestRecordeLifestyle','ಕಡಿಮೆ ರೆಕಾರ್ಡ್ ಮಾಡಿದ ಜೀವನಶೈಲಿ','AD163P'),('kn','highestRecordedWellness','ಅತ್ಯಧಿಕ ರೆಕಾರ್ಡ್ ಮಾಡಿದ ಸ್ವಾಸ್ಥ್ಯ','AD163P'),('kn','averageWellness','ಸರಾಸರಿ ಸ್ವಾಸ್ಥ್ಯ','AD163P'),('kn','lowestRecordedWellness','ಕಡಿಮೆ ದಾಖಲಾದ ಸ್ವಾಸ್ಥ್ಯ','AD163P'),('kn','highestRecordedScore','ಅತಿ ಹೆಚ್ಚು ದಾಖಲಾದ ಸ್ಕೋರ್','AD163P'),('kn','averageScore','ಸರಾಸರಿ ಸ್ಕೋರ್','AD163P'),('kn','lowestRecordeScore','ಕಡಿಮೆ ರೆಕಾರ್ಡ್ ಮಾಡಿದ ಸ್ಕೋರ್','AD163P'),('kn','notYetEnrolled','ಇನ್ನೂ ದಾಖಲಾಗಲಿಲ್ಲ','AD163P'),('kn','enrolled','ಸೇರಿಕೊಂಡಳು','AD163P'),('kn','testCompleted','ಪರೀಕ್ಷೆ ಪೂರ್ಣಗೊಂಡಿದೆ','AD163P'),('kn','location','ಸ್ಥಳ','AD163P'),('kn','employeeID','ಉದ್ಯೋಗಿ ಐಡಿ','AD163P'),('kn','employeeName','ನೌಕರನ ಹೆಸರು','AD163P'),('kn','previousScore','ಹಿಂದಿನ ಸ್ಕೋರ್','AD163P'),('kn','currentSScore','ಪ್ರಸ್ತುತ ಸ್ಕೋರ್','AD163P'),('kn','percentageChange','ಪರ್ಸೆಂಟೇಜ್ ಬದಲಾವಣೆ (%)','AD163P'),('kn','sendMessage','ಸಂದೇಶ ಕಳುಹಿಸಿ','AD163P'),('kn','send','ಕಳುಹಿಸು','AD163P'),('kn','pushNotification','ಪುಶ್ ಅಧಿಸೂಚನೆ','AD163P'),('kn','e_mail','ಇ-ಮೇಲ್','AD163P'),('kn','reSet','ಮರುಹೊಂದಿಸಿ','AD163P'),('kn','setting','ಹೊಂದಿಸಲಾಗುತ್ತಿದೆ','AD163P'),('kn','Log-Out','ಲಾಗ್ ಔಟ್','AD163P'),('kn','account','ಖಾತೆ','AD163P'),('kn','editAccount','ನಿಮ್ಮ ಖಾತೆ ಸೆಟ್ಟಿಂಗ್‌ಗಳನ್ನು ಸಂಪಾದಿಸಿ ಮತ್ತು ನಿಮ್ಮ ಪಾಸ್‌ವರ್ಡ್ ಅನ್ನು ಇಲ್ಲಿ ಬದಲಾಯಿಸಿ.','AD163P'),('kn','firstName','ಮೊದಲ ಹೆಸರು','AD163P'),('kn','lastName','ಕೊನೆಯ ಹೆಸರು','AD163P'),('kn','contactNumber','ಸಂಪರ್ಕ ಸಂಖ್ಯೆ','AD163P'),('kn','oldPassword','ಹಳೆಯ ಪಾಸ್‌ವರ್ಡ್','AD163P'),('kn','newPassword','ಹೊಸ ಪಾಸ್‌ವರ್ಡ್','AD163P'),('kn','confirmPassword','ಪಾಸ್ವರ್ಡ್ ದೃಢೀಕರಿಸಿ','AD163P'),('kn','save','ಉಳಿಸಿ','AD163P'),('kn','photo','ಫೋಟೋ','AD163P'),('kn','addProfile','ಪ್ರೊಫೈಲ್‌ಗಾಗಿ ನಿಮ್ಮ ಫೋಟೋವನ್ನು ಸೇರಿಸಿ','AD163P'),('kn','saveImage','ಚಿತ್ರವನ್ನು ಉಳಿಸು','AD163P'),('kn','address','ವಿಳಾಸ','AD163P'),('kn','sendTo','ಕಳುಹಿಸು','AD163P'),('kn','meassage','ಸಂದೇಶ','AD163P'),('kn','ok','ಸರಿ','AD163P'),('kn','cancel','ರದ್ದುಗೊಳಿಸಿ','AD163P'),('kn','withoutFilter','ಫಿಲ್ಟರ್ ಇಲ್ಲದೆ','AD163P'),('kn','withFilter','ಫಿಲ್ಟರ್‌ನೊಂದಿಗೆ','AD163P'),('kn','hideReport','ಮರೆಮಾಡು ವರದಿ','AD163P'),('kn','showReport','ಶೋ ವರದಿ','AD163P');
/*!40000 ALTER TABLE `employerlabeltranslater` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:45:36
