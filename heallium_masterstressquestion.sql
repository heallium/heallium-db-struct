-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `masterstressquestion`
--

DROP TABLE IF EXISTS `masterstressquestion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `masterstressquestion` (
  `questionID` int(11) NOT NULL,
  `description` varchar(500) NOT NULL,
  `optionFormat` int(11) NOT NULL,
  `languageID` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`questionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `masterstressquestion`
--

LOCK TABLES `masterstressquestion` WRITE;
/*!40000 ALTER TABLE `masterstressquestion` DISABLE KEYS */;
INSERT INTO `masterstressquestion` VALUES (1,'Is work culture supportive in your organization',0,'en'),(2,'How often you face stress situation in your organization.',0,'en'),(3,'Most of your stress are related to',0,'en'),(4,'How do you feel while working in the organization',0,'en'),(5,'Have you taken leave in the past 12 months due to work related stress',0,'en'),(6,'	Please estimate the average number of hours per week that you work (both on and off site) during term time.',0,'en'),(7,'Please indicate total workload has changed during last three years',0,'en'),(8,'Stress is related to demand',0,'en'),(9,'Stress related to support',0,'en'),(10,'Whom does you report / share if you have any problems in your work',0,'en'),(11,'How often you face stress situation being taken care off',0,'en'),(12,'How do you handle stress situations',0,'en'),(13,'To what level the management is effective in handling your stress situation',0,'en'),(14,'Does the upper management pressure main reason for stress',0,'en'),(15,'Do you feel stressed at work',0,'en'),(16,'ನಿಮ್ಮ ಸಂಸ್ಥೆಯಲ್ಲಿ ಕೆಲಸದ ಸಂಸ್ಕೃತಿ ಬೆಂಬಲಿತವಾಗಿದೆ',0,'kn'),(17,'ನಿಮ್ಮ ಸಂಸ್ಥೆಯಲ್ಲಿ ನೀವು ಎಷ್ಟು ಬಾರಿ ಒತ್ತಡದ ಪರಿಸ್ಥಿತಿಯನ್ನು ಎದುರಿಸುತ್ತೀರಿ.',0,'kn'),(18,'ನಿಮ್ಮ ಹೆಚ್ಚಿನ ಒತ್ತಡವು ಸಂಬಂಧಿಸಿದೆ',0,'kn'),(19,'ಸಂಸ್ಥೆಯಲ್ಲಿ ಕೆಲಸ ಮಾಡುವಾಗ ನಿಮಗೆ ಹೇಗೆ ಅನಿಸುತ್ತದೆ',0,'kn'),(20,' ಕೆಲಸಕ್ಕೆ ಸಂಬಂಧಿಸಿದ ಒತ್ತಡದಿಂದಾಗಿ ಕಳೆದ 12 ತಿಂಗಳುಗಳಲ್ಲಿ ನೀವು ರಜೆ ತೆಗೆದುಕೊಂಡಿದ್ದೀರಾ?',0,'kn'),(21,'ಟರ್ಮ್ ಸಮಯದಲ್ಲಿ ನೀವು ಕೆಲಸ ಮಾಡುವ (ಸೈಟ್‌ನಲ್ಲಿ ಮತ್ತು ಹೊರಗೆ ಎರಡೂ) ವಾರಕ್ಕೆ ಸರಾಸರಿ ಗಂಟೆಗಳ ಸಂಖ್ಯೆಯನ್ನು ಅಂದಾಜು ಮಾಡಿ.',0,'kn'),(22,'ಕಳೆದ ಮೂರು ವರ್ಷಗಳಲ್ಲಿ ಒಟ್ಟು ಕೆಲಸದ ಹೊರೆ ಬದಲಾಗಿದೆ ಎಂದು ದಯವಿಟ್ಟು ಸೂಚಿಸಿ',0,'kn'),(23,'ಒತ್ತಡವು ಬೇಡಿಕೆಗೆ ಸಂಬಂಧಿಸಿದೆ',0,'kn'),(24,'ಬೆಂಬಲಕ್ಕೆ ಸಂಬಂಧಿಸಿದ ಒತ್ತಡ',0,'kn'),(25,'ನಿಮ್ಮ ಕೆಲಸದಲ್ಲಿ ಏನಾದರೂ ಸಮಸ್ಯೆಗಳಿದ್ದರೆ ನೀವು ಯಾರನ್ನು ವರದಿ ಮಾಡುತ್ತೀರಿ / ಹಂಚಿಕೊಳ್ಳುತ್ತೀರಿ',0,'kn'),(26,'ಒತ್ತಡದ ಪರಿಸ್ಥಿತಿಯನ್ನು ನೀವು ಎಷ್ಟು ಬಾರಿ ಎದುರಿಸುತ್ತೀರಿ',0,'kn'),(27,'ಒತ್ತಡದ ಸಂದರ್ಭಗಳನ್ನು ನೀವು ಹೇಗೆ ನಿರ್ವಹಿಸುತ್ತೀರಿ',0,'kn'),(28,'ನಿಮ್ಮ ಒತ್ತಡದ ಪರಿಸ್ಥಿತಿಯನ್ನು ನಿಭಾಯಿಸುವಲ್ಲಿ ನಿರ್ವಹಣೆ ಯಾವ ಮಟ್ಟಕ್ಕೆ ಪರಿಣಾಮಕಾರಿಯಾಗಿದೆ',0,'kn'),(29,'ಮೇಲಿನ ನಿರ್ವಹಣಾ ಒತ್ತಡವು ಒತ್ತಡಕ್ಕೆ ಮುಖ್ಯ ಕಾರಣವೇ?',0,'kn'),(30,'ನೀವು ಕೆಲಸದಲ್ಲಿ ಒತ್ತಡವನ್ನು ಅನುಭವಿಸುತ್ತೀರಾ ',0,'kn');
/*!40000 ALTER TABLE `masterstressquestion` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:45:51
