-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wellnessmonthlydifferencereporttable`
--

DROP TABLE IF EXISTS `wellnessmonthlydifferencereporttable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wellnessmonthlydifferencereporttable` (
  `locationID` varchar(45) NOT NULL,
  `employeeID` varchar(45) NOT NULL,
  `employeeName` varchar(45) NOT NULL,
  `previousScore` double NOT NULL,
  `currentScore` double NOT NULL,
  `percentageChange` double NOT NULL,
  `branchID` varchar(45) NOT NULL,
  `projectID` varchar(45) NOT NULL,
  `teamID` varchar(45) NOT NULL,
  `groupPathID` varchar(45) DEFAULT NULL,
  `testDate` varchar(45) DEFAULT NULL,
  `clientID` varchar(45) DEFAULT NULL,
  KEY `fk_bi_idx` (`branchID`),
  CONSTRAINT `fk_bi` FOREIGN KEY (`branchID`) REFERENCES `masterclientbranch` (`branchID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wellnessmonthlydifferencereporttable`
--

LOCK TABLES `wellnessmonthlydifferencereporttable` WRITE;
/*!40000 ALTER TABLE `wellnessmonthlydifferencereporttable` DISABLE KEYS */;
INSERT INTO `wellnessmonthlydifferencereporttable` VALUES ('553471','Emp1','Pramodh',8.8,8.8,0,'M742187','B1744','AT77S','IF51J','2019-12-10','AD163P'),('553471','Emp2','Rajesh',7.7,7.8,0.1,'M742187','B1744','AT77S','IF51J','2019-12-10','AD163P'),('553471','Emp3','Puneeth',7,7.1,0.1,'M742187','B1744','AT77S','IF51J','2019-12-10','AD163P'),('553471','Emp4','Chadan',8.5,8.6,0.1,'M742187','B1744','AT77S','IF51J','2019-12-10','AD163P'),('553471','Emp5','Shivaraj',5.6,5.6,0,'M742187','B1744','AT77S','IF51J','2019-12-10','AD163P'),('553471','Emp6','Kiran',4.6,4.6,0,'M742187','B1744','AT77S','IF51J','2019-12-10','AD163P'),('553471','Emp7','Siddu',6.7,6.8,0.1,'M742187','B1744','AT77S','IF51J','2019-12-10','AD163P'),('553471','Emp8','Suresh',10.6,10.6,0,'M742187','B1744','AT77S','IF51J','2019-12-10','AD163P'),('553471','Emp9','Anil',9.6,9.6,0,'M742187','B1744','AT77S','IF51J','2019-12-10','AD163P'),('553471','Emp10','Ravi',7.6,7.6,0,'M742187','B1744','AT77S','IF51J','2019-12-10','AD163P'),('553471','Emp11','Aravind',8,8.4,0.4,'M742187','B1744','AT97I','ASP32K','2019-12-11','AD163P'),('553471','Emp12','Hari ',5,5.2,0.2,'M742187','B1744','AT97I','ASP32K','2019-12-11','AD163P'),('553471','Emp13','Sachin',4,4.3,0.3,'M742187','B1744','AT97I','ASP32K','2019-12-11','AD163P'),('553471','Emp14','Arun',4.1,4.1,0.1,'M742187','B1744','AT97I','ASP32K','2019-12-11','AD163P'),('553471','Emp15','Aditya',4.5,4.6,0.1,'M742187','B1744','AT97I','ASP32K','2019-12-11','AD163P'),('553471','Emp16','Santhosh',5.5,5.5,0,'M742187','B1744','AT97I','ASP32K','2019-12-11','AD163P'),('553471','Emp17','Vinod',7.2,7.3,0,'M742187','B1744','AT97I','ASP32K','2019-12-11','AD163P'),('553471','Emp18','Laxmikanth',6.2,6.2,0.1,'M742187','B1744','AT97I','ASP32K','2019-12-11','AD163P'),('553471','Emp19','Vijay',8.3,8.4,0.1,'M742187','B1744','AT97I','ASP32K','2019-12-11','AD163P'),('553471','Emp20','Karthik',7,7.1,0.1,'M742187','B1744','AT97I','ASP32K','2019-12-11','AD163P');
/*!40000 ALTER TABLE `wellnessmonthlydifferencereporttable` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:45:21
