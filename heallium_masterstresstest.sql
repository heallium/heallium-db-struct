-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `masterstresstest`
--

DROP TABLE IF EXISTS `masterstresstest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `masterstresstest` (
  `stressTestID` varchar(45) NOT NULL,
  `stressTestQuestion` varchar(100) NOT NULL,
  `stressTestQuestionOption1` varchar(45) NOT NULL,
  `stressTestQuestionOption2` varchar(45) NOT NULL,
  `stressTestQuestionOption3` varchar(45) NOT NULL,
  `stressTestQuestionOption4` varchar(45) NOT NULL,
  `stressTestQuestionOption5` varchar(45) NOT NULL,
  PRIMARY KEY (`stressTestID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `masterstresstest`
--

LOCK TABLES `masterstresstest` WRITE;
/*!40000 ALTER TABLE `masterstresstest` DISABLE KEYS */;
INSERT INTO `masterstresstest` VALUES ('STID1','When theres a great deal of stress in my life','Always','Often','Sometimes','Rarely','Never'),('STID10','If there is something that bothers me greatly about our relationship, I let my partner know','Completely true','Mostlytrue','Somewhat true/false','Mostlyfalse','Completely false'),('STID2','Creative Problem-Solving Test','Completely false','Completely true','Mostlytrue','Somewhat true/false','Mostlyfalse'),('STID3','When others get stuck, I am able to think of new solutions to problems','Strongly agree','Agree','Somewhat agree/disagree','Disagree Strongly','Disagree'),('STID4','Once Ive found a solution that I believe will work, I see no point in coming up with more','Exactly like me','A lot like me','Somewhat like me','A little like me','Not at all like me'),('STID5','I have noticed that my appetite has changed (increased or decreased) without my meaning to modify it','Markedly','Noticeably','Slightly','Barely','Not at all'),('STID6','Have you experienced any sleep disturbances or changes in your sleep pattern?','Ive been having trouble staying asleep','Ive been having trouble falling asleep','Ive been sleeping more than I usually do','Ive been sleeping less than I usually do','No'),('STID7','Even if I dont intend to, I raise my voice when I get upset','Almost never','Rarely','Sometimes','Often','Most of the time'),('STID8','When I realize that I was wrong, I admit it to my partner','Completely true','Mostlytrue','Somewhat true/false','Mostlyfalse','Completely false'),('STID9','When we argue, I point out my partners character flaws','Most of the time','Often','Sometimes','Rarely','Almost never');
/*!40000 ALTER TABLE `masterstresstest` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:47:07
