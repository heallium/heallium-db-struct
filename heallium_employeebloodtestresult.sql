-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employeebloodtestresult`
--

DROP TABLE IF EXISTS `employeebloodtestresult`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `employeebloodtestresult` (
  `employeeID` varchar(45) NOT NULL,
  `clientID` varchar(45) NOT NULL,
  `score` int(11) DEFAULT NULL,
  `bloodTestDate` date NOT NULL,
  `status` varchar(45) NOT NULL,
  `locationID` varchar(45) DEFAULT NULL,
  `branchID` varchar(45) DEFAULT NULL,
  `projectID` varchar(45) DEFAULT NULL,
  `teamID` varchar(45) DEFAULT NULL,
  `groupPathID` varchar(45) DEFAULT NULL,
  KEY `fk_BloodTESTClient_idx` (`clientID`),
  CONSTRAINT `fk_clientID_blood_test` FOREIGN KEY (`clientID`) REFERENCES `masterclient` (`clientID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employeebloodtestresult`
--

LOCK TABLES `employeebloodtestresult` WRITE;
/*!40000 ALTER TABLE `employeebloodtestresult` DISABLE KEYS */;
INSERT INTO `employeebloodtestresult` VALUES ('Emp1','AD163P',30,'2019-06-04','Completed',NULL,NULL,NULL,NULL,NULL),('Emp2','AD163P',24,'2019-06-10','Completed',NULL,NULL,NULL,NULL,NULL),('Emp3','AD163P',20,'2019-06-12','Completed',NULL,NULL,NULL,NULL,NULL),('Emp4','AD163P',22,'2019-06-16','Completed',NULL,NULL,NULL,NULL,NULL),('Emp5','AD163P',18,'2019-06-17','Completed',NULL,NULL,NULL,NULL,NULL),('Emp6','AD163P',16,'2019-06-18','Completed',NULL,NULL,NULL,NULL,NULL),('Emp7','AD163P',20,'2019-06-15','Completed',NULL,NULL,NULL,NULL,NULL),('Emp8','AD163P',24,'2019-06-14','Completed',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `employeebloodtestresult` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:46:56
