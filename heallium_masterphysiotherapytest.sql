-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `masterphysiotherapytest`
--

DROP TABLE IF EXISTS `masterphysiotherapytest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `masterphysiotherapytest` (
  `physiotherapyTestID` varchar(45) NOT NULL,
  `physiotherapyTestQuestion` varchar(150) NOT NULL,
  `physiotherapyTestQuestionOption1` varchar(45) NOT NULL,
  `physiotherapyTestQuestionOption2` varchar(45) NOT NULL,
  `physiotherapyTestQuestionOption3` varchar(45) NOT NULL,
  `physiotherapyTestQuestionOption4` varchar(45) NOT NULL,
  `physiotherapyTestQuestionOption5` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `masterphysiotherapytest`
--

LOCK TABLES `masterphysiotherapytest` WRITE;
/*!40000 ALTER TABLE `masterphysiotherapytest` DISABLE KEYS */;
INSERT INTO `masterphysiotherapytest` VALUES ('PHID1','When reading a book or magazine, how often do you find yourself re-reading the same paragraph or skipping ahead','Quite often','Often','Sometimes','Rarely','Almost never'),('PHID2','How often do you interrupt people during conversations','Quite often','Often','Sometimes','Rarely','Almost never'),('PHID3','When theres a great deal of stress in my life','Quite often','Often','Sometimes','Rarely','Almost never'),('PHID4','Youre on the phone with a friend just as your favorite TV show starts. How difficult would it be for you to pay attention to the conversation','Extremely difficult','Very difficult','Somewhat difficult','Slightly difficult','Not at all difficult'),('PHID5','I have noticed that my appetite has changed (increased or decreased) without my meaning to modify it','Markedly','Noticeably','Slightly','Barely','Not at all'),('PHID6','I am concerned about my health','Most of the time','Often','Sometimes','Rarely','Almost never'),('PHID7','My body is aching','Most of the time','Often','Sometimes','Rarely','Almost never'),('PHID8','Have you experienced any sleep disturbances or changes in your sleep pattern','Ive been having trouble staying asleep','Ive been having trouble falling asleep','Ive been sleeping more than I usually do','Ive been sleeping less than I usually do.','No'),('PHID9','I feel in control of my body','Most of the time','Often','Sometimes','Rarely','Almost never'),('PHID10','You overhear a friend badmouthing you. How angry does that make you feel','I dont feel angry at all','I feel slightly annoyed','I feel a little angry','I feel moderately angry','I feel very angry');
/*!40000 ALTER TABLE `masterphysiotherapytest` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:46:41
