-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employeewellnessaitransaction`
--

DROP TABLE IF EXISTS `employeewellnessaitransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `employeewellnessaitransaction` (
  `transactionID` varchar(45) NOT NULL,
  `empID` int(11) NOT NULL,
  `actionID` varchar(45) NOT NULL,
  `wellNessTransactionID` varchar(45) NOT NULL,
  `clientID` varchar(45) NOT NULL,
  PRIMARY KEY (`transactionID`),
  KEY `fk_wellNess_transaction_id_idx` (`transactionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employeewellnessaitransaction`
--

LOCK TABLES `employeewellnessaitransaction` WRITE;
/*!40000 ALTER TABLE `employeewellnessaitransaction` DISABLE KEYS */;
INSERT INTO `employeewellnessaitransaction` VALUES ('WAI1',1,'WAC1','WAIT1','5'),('WAI10',10,'WAC10','EAIT10','12'),('WAI2',2,'WAC2','WAIT2','4'),('WAI3',3,'WAC3','WAIT3','3'),('WAI4',4,'WAC4','WAIT4','6'),('WAI5',5,'WAC5','WAIT5','7'),('WAI6',6,'WAC6','WAIT6','8'),('WAI7',7,'WAC7','WAIT7','9'),('WAI8',8,'WAC8','WAIT8','10'),('WAI9',9,'WAC9','WAIT9','11');
/*!40000 ALTER TABLE `employeewellnessaitransaction` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:45:32
