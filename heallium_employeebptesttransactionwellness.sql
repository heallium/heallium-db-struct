-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employeebptesttransactionwellness`
--

DROP TABLE IF EXISTS `employeebptesttransactionwellness`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `employeebptesttransactionwellness` (
  `transactionID` varchar(45) NOT NULL,
  `empID` int(11) NOT NULL,
  `clientID` varchar(45) NOT NULL,
  `score` int(11) NOT NULL,
  `transactionDate` date NOT NULL,
  PRIMARY KEY (`transactionID`),
  KEY `fk_BpTestwellclientID_idx` (`clientID`),
  CONSTRAINT `fk_BpTestwellclientID` FOREIGN KEY (`clientID`) REFERENCES `masterclient` (`clientID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employeebptesttransactionwellness`
--

LOCK TABLES `employeebptesttransactionwellness` WRITE;
/*!40000 ALTER TABLE `employeebptesttransactionwellness` DISABLE KEYS */;
INSERT INTO `employeebptesttransactionwellness` VALUES ('WBPID1',1,'5',60,'2019-04-03'),('WBPID10',10,'5',55,'2019-04-26'),('WBPID2',2,'4',65,'2019-04-02'),('WBPID3',3,'6',75,'2019-04-05'),('WBPID4',4,'7',65,'2019-04-06'),('WBPID5',5,'10',77,'2019-04-08'),('WBPID6',6,'11',66,'2019-04-09'),('WBPID7',7,'12',55,'2019-04-12'),('WBPID8',8,'8',60,'2019-04-22'),('WBPID9',9,'3',45,'2019-04-25');
/*!40000 ALTER TABLE `employeebptesttransactionwellness` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:46:21
