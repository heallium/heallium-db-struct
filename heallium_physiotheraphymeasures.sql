-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `physiotheraphymeasures`
--

DROP TABLE IF EXISTS `physiotheraphymeasures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `physiotheraphymeasures` (
  `phymeasuresID` varchar(45) DEFAULT NULL,
  `phymeasuresDetails` varchar(1050) DEFAULT NULL,
  `clientID` varchar(45) DEFAULT NULL,
  `employeeID` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `physiotheraphymeasures`
--

LOCK TABLES `physiotheraphymeasures` WRITE;
/*!40000 ALTER TABLE `physiotheraphymeasures` DISABLE KEYS */;
INSERT INTO `physiotheraphymeasures` VALUES ('1','Always stretch before and after exercising and get enough aerobic exercise.','AD163P','emp1'),('2','Limit other oils and use Olive oil.','AD163P','emp1'),('3','Try doing Endurance sports.','AD163P','emp1'),('4','Never skip your Yoga or Exercise session. ','AD163P','emp1'),('5','Chew slowly whatever eat and don\'t deny dessert.','AD163P','emp1'),('6','Always stretch before and after exercising and get enough aerobic exercise.','AD163P','emp2'),('7','Try doing Endurance sports.','AD163P','emp2'),('8','Never skip your Yoga or Exercise session. ','AD163P','emp2'),('9','Chew slowly whatever eat and don\'t deny dessert.','AD163P','emp2'),('10','Limit other oils and use Olive oil.','AD163P','emp2'),('11','Try doing Endurance sports.','AD163P','emp3'),('12','Never skip your Yoga or Exercise session. ','AD163P','emp3'),('13','Chew slowly whatever eat and don\'t deny dessert.','AD163P','emp3'),('14','Limit other oils and use Olive oil.','AD163P','emp3'),('15','Always stretch before and after exercising and get enough aerobic exercise.','AD163P','emp3');
/*!40000 ALTER TABLE `physiotheraphymeasures` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:46:38
