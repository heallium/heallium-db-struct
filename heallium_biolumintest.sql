-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `biolumintest`
--

DROP TABLE IF EXISTS `biolumintest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `biolumintest` (
  `SINO` int(11) DEFAULT NULL,
  `Location` text,
  `EmployeeID` text,
  `EmployeeName` text,
  `Office` text,
  `Project` text,
  `Team` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `biolumintest`
--

LOCK TABLES `biolumintest` WRITE;
/*!40000 ALTER TABLE `biolumintest` DISABLE KEYS */;
INSERT INTO `biolumintest` VALUES (1,'Hyderabad','42ER459','Puneeth','Office3','Fusion','DreamMaker'),(2,'Banglore','35ER489','Rajesh','Office2','RamPage','Connect Tech'),(3,'Mysore','85ER582','Aravind','Office1','Fusion','Dream Maker'),(4,'Pune','72ER745','Suresh','Office2','Quadro','Greed Pack'),(5,'Mumbai','25ER432','Sidram','Office4','Hornets','Master Of Power'),(6,'Cheanni','23ER412','Anil','Office1','Ramp Page','Connect Tech'),(7,'Davanageri','20ER317','Santhosh','Office5','Hornets','The Achivers'),(8,'Hubli','18ER512','Shashnk','Office1','Fusion','Dream Makers'),(9,'Bejwada','15ER420','Vinayak','Office4','Hornets','Greed Pack'),(10,'Vizag','20ER421','Varun','Office5','Rampage','Connetc Tech'),(11,'Vijayawada','19ER325','Ramya','Office1','Fusion','Master Of power'),(12,'Chennai','17ER319','Hari','Office4','Qudaro','Dream Maker'),(13,'Mumbai','15ER712','Pradeep','Office5','Rampage','Master Of Power'),(14,'Pune','18ER112','Pavan','Office6','Fusion','The Achivers'),(15,'Hariyan','17ER210','Pavankalyan','Office7','Hornets','Connect Tech'),(16,'Bhuvaneswar','11ER216','Sumith','Office1','Topcat','Greed Pach'),(17,'Pandichery','07ER206','Sumanth','Office4','Qudaro','Master Of Power'),(18,'Bhihar','15ER212','Vikash','Office5','Fusion','The Achivers'),(19,'Chennai','14ER222','Arjun','Office4','Qudaro','Dream Maker'),(20,'Vizag','10ER218','Vadiraj','Office7','Hornets','Connetc Tech');
/*!40000 ALTER TABLE `biolumintest` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:46:32
