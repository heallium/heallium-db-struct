-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mastergrouppath`
--

DROP TABLE IF EXISTS `mastergrouppath`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `mastergrouppath` (
  `groupPathID` varchar(45) NOT NULL,
  `clientID` varchar(45) NOT NULL,
  `groupPathName` varchar(45) NOT NULL,
  `groupPathDesc` varchar(45) NOT NULL,
  `teamID` varchar(45) DEFAULT NULL,
  `branchID` varchar(45) DEFAULT NULL,
  `projectID` varchar(45) DEFAULT NULL,
  `cityID` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`groupPathID`),
  KEY `fk_clientID_masterGroupPath_idx` (`clientID`),
  CONSTRAINT `fk_clientID_masterGroupPath` FOREIGN KEY (`clientID`) REFERENCES `masterclient` (`clientID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mastergrouppath`
--

LOCK TABLES `mastergrouppath` WRITE;
/*!40000 ALTER TABLE `mastergrouppath` DISABLE KEYS */;
INSERT INTO `mastergrouppath` VALUES ('ASP32K','AD163P','CRYSTAL','Execution','AT97I','M742187','B1744','553471'),('ASP51H','AD163P','VISTA','Integer','MT67V','W612441','B8119','553471'),('IF32M','AD163P','TRACE','Path','KT99X','W612441','B8119','553471'),('IF51J','AD163P','Boolean','Crystaldecisions','AT77S','M742187','B1744','553471'),('LP23B','AD163P','PRISM','Internal','ST36Q','M742187','Z6381','553471'),('LP43X','AD163P','HILUX','String','PT33M','M742187','P1741','553471'),('OAK23K','AD163P','LASER','Authentication','BT12A','M742187','H6649','553471'),('OAK73P','AD163P','XYRO','Pagination','HT12D','W612441','H7558','553471'),('PS32L','AD163P','ELEMENT','Command','HT66F','W612441','H7558','553471'),('PS41N','AD163P','TROVIT','Pipeline','QT78H','M742187','P1741','553471'),('PSN31T','AD163P','CLONE','Registration','BT44K','M742187','Z6381','553471'),('PSN52y','AD163P','DROID','External','ZT88P','M742187','H6649','553471');
/*!40000 ALTER TABLE `mastergrouppath` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:46:11
