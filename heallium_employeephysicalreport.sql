-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employeephysicalreport`
--

DROP TABLE IF EXISTS `employeephysicalreport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `employeephysicalreport` (
  `phyMonth` varchar(45) NOT NULL,
  `maxPhyScore` int(11) NOT NULL,
  `minPhyScore` int(11) NOT NULL,
  `avgPhyScore` int(11) NOT NULL,
  `clientID` varchar(45) DEFAULT NULL,
  `locationID` varchar(45) NOT NULL,
  `branchID` varchar(45) NOT NULL,
  `projectID` varchar(45) NOT NULL,
  `teamID` varchar(45) NOT NULL,
  `groupPathID` varchar(45) NOT NULL,
  `testDate` varchar(45) DEFAULT NULL,
  KEY `fk_branchID_empreport_idx` (`branchID`),
  KEY `fk_teamID_empreport_idx` (`teamID`),
  KEY `fk_projectID_empreport_idx` (`projectID`),
  KEY `fk_groupPathID_empreport_idx` (`groupPathID`),
  CONSTRAINT `fk_branchID_empreport` FOREIGN KEY (`branchID`) REFERENCES `masterclientbranch` (`branchID`),
  CONSTRAINT `fk_groupPathID_empreport` FOREIGN KEY (`groupPathID`) REFERENCES `mastergrouppath` (`groupPathID`),
  CONSTRAINT `fk_projectID_empreport` FOREIGN KEY (`projectID`) REFERENCES `masterproject` (`projectID`),
  CONSTRAINT `fk_teamID_empreport` FOREIGN KEY (`teamID`) REFERENCES `masterteam` (`teamID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employeephysicalreport`
--

LOCK TABLES `employeephysicalreport` WRITE;
/*!40000 ALTER TABLE `employeephysicalreport` DISABLE KEYS */;
INSERT INTO `employeephysicalreport` VALUES ('January',5,10,5,'AD163P','553471','M742187','B1744','AT77S','IF51J','2019-01-10'),('February',10,7,3,'AD163P','553471','M742187','B1744','AT77S','IF51J','2019-02-10'),('March',3,7,10,'AD163P','553471','M742187','B1744','AT77S','IF51J','2019-03-10'),('April',10,3,7,'AD163P','553471','M742187','B1744','AT77S','IF51J','2019-04-10'),('May',5,10,5,'AD163P','553471','M742187','B1744','AT77S','IF51J','2019-05-10'),('June',4,6,10,'AD163P','553471','M742187','B1744','AT77S','IF51J','2019-06-10'),('July',10,4,6,'AD163P','553471','M742187','B1744','AT77S','IF51J','2019-07-10'),('August',3,10,7,'AD163P','553471','M742187','B1744','AT77S','IF51J','2019-08-10'),('September',6,7,7,'AD163P','553471','M742187','B1744','AT77S','IF51J','2019-09-10'),('October',6,8,6,'AD163P','553471','M742187','B1744','AT77S','IF51J','2019-10-10'),('November',5,9,6,'AD163P','553471','M742187','B1744','AT77S','IF51J','2019-11-10'),('Decembeer',10,2,8,'AD163P','553471','M742187','B1744','AT77S','IF51J','2019-12-10'),('January',4,8,8,'AD163P','553471','M742187','B1744','AT97I','ASP32K','2019-01-11'),('February',3,3,14,'AD163P','553471','M742187','B1744','AT97I','ASP32K','2019-02-11'),('March',14,4,2,'AD163P','553471','M742187','B1744','AT97I','ASP32K','2019-03-11'),('April',4,4,12,'AD163P','553471','M742187','B1744','AT97I','ASP32K','2019-04-11'),('May',8,4,8,'AD163P','553471','M742187','B1744','AT97I','ASP32K','2019-05-11'),('June',8,5,7,'AD163P','553471','M742187','B1744','AT97I','ASP32K','2019-06-11'),('July',6,6,8,'AD163P','553471','M742187','B1744','AT97I','ASP32K','2019-07-11'),('August',8,8,4,'AD163P','553471','M742187','B1744','AT97I','ASP32K','2019-08-11'),('September',8,4,8,'AD163P','553471','M742187','B1744','AT97I','ASP32K','2019-09-11'),('October',4,5,11,'AD163P','553471','M742187','B1744','AT97I','ASP32K','2019-10-11'),('November',12,4,4,'AD163P','553471','M742187','B1744','AT97I','ASP32K','2019-11-11'),('Decembeer',8,7,5,'AD163P','553471','M742187','B1744','AT97I','ASP32K','2019-12-11');
/*!40000 ALTER TABLE `employeephysicalreport` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:46:02
