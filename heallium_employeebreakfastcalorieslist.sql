-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employeebreakfastcalorieslist`
--

DROP TABLE IF EXISTS `employeebreakfastcalorieslist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `employeebreakfastcalorieslist` (
  `clientID` varchar(45) DEFAULT NULL,
  `employeeID` varchar(45) DEFAULT NULL,
  `foodDate` varchar(45) DEFAULT NULL,
  `foodTime` time DEFAULT NULL,
  `foodID` varchar(45) DEFAULT NULL,
  `foodName` varchar(45) DEFAULT NULL,
  `foodCalories` int(11) DEFAULT NULL,
  `totalBreakFastCalories` int(11) DEFAULT NULL,
  `foodQuantity` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employeebreakfastcalorieslist`
--

LOCK TABLES `employeebreakfastcalorieslist` WRITE;
/*!40000 ALTER TABLE `employeebreakfastcalorieslist` DISABLE KEYS */;
INSERT INTO `employeebreakfastcalorieslist` VALUES ('AD163P','emp1','2019-10-10','08:00:00','10','Dosa',80,0,NULL),('AD163P','emp1','2019-10-09','09:00:00','20','Puri',100,0,NULL),('AD163P','emp1','2019-10-08','08:00:00','30','Idli',120,0,NULL),('AD163P','emp1','2019-10-07','08:00:00','10','Dosa',80,0,NULL),('AD163P','emp1','2019-10-06','09:00:00','30','Idli',120,0,NULL),('AD163P','emp1','2019-10-05','08:00:00','20','Puri',100,0,NULL),('AD163P','emp1','2019-10-04','09:00:00','10','Dosa',80,0,NULL),('AD163P','emp1','2019-10-03','09:00:00','20','Puri',100,0,NULL),('AD163P','emp1','2019-10-02','08:00:00','10','Dosa',80,0,NULL),('AD163P','emp1','2019-10-01','08:00:00','30','Idli',120,0,NULL),('AD163P','emp1','Fri Oct 11 05:30:00 IST 2019','08:00:00','40','Dosa',80,NULL,'1'),('AD163P','emp1','2019-10-11','11:00:00','40','Dosa',120,NULL,'2'),('AD163P','emp1','2019-10-11','11:00:00','40','Dosa',120,NULL,'2'),('AD163P','emp1','2019-10-12','09:00:00','40','Dosa',100,NULL,'1'),('AD163P','emp1','2019-10-11','11:00:00','40','Dosa',120,NULL,'2'),('AD163P','emp1','2019-10-13','11:00:00','40','Dosa',120,NULL,'2'),('AD163P','Emp1','2019-10-14',NULL,'7','Chapati',60,NULL,'1'),('AD163P','Emp1','2019-10-15',NULL,'80','Tamarind Rice',415,NULL,'150 grams'),('AD163P','Emp1','2019-10-15',NULL,'81','Fried rice',90,NULL,'1 Katori or 60 grams');
/*!40000 ALTER TABLE `employeebreakfastcalorieslist` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:45:31
