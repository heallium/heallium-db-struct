-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `masteremployeesuggestions`
--

DROP TABLE IF EXISTS `masteremployeesuggestions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `masteremployeesuggestions` (
  `employeeID` varchar(45) DEFAULT NULL,
  `clientID` varchar(45) DEFAULT NULL,
  `suggestionName` varchar(1500) DEFAULT NULL,
  `suggestionID` varchar(1500) DEFAULT NULL,
  `suggestionDetails` varchar(1500) DEFAULT NULL,
  `languageID` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `masteremployeesuggestions`
--

LOCK TABLES `masteremployeesuggestions` WRITE;
/*!40000 ALTER TABLE `masteremployeesuggestions` DISABLE KEYS */;
INSERT INTO `masteremployeesuggestions` VALUES ('emp1','AD163P','Don\'t drink sugar calories','1','Sugary drinks are among the most fattening items you can put into your body.','en'),('emp1','AD163P','Get enough sleep','2','The importance of getting enoughÂ quality sleepÂ cannot be overstated.Poor sleep can drive insulin resistance, disrupt your appetite hormones, and reduce your physical and mental performance','en'),('emp1','AD163P','Don\'t overcook or burn your meat ','3','Meat can be a nutritious and healthy part of your diet. It\'s veryÂ high in proteinÂ and contains various important nutrients.However, problems occur when meat is overcooked or burnt.','en'),('emp1','AD163P','Eat vegetables and fruits ','4','Vegetables andÂ fruitsÂ are loaded with prebiotic fiber, vitamins, minerals, and many antioxidants, some of which have potent biological effects.Studies show that people who eat the mostÂ vegetablesÂ and fruits live longer and have a lower risk of heart disease, type 2 diabetes, obesity, and other illnesses','en'),('emp1','AD163P','Avoid bright lights before sleep ','5','When you\'re exposed to bright lights in the evening, it may disrupt your production of the sleep hormone melatonin.One strategy is to use a pair of amber-tinted glasses that block blue light from entering your eyes in the evening','en'),('emp1','AD163P','Do some cardio','6','Doing aerobic exercise, also called cardio, is one of the best things you can do for your mental and physical health.It\'s particularly effective at reducing belly fat, the harmful type of fat that builds up around your organs. Reduced belly fat should lead to major improvements in metabolic health','en'),('emp1','AD163P','Don\'t eat a lot of refined carbs ','7','Not all carbs are created equal.Refined carbs have been highly processed to remove their fiber. They\'re relatively low in nutrients and can harm your health when eaten in excess.','en'),('emp1','AD163P','Use plenty of herbs and spices ','8','Many incredibly healthyÂ herbs and spicesÂ exist. For example, ginger and turmeric both have potent anti-inflammatory and antioxidant effects, leading to various health benefits','en'),('emp1','AD163P','A Don\'t fear saturated fat ','9','Saturated fat has been controversial.While it\'s true that saturated fat raises cholesterol levels, it also raises HDL (good) cholesterol and shrinks your LDL (bad) particles, which is linked to a lower risk of heart disease','en'),('emp1','AD163P','ಸಕ್ಕರೆ ಕ್ಯಾಲೊರಿಗಳನ್ನು ಕುಡಿಯಬೇಡಿ','1','ಸಕ್ಕರೆ ಪಾನೀಯಗಳು ನಿಮ್ಮ ದೇಹಕ್ಕೆ ನೀವು ಹಾಕಬಹುದಾದ ಹೆಚ್ಚು ಕೊಬ್ಬಿನಂಶದ ವಸ್ತುಗಳಾಗಿವೆ.','kn'),('emp1','AD163P','ಸಾಕಷ್ಟು ನಿದ್ರೆ ಪಡೆಯಿರಿ','2','ಸಾಕಷ್ಟು ಗುಣಮಟ್ಟದ ನಿದ್ರೆಯನ್ನು ಪಡೆಯುವ ಪ್ರಾಮುಖ್ಯತೆಯನ್ನು ಅತಿಯಾಗಿ ಹೇಳಲಾಗುವುದಿಲ್ಲ. ಕಳಪೆ ನಿದ್ರೆ ಇನ್ಸುಲಿನ್ ಪ್ರತಿರೋಧವನ್ನು ಹೆಚ್ಚಿಸುತ್ತದೆ, ನಿಮ್ಮ ಹಸಿವಿನ ಹಾರ್ಮೋನುಗಳನ್ನು ಅಡ್ಡಿಪಡಿಸುತ್ತದೆ ಮತ್ತು ನಿಮ್ಮ ದೈಹಿಕ ಮತ್ತು ಮಾನಸಿಕ ಕಾರ್ಯಕ್ಷಮತೆಯನ್ನು ಕಡಿಮೆ ಮಾಡುತ್ತದೆ','kn'),('emp1','AD163P','ನಿಮ್ಮ ಮಾಂಸವನ್ನು ಅತಿಯಾಗಿ ಬೇಯಿಸಬೇಡಿ ಅಥವಾ ಸುಡಬೇಡಿ','3','ಮಾಂಸವು ನಿಮ್ಮ ಆಹಾರದ ಪೌಷ್ಟಿಕ ಮತ್ತು ಆರೋಗ್ಯಕರ ಭಾಗವಾಗಬಹುದು. ಇದು ಪ್ರೋಟೀನ್‌ನಲ್ಲಿ ಅತಿ ಹೆಚ್ಚು ಮತ್ತು ವಿವಿಧ ಪ್ರಮುಖ ಪೋಷಕಾಂಶಗಳನ್ನು ಹೊಂದಿರುತ್ತದೆ.ಆದರೆ, ಮಾಂಸವನ್ನು ಅತಿಯಾಗಿ ಬೇಯಿಸಿದಾಗ ಅಥವಾ ಸುಟ್ಟಾಗ ಸಮಸ್ಯೆಗಳು ಉಂಟಾಗುತ್ತವೆ.','kn'),('emp1','AD163P','ತರಕಾರಿಗಳು ಮತ್ತು ಹಣ್ಣುಗಳನ್ನು ಸೇವಿಸಿ','4','ತರಕಾರಿಗಳು ಮತ್ತು ಹಣ್ಣುಗಳು ಪ್ರಿಬಯಾಟಿಕ್ ಫೈಬರ್, ಜೀವಸತ್ವಗಳು, ಖನಿಜಗಳು ಮತ್ತು ಅನೇಕ ಉತ್ಕರ್ಷಣ ನಿರೋಧಕಗಳಿಂದ ತುಂಬಿರುತ್ತವೆ, ಅವುಗಳಲ್ಲಿ ಕೆಲವು ಪ್ರಬಲ ಜೈವಿಕ ಪರಿಣಾಮಗಳನ್ನು ಹೊಂದಿವೆ. ಹೆಚ್ಚಿನ ತರಕಾರಿಗಳನ್ನು ತಿನ್ನುವ ಜನರು ಮತ್ತು ಹಣ್ಣುಗಳು ಹೆಚ್ಚು ಕಾಲ ಬದುಕುತ್ತವೆ ಮತ್ತು ಹೃದಯ ಕಾಯಿಲೆ, ಟೈಪ್ 2 ಡಯಾಬಿಟಿಸ್ ಕಡಿಮೆ ಅಪಾಯವನ್ನು ಹೊಂದಿರುತ್ತವೆ ಎಂದು ಅಧ್ಯಯನಗಳು ತೋರಿಸುತ್ತವೆ. , ಬೊಜ್ಜು ಮತ್ತು ಇತರ ಕಾಯಿಲೆಗಳು','kn'),('emp1','AD163P','ನಿದ್ರೆಯ ಮೊದಲು ಪ್ರಕಾಶಮಾನವಾದ ದೀಪಗಳನ್ನು ತಪ್ಪಿಸಿ','5','ನೀವು ಸಂಜೆ ಪ್ರಕಾಶಮಾನವಾದ ದೀಪಗಳಿಗೆ ಒಡ್ಡಿಕೊಂಡಾಗ, ಇದು ನಿಮ್ಮ ನಿದ್ರೆಯ ಹಾರ್ಮೋನ್ ಮೆಲಟೋನಿನ್ ಉತ್ಪಾದನೆಯನ್ನು ಅಡ್ಡಿಪಡಿಸಬಹುದು.ಒಂದು ತಂತ್ರವೆಂದರೆ ಒಂದು ಜೋಡಿ ಅಂಬರ್-ಬಣ್ಣದ ಕನ್ನಡಕವನ್ನು ಬಳಸುವುದು ಅದು ನೀಲಿ ಬೆಳಕನ್ನು ಸಂಜೆ ನಿಮ್ಮ ಕಣ್ಣುಗಳಿಗೆ ಪ್ರವೇಶಿಸದಂತೆ ತಡೆಯುತ್ತದೆ','kn'),('emp1','AD163P','ಸ್ವಲ್ಪ ಕಾರ್ಡಿಯೋ ಮಾಡಿ','6','ಕಾರ್ಡಿಯೋ ಎಂದೂ ಕರೆಯಲ್ಪಡುವ ಏರೋಬಿಕ್ ವ್ಯಾಯಾಮ ಮಾಡುವುದು ನಿಮ್ಮ ಮಾನಸಿಕ ಮತ್ತು ದೈಹಿಕ ಆರೋಗ್ಯಕ್ಕಾಗಿ ನೀವು ಮಾಡಬಹುದಾದ ಅತ್ಯುತ್ತಮ ಕೆಲಸಗಳಲ್ಲಿ ಒಂದಾಗಿದೆ. ಇದು ಹೊಟ್ಟೆಯ ಕೊಬ್ಬನ್ನು ಕಡಿಮೆ ಮಾಡಲು ವಿಶೇಷವಾಗಿ ಪರಿಣಾಮಕಾರಿಯಾಗಿದೆ, ನಿಮ್ಮ ಅಂಗಗಳ ಸುತ್ತಲೂ ನಿರ್ಮಿಸುವ ಹಾನಿಕಾರಕ ಕೊಬ್ಬು. ಹೊಟ್ಟೆಯ ಕೊಬ್ಬನ್ನು ಕಡಿಮೆ ಮಾಡುವುದರಿಂದ ಚಯಾಪಚಯ ಆರೋಗ್ಯದಲ್ಲಿ ಪ್ರಮುಖ ಸುಧಾರಣೆಗಳು ಕಂಡುಬರುತ್ತವೆ','kn'),('emp1','AD163P','ಸಾಕಷ್ಟು ಸಂಸ್ಕರಿಸಿದ ಕಾರ್ಬ್‌ಗಳನ್ನು ತಿನ್ನಬೇಡಿ','7','ಎಲ್ಲಾ ಕಾರ್ಬ್‌ಗಳನ್ನು ಸಮಾನವಾಗಿ ರಚಿಸಲಾಗಿಲ್ಲ. ಅವುಗಳ ಫೈಬರ್ ಅನ್ನು ತೆಗೆದುಹಾಕಲು ಸಂಸ್ಕರಿಸಿದ ಕಾರ್ಬ್‌ಗಳನ್ನು ಹೆಚ್ಚು ಸಂಸ್ಕರಿಸಲಾಗಿದೆ. ಅವು ಪೋಷಕಾಂಶಗಳಲ್ಲಿ ಕಡಿಮೆ ಮತ್ತು ಹೆಚ್ಚಿನ ಪ್ರಮಾಣದಲ್ಲಿ ಸೇವಿಸಿದಾಗ ನಿಮ್ಮ ಆರೋಗ್ಯಕ್ಕೆ ಹಾನಿ ಮಾಡುತ್ತದೆ.','kn'),('emp1','AD163P','ಸಾಕಷ್ಟು ಗಿಡಮೂಲಿಕೆಗಳು ಮತ್ತು ಮಸಾಲೆಗಳನ್ನು ಬಳಸಿ','8','ಅನೇಕ ನಂಬಲಾಗದಷ್ಟು ಆರೋಗ್ಯಕರ-ಗಿಡಮೂಲಿಕೆಗಳು ಮತ್ತು ಮಸಾಲೆಗಳು ಅಸ್ತಿತ್ವದಲ್ಲಿವೆ. ಉದಾಹರಣೆಗೆ, ಶುಂಠಿ ಮತ್ತು ಅರಿಶಿನ ಎರಡೂ ಪ್ರಬಲವಾದ ಉರಿಯೂತದ ಮತ್ತು ಉತ್ಕರ್ಷಣ ನಿರೋಧಕ ಪರಿಣಾಮಗಳನ್ನು ಹೊಂದಿವೆ, ಇದು ವಿವಿಧ ಆರೋಗ್ಯ ಪ್ರಯೋಜನಗಳಿಗೆ ಕಾರಣವಾಗುತ್ತದೆ','kn'),('emp1','AD163P','ಸ್ಯಾಚುರೇಟೆಡ್ ಕೊಬ್ಬನ್ನು ಭಯಪಡಬೇಡಿ','9','ಸ್ಯಾಚುರೇಟೆಡ್ ಕೊಬ್ಬು ವಿವಾದಾಸ್ಪದವಾಗಿದೆ. ಸ್ಯಾಚುರೇಟೆಡ್ ಕೊಬ್ಬು ಕೊಲೆಸ್ಟ್ರಾಲ್ ಮಟ್ಟವನ್ನು ಹೆಚ್ಚಿಸುತ್ತದೆ ಎಂಬುದು ನಿಜವಾಗಿದ್ದರೂ, ಇದು ಎಚ್ಡಿಎಲ್ (ಉತ್ತಮ) ಕೊಲೆಸ್ಟ್ರಾಲ್ ಅನ್ನು ಹೆಚ್ಚಿಸುತ್ತದೆ ಮತ್ತು ನಿಮ್ಮ ಎಲ್ಡಿಎಲ್ (ಕೆಟ್ಟ) ಕಣಗಳನ್ನು ಕುಗ್ಗಿಸುತ್ತದೆ, ಇದು ಹೃದಯ ಕಾಯಿಲೆಯ ಕಡಿಮೆ ಅಪಾಯಕ್ಕೆ ಸಂಬಂಧಿಸಿದೆ','kn');
/*!40000 ALTER TABLE `masteremployeesuggestions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:46:49
