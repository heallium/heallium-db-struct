-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employeewellnessactionoptionlist`
--

DROP TABLE IF EXISTS `employeewellnessactionoptionlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `employeewellnessactionoptionlist` (
  `actionID` int(11) NOT NULL,
  `actionOptionID` varchar(45) NOT NULL,
  `clientID` varchar(45) NOT NULL,
  KEY `fk_clientID_weellList_idx` (`clientID`),
  CONSTRAINT `fk_clientID_weellList` FOREIGN KEY (`clientID`) REFERENCES `masterclient` (`clientID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employeewellnessactionoptionlist`
--

LOCK TABLES `employeewellnessactionoptionlist` WRITE;
/*!40000 ALTER TABLE `employeewellnessactionoptionlist` DISABLE KEYS */;
INSERT INTO `employeewellnessactionoptionlist` VALUES (1,'1','AD163P'),(1,'2','AD163P'),(2,'3','AD163P'),(2,'4','AD163P'),(3,'5','AD163P'),(3,'6','AD163P'),(3,'7','AD163P'),(3,'8','AD163P'),(4,'9','AD163P'),(5,'10','AD163P'),(5,'11','AD163P'),(6,'12','AD163P'),(6,'13','AD163P'),(6,'14','AD163P'),(6,'15','AD163P'),(7,'16','AD163P'),(8,'17','AD163P'),(8,'18','AD163P'),(9,'19','AD163P'),(9,'20','AD163P'),(10,'21','AD163P'),(11,'22','AD163P'),(11,'23','AD163P'),(12,'24','AD163P'),(12,'25','AD163P'),(13,'26','AD163P'),(13,'27','AD163P'),(14,'28','AD163P'),(14,'29','AD163P'),(15,'30','AD163P'),(15,'31','AD163P'),(16,'32','AD163P'),(16,'33','AD163P'),(17,'34','AD163P'),(18,'35','AD163P'),(18,'36','AD163P'),(18,'37','AD163P'),(18,'38','AD163P'),(19,'39','AD163P'),(19,'40','AD163P'),(20,'41','AD163P'),(20,'42','AD163P'),(20,'43','AD163P'),(21,'44','AD163P'),(21,'45','AD163P'),(22,'46','AD163P'),(22,'47','AD163P'),(22,'48','AD163P'),(22,'49','AD163P'),(23,'50','AD163P'),(23,'51','AD163P'),(24,'52','AD163P'),(25,'53','AD163P'),(25,'54','AD163P'),(26,'55','AD163P'),(26,'56','AD163P'),(26,'57','AD163P'),(26,'58','AD163P'),(27,'59','AD163P'),(27,'60','AD163P'),(28,'61','AD163P'),(28,'62','AD163P'),(29,'63','AD163P'),(29,'64','AD163P'),(30,'65','AD163P'),(30,'66','AD163P'),(30,'67','AD163P'),(30,'68','AD163P'),(31,'69','AD163P'),(31,'70','AD163P'),(32,'71','AD163P'),(32,'72','AD163P'),(33,'73','AD163P'),(33,'74','AD163P'),(34,'75','AD163P'),(34,'76','AD163P'),(35,'77','AD163P'),(35,'78','AD163P'),(36,'79','AD163P'),(36,'80','AD163P'),(36,'81','AD163P'),(36,'82','AD163P'),(36,'83','AD163P'),(37,'84','AD163P'),(38,'85','AD163P'),(38,'86','AD163P'),(39,'87','AD163P'),(39,'88','AD163P'),(40,'89','AD163P'),(40,'90','AD163P'),(41,'91','AD163P'),(41,'92','AD163P'),(41,'93','AD163P'),(41,'94','AD163P'),(42,'95','AD163P'),(42,'96','AD163P'),(43,'97','AD163P'),(43,'98','AD163P'),(44,'99','AD163P'),(44,'100','AD163P'),(44,'101','AD163P'),(44,'102','AD163P'),(45,'103','AD163P'),(45,'104','AD163P'),(46,'105','AD163P'),(46,'106','AD163P'),(47,'107','AD163P'),(47,'108','AD163P'),(48,'109','AD163P'),(49,'110','AD163P'),(50,'111','AD163P'),(50,'112','AD163P'),(51,'113','AD163P'),(51,'114','AD163P'),(52,'115','AD163P'),(52,'116','AD163P'),(53,'117','AD163P'),(53,'118','AD163P'),(54,'119','AD163P'),(54,'120','AD163P'),(55,'121','AD163P'),(55,'122','AD163P'),(56,'123','AD163P'),(56,'124','AD163P'),(56,'125','AD163P'),(56,'126','AD163P'),(57,'127','AD163P'),(58,'128','AD163P'),(58,'129','AD163P'),(59,'130','AD163P'),(59,'131','AD163P'),(59,'132','AD163P'),(60,'133','AD163P');
/*!40000 ALTER TABLE `employeewellnessactionoptionlist` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:46:15
