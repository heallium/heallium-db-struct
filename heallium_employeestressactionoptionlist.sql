-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employeestressactionoptionlist`
--

DROP TABLE IF EXISTS `employeestressactionoptionlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `employeestressactionoptionlist` (
  `actionID` int(11) NOT NULL,
  `actionOptionID` varchar(45) NOT NULL,
  `clientID` varchar(45) NOT NULL,
  KEY `fk_client_actionList_idx` (`clientID`),
  KEY `fk_action_option_List_idx` (`actionOptionID`),
  CONSTRAINT `fk_client_actionList` FOREIGN KEY (`clientID`) REFERENCES `masterclient` (`clientID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employeestressactionoptionlist`
--

LOCK TABLES `employeestressactionoptionlist` WRITE;
/*!40000 ALTER TABLE `employeestressactionoptionlist` DISABLE KEYS */;
INSERT INTO `employeestressactionoptionlist` VALUES (1,'1','AD163P'),(1,'2','AD163P'),(2,'3','AD163P'),(2,'4','AD163P'),(3,'5','AD163P'),(4,'6','AD163P'),(4,'7','AD163P'),(5,'8','AD163P'),(5,'9','AD163P'),(6,'10','AD163P'),(6,'11','AD163P'),(6,'12','AD163P'),(6,'13','AD163P'),(7,'14','AD163P'),(7,'15','AD163P'),(8,'16','AD163P'),(8,'17','AD163P'),(9,'18','AD163P'),(9,'19','AD163P'),(9,'20','AD163P'),(9,'21','AD163P'),(9,'22','AD163P'),(10,'23','AD163P'),(10,'24','AD163P'),(10,'25','AD163P'),(10,'26','AD163P'),(11,'27','AD163P'),(12,'28','AD163P'),(12,'29','AD163P'),(13,'30','AD163P'),(13,'31','AD163P'),(14,'32','AD163P'),(14,'33','AD163P'),(15,'34','AD163P'),(15,'35','AD163P'),(16,'36','AD163P'),(16,'37','AD163P'),(17,'38','AD163P'),(17,'39','AD163P'),(18,'40','AD163P'),(18,'41','AD163P'),(19,'42','AD163P'),(19,'43','AD163P'),(20,'44','AD163P'),(20,'45','AD163P'),(21,'46','AD163P'),(21,'47','AD163P'),(22,'48','AD163P'),(22,'49','AD163P'),(23,'50','AD163P'),(23,'51','AD163P'),(24,'52','AD163P'),(24,'53','AD163P'),(25,'54','AD163P'),(25,'55','AD163P'),(26,'56','AD163P'),(27,'57','AD163P'),(27,'58','AD163P'),(28,'59','AD163P'),(28,'60','AD163P'),(29,'61','AD163P'),(29,'62','AD163P'),(30,'63','AD163P'),(30,'64','AD163P'),(30,'65','AD163P'),(30,'66','AD163P'),(31,'67','AD163P'),(31,'68','AD163P'),(32,'69','AD163P'),(32,'70','AD163P'),(33,'71','AD163P'),(33,'72','AD163P'),(34,'73','AD163P'),(34,'74','AD163P'),(35,'75','AD163P'),(35,'76','AD163P'),(36,'77','AD163P'),(37,'78','AD163P'),(37,'79','AD163P'),(38,'80','AD163P'),(38,'81','AD163P'),(39,'82','AD163P'),(39,'83','AD163P'),(40,'84','AD163P'),(40,'85','AD163P'),(41,'86','AD163P'),(41,'87','AD163P'),(42,'88','AD163P'),(42,'89','AD163P'),(43,'90','AD163P'),(43,'91','AD163P'),(44,'92','AD163P'),(44,'93','AD163P'),(45,'94','AD163P'),(45,'95','AD163P'),(46,'96','AD163P'),(46,'97','AD163P'),(47,'98','AD163P'),(47,'99','AD163P'),(48,'100','AD163P'),(48,'101','AD163P'),(49,'102','AD163P'),(49,'103','AD163P'),(49,'104','AD163P'),(50,'105','AD163P'),(50,'106','AD163P'),(51,'107','AD163P'),(51,'108','AD163P'),(52,'109','AD163P'),(52,'110','AD163P'),(53,'111','AD163P'),(53,'112','AD163P'),(54,'113','AD163P'),(54,'114','AD163P'),(54,'115','AD163P'),(54,'116','AD163P'),(55,'117','AD163P'),(55,'118','AD163P'),(56,'119','AD163P'),(56,'120','AD163P'),(57,'121','AD163P'),(57,'122','AD163P'),(58,'123','AD163P'),(59,'124','AD163P'),(59,'125','AD163P'),(59,'126','AD163P'),(60,'127','AD163P');
/*!40000 ALTER TABLE `employeestressactionoptionlist` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:45:49
