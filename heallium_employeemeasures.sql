-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employeemeasures`
--

DROP TABLE IF EXISTS `employeemeasures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `employeemeasures` (
  `employeeMeasuresID` varchar(45) NOT NULL,
  `employeeMeasuresDetails` varchar(1050) NOT NULL,
  `clientID` varchar(45) DEFAULT NULL,
  `employeeID` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employeemeasures`
--

LOCK TABLES `employeemeasures` WRITE;
/*!40000 ALTER TABLE `employeemeasures` DISABLE KEYS */;
INSERT INTO `employeemeasures` VALUES ('1','Use plenty of herbs and spices. ','AD163P','emp1'),('2','Never skip your yoga or exercise session. ','AD163P','emp1'),('3','Kick your alcohol and smoking habit. ','AD163P','emp1'),('4','Eat by the clock  not by your stomach. ','AD163P','emp1'),('5','Eat regular meals and avoid dieting.','AD163P','emp2'),('6','Reduce soda  salt and sugar intake.','AD163P','emp2'),('7','Give up soft drinks and avoid microwaved food.','AD163P','emp2'),('8','Limit other oils and use olive oil.','AD163P','emp2'),('9','Try doing endurance sports.','AD163P','emp2'),('10','Bring on the broccoli and green veggies.','AD163P','emp3'),('11','Replace saturated with unsaturated fat.','AD163P','emp3'),('12','Go for ginger,garlic and tulsi.','AD163P','emp3'),('13','Eat regularly  control the portion size.','AD163P','emp3'),('14','Reduce your caffeine intake.','AD163P','emp3');
/*!40000 ALTER TABLE `employeemeasures` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:45:04
