-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `stressquestionoption`
--

DROP TABLE IF EXISTS `stressquestionoption`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `stressquestionoption` (
  `optionID` varchar(45) NOT NULL,
  `optionText` varchar(150) NOT NULL,
  `points` int(11) NOT NULL,
  `questionID` int(11) NOT NULL,
  `actionID` int(11) NOT NULL,
  `languageID` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`optionID`),
  KEY `fk_action_stressQuestionOption_idx` (`actionID`),
  KEY `fk_questionID_A_idx` (`questionID`),
  CONSTRAINT `fk_questionID_A` FOREIGN KEY (`questionID`) REFERENCES `masterstressquestion` (`questionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stressquestionoption`
--

LOCK TABLES `stressquestionoption` WRITE;
/*!40000 ALTER TABLE `stressquestionoption` DISABLE KEYS */;
INSERT INTO `stressquestionoption` VALUES ('1','Mostly',0,1,1,'en'),('10','Supervision',0,3,10,'en'),('100','ಸ್ವಲ್ಪ ಮಟ್ಟಿಗೆ',0,28,100,'kn'),('101','ತೃಪ್ತಿಕರಕ್ಕೆ',0,28,101,'kn'),('102','ಇಲ್ಲವೇ ಇಲ್ಲ',0,28,102,'kn'),('103','ಹೌದು',0,29,103,'kn'),('104','ಇಲ್ಲ',1,29,104,'kn'),('105','ಹೌದು',0,30,105,'kn'),('106','ಇಲ್ಲ ',1,30,106,'kn'),('11','Workgroup',1,3,11,'en'),('12','Social Injustice',0,3,12,'en'),('13','Great',1,4,13,'en'),('14','Satisfied',0,4,14,'en'),('15','Unable to concentrate',1,4,15,'en'),('16','Frustrated',0,4,16,'en'),('17','Depressed',1,4,17,'en'),('18','Yes',0,5,18,'en'),('19','No',1,5,19,'en'),('2','Rarely',1,1,2,'en'),('20','	40 – 50',1,6,20,'en'),('21','50 – 60',0,6,21,'en'),('22','60 – Above',0,6,22,'en'),('23','Workload has decreased',1,7,23,'en'),('24','Remained the',0,7,24,'en'),('25','Same',1,7,25,'en'),('26','Workload increased',0,7,26,'en'),('27','Dealing with Customers/ Colleagues',1,8,27,'en'),('28','Administration',1,8,28,'en'),('29','Need to hit targets/deadlines',0,8,29,'en'),('3','Sometimes',0,1,3,'en'),('30','	Long working hours',0,8,30,'en'),('31','Feeling work not valued',1,9,31,'en'),('32','Lack of management support',0,9,32,'en'),('33','Over competitive/ confrontational institutional culture',0,9,33,'en'),('34','Incentive Policy',0,9,34,'en'),('35','Superior',1,10,35,'en'),('36','Colleagues (discussion)',0,10,36,'en'),('37','Function Head',1,10,37,'en'),('38','Head of HR department',1,10,38,'en'),('39','Mostly',1,11,39,'en'),('4','Not at all',1,1,4,'en'),('40','Rarely',0,11,40,'en'),('41','Frequently',0,11,41,'en'),('42','Not at all',1,11,42,'en'),('43','Optimistically',1,12,43,'en'),('44','With the help of others',0,12,44,'en'),('45','Depends upon level',0,12,45,'en'),('46','Completely',1,13,46,'en'),('47','To a certain extent',0,13,47,'en'),('48','To a satisfactory',0,13,48,'en'),('49','Not at all',0,13,49,'en'),('5','Mostly',1,2,5,'en'),('50','Yes',0,14,50,'en'),('51','No',1,14,51,'en'),('52','Yes',0,15,52,'en'),('53','No',1,15,53,'en'),('54','ಹೆಚ್ಚಾಗಿ',0,16,54,'kn'),('55','ಅಪರೂಪ',1,16,55,'kn'),('56','ಕೆಲವೊಮ್ಮೆ',0,16,56,'kn'),('57','ಇಲ್ಲವೇ ಇಲ್ಲ',1,16,57,'kn'),('58','ಹೆಚ್ಚಾಗಿ',1,17,58,'kn'),('59','ಅಪರೂಪ',0,17,59,'kn'),('6','Rarely',0,2,6,'en'),('60','ಕೆಲವೊಮ್ಮೆ',1,17,60,'kn'),('61','ಇಲ್ಲವೇ ಇಲ್ಲ',0,17,61,'kn'),('62','ಮೇಲ್ವಿಚಾರಣೆ',0,18,62,'kn'),('63','ಕಾರ್ಯ ಸಮೂಹ',1,18,63,'kn'),('64','ಸಾಮಾಜಿಕ ಅನ್ಯಾಯ',0,18,64,'kn'),('65','ಕೆಲಸದ ವಾತಾವರಣ',1,18,65,'kn'),('66','ಅದ್ಭುತವಾಗಿದೆ',1,19,66,'kn'),('67','ತೃಪ್ತಿ',0,19,67,'kn'),('68','ಏಕಾಗ್ರತೆ ಸಾಧ್ಯವಿಲ್ಲ',1,19,68,'kn'),('69','ನಿರಾಶೆಗೊಂಡ',0,19,69,'kn'),('7','Sometimes',1,2,7,'en'),('70','ಖಿನ್ನತೆಗೆ ಒಳಗಾಗಿದೆ',1,19,70,'kn'),('71','ಹೌದು',0,20,71,'kn'),('72','ಇಲ್ಲ',1,20,72,'kn'),('73','40 - 50',1,21,73,'kn'),('74','50 - 60',0,21,74,'kn'),('75','60 - ಮೇಲೆ',0,21,75,'kn'),('76','ಕೆಲಸದ ಹೊರೆ ಕಡಿಮೆಯಾಗಿದೆ',1,22,76,'kn'),('77','ಉಳಿದಿದೆ',0,22,77,'kn'),('78','ಅದೇ',1,22,78,'kn'),('79','ಕೆಲಸದ ಹೊರೆ ಹೆಚ್ಚಾಗಿದೆ',0,22,79,'kn'),('8','Not at all',0,2,8,'en'),('80','ಗ್ರಾಹಕರು / ಸಹೋದ್ಯೋಗಿಗಳೊಂದಿಗೆ ವ್ಯವಹರಿಸುವುದು',1,23,80,'kn'),('81','ಆಡಳಿತ',1,23,81,'kn'),('82','ಗುರಿ / ಗಡುವನ್ನು ಹೊಡೆಯುವ ಅಗತ್ಯವಿದೆ',0,23,82,'kn'),('83','ದೀರ್ಘ ಕೆಲಸದ ಸಮಯ',0,23,83,'kn'),('84','ಕೆಲಸದ ಮೌಲ್ಯವಿಲ್ಲ ಎಂದು ಭಾವಿಸುತ್ತಿದೆ',1,24,84,'kn'),('85','ನಿರ್ವಹಣಾ ಬೆಂಬಲದ ಕೊರತೆ',0,24,85,'kn'),('86','ಸ್ಪರ್ಧಾತ್ಮಕ / ಮುಖಾಮುಖಿ ಸಾಂಸ್ಥಿಕ ಸಂಸ್ಕೃತಿಯ ಮೇಲೆ',0,24,86,'kn'),('87','ಪ್ರೋತ್ಸಾಹಕ ನೀತಿ',0,24,87,'kn'),('88','ಉನ್ನತ',1,25,88,'kn'),('89','ಸಹೋದ್ಯೋಗಿಗಳು (ಚರ್ಚೆ)',0,25,89,'kn'),('9','Work Environment',1,3,9,'en'),('90','ಫಂಕ್ಷನ್ ಹೆಡ್',1,25,90,'kn'),('91','ಮಾನವ ಸಂಪನ್ಮೂಲ ವಿಭಾಗದ ಮುಖ್ಯಸ್ಥ',1,25,91,'kn'),('92','ಹೆಚ್ಚಾಗಿ',1,26,92,'kn'),('93','ಅಪರೂಪ',0,26,93,'kn'),('94','ಆಗಾಗ್ಗೆ',0,26,94,'kn'),('95','ಇಲ್ಲವೇ ಇಲ್ಲ',1,26,95,'kn'),('96','ಆಶಾವಾದಿಯಾಗಿ',1,27,96,'kn'),('97','ಇತರರ ಸಹಾಯದಿಂದ',0,27,97,'kn'),('98','ಮಟ್ಟವನ್ನು ಅವಲಂಬಿಸಿರುತ್ತದೆ',0,27,98,'kn'),('99','ಸಂಪೂರ್ಣವಾಗಿ',1,28,99,'kn');
/*!40000 ALTER TABLE `stressquestionoption` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:46:52
