-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `masterteam`
--

DROP TABLE IF EXISTS `masterteam`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `masterteam` (
  `teamID` varchar(45) NOT NULL,
  `clientID` varchar(45) NOT NULL,
  `teamName` varchar(45) NOT NULL,
  `projectID` varchar(45) DEFAULT NULL,
  `branchID` varchar(45) DEFAULT NULL,
  `cityID` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`teamID`),
  KEY `fk_c_team_idx` (`clientID`),
  KEY `fk_p_t_t_idx` (`projectID`),
  CONSTRAINT `fk_c_team` FOREIGN KEY (`clientID`) REFERENCES `masterclient` (`clientID`),
  CONSTRAINT `fk_p_t_t` FOREIGN KEY (`projectID`) REFERENCES `masterproject` (`projectID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `masterteam`
--

LOCK TABLES `masterteam` WRITE;
/*!40000 ALTER TABLE `masterteam` DISABLE KEYS */;
INSERT INTO `masterteam` VALUES ('AT77S','AD163P','Men of Genius ','B1744','M742187','553471'),('AT97I','AD163P','Men on a Mission','B1744','M742187','553471'),('BT12A','AD163P','Over Achievers ','H6649','M742187','553471'),('BT44K','AD163P','Policy Makers ','Z6381','M742187','553471'),('HT12D','AD163P','Power House','H7558','W612441','553471'),('HT66F','AD163P','Statesmen','H7558','W612441','553471'),('KT99X','AD163P','Squadron ','B8119','W612441','553471'),('MT67V','AD163P','The Capitalist','B8119','W612441','553471'),('PT33M','AD163P','The Elite','P1741','M742187','553471'),('QT78H','AD163P','The Executives ','P1741','M742187','553471'),('ST36Q','AD163P','The Godfathers','Z6381','M742187','553471'),('ZT88P','AD163P','The Hive','H6649','M742187','553471');
/*!40000 ALTER TABLE `masterteam` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:46:28
