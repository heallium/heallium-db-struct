-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `masterlifestylequestion`
--

DROP TABLE IF EXISTS `masterlifestylequestion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `masterlifestylequestion` (
  `questionID` int(11) NOT NULL,
  `description` varchar(300) NOT NULL,
  `optionFormat` int(11) NOT NULL,
  `languageID` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`questionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `masterlifestylequestion`
--

LOCK TABLES `masterlifestylequestion` WRITE;
/*!40000 ALTER TABLE `masterlifestylequestion` DISABLE KEYS */;
INSERT INTO `masterlifestylequestion` VALUES (1,'I avoid excess sugar',0,'en'),(2,'I like learning new things',0,'en'),(3,'I have the motivation to realize my plans',0,'en'),(4,'I eat a healthy balanced diet according to the official nutritional guidelines',0,'en'),(5,'I avoid excess fat',0,'en'),(6,'I make sure I get enough fiber and vitamins',0,'en'),(7,'I eat meat less than three times a week',0,'en'),(8,'I drink less than three cups of coffee or tea a day',0,'en'),(9,'I avoid excess salt',0,'en'),(10,'I feel good about what i do (studies, job, being a full-time mother/father, etc.)',0,'en'),(11,'I drink less than 5 or (three drinks) of alcohol per week',0,'en'),(12,'I smoke less than 1 pack of cigarettes per week (less than 3 a day)',0,'en'),(13,'During my usual day, I am physically active (I take the stairs instead of the elevator, I walk)',0,'en'),(14,'During my usual day, I take time to stretch and to relax',0,'en'),(15,'I exercise at least once a week',0,'en'),(16,'ನಾನು ಹೆಚ್ಚುವರಿ ಸಕ್ಕರೆಯನ್ನು ತಪ್ಪಿಸುತ್ತೇನೆ.',0,'kn'),(17,'ನಾನು ಹೊಸ ವಿಷಯಗಳನ್ನು ಕಲಿಯಲು ಇಷ್ಟಪಡುತ್ತೇನೆ.',0,'kn'),(18,'ನನ್ನ ಯೋಜನೆಗಳನ್ನು ಸಾಕಾರಗೊಳಿಸುವ ಪ್ರೇರಣೆ ನನ್ನಲ್ಲಿದೆ.',0,'kn'),(19,'ಅಧಿಕೃತ ಪೌಷ್ಠಿಕಾಂಶದ ಮಾರ್ಗಸೂಚಿಗಳ ಪ್ರಕಾರ ನಾನು ಆರೋಗ್ಯಕರ ಸಮತೋಲಿತ ಆಹಾರವನ್ನು ಸೇವಿಸುತ್ತೇನೆ.',0,'kn'),(20,'ನಾನು ಹೆಚ್ಚುವರಿ ಕೊಬ್ಬನ್ನು ತಪ್ಪಿಸುತ್ತೇನೆ.',0,'kn'),(21,'ನಾನು ಸಾಕಷ್ಟು ಫೈಬರ್ ಮತ್ತು ವಿಟಮಿನ್ಗಳನ್ನು ಪಡೆಯುತ್ತೇನೆ ಎಂದು ಖಚಿತಪಡಿಸಿಕೊಳ್ಳುತ್ತೇನೆ.',0,'kn'),(22,'ನಾನು ವಾರದಲ್ಲಿ ಮೂರು ಬಾರಿ ಕಡಿಮೆ ಮಾಂಸವನ್ನು ತಿನ್ನುತ್ತೇನೆ.',0,'kn'),(23,'ನಾನು ದಿನಕ್ಕೆ ಮೂರು ಕಪ್ ಕಾಫಿ ಅಥವಾ ಚಹಾಕ್ಕಿಂತ ಕಡಿಮೆ ಕುಡಿಯುತ್ತೇನೆ.',0,'kn'),(24,'ನಾನು ಹೆಚ್ಚುವರಿ ಉಪ್ಪನ್ನು ತಪ್ಪಿಸುತ್ತೇನೆ.',0,'kn'),(25,'ನಾನು ಏನು ಮಾಡುತ್ತೇನೆ (ಅಧ್ಯಯನ, ಕೆಲಸ, ಪೂರ್ಣ ಸಮಯದ ತಾಯಿ / ತಂದೆ, ಇತ್ಯಾದಿ) ಬಗ್ಗೆ ನನಗೆ ಒಳ್ಳೆಯದಾಗಿದೆ.',0,'kn'),(26,'ನಾನು ವಾರಕ್ಕೆ 5 z ನ್ಸ್ (ಮೂರು ಪಾನೀಯಗಳು) ಗಿಂತ ಕಡಿಮೆ ಆಲ್ಕೊಹಾಲ್ ಕುಡಿಯುತ್ತೇನೆ.',0,'kn'),(27,'ನಾನು ವಾರಕ್ಕೆ 1 ಪ್ಯಾಕ್ ಸಿಗರೇಟ್ ಗಿಂತ ಕಡಿಮೆ ಧೂಮಪಾನ ಮಾಡುತ್ತೇನೆ (ದಿನಕ್ಕೆ 3 ಕ್ಕಿಂತ ಕಡಿಮೆ).',0,'kn'),(28,'ನನ್ನ ಸಾಮಾನ್ಯ ದಿನದಲ್ಲಿ, ನಾನು ದೈಹಿಕವಾಗಿ ಸಕ್ರಿಯನಾಗಿರುತ್ತೇನೆ (ನಾನು ಲಿಫ್ಟ್‌ನ ಬದಲು ಮೆಟ್ಟಿಲುಗಳನ್ನು ತೆಗೆದುಕೊಳ್ಳುತ್ತೇನೆ, ನಾನು ನಡೆಯುತ್ತೇನೆ).',0,'kn'),(29,'ನನ್ನ ಸಾಮಾನ್ಯ ದಿನದಲ್ಲಿ, ನಾನು ಹಿಗ್ಗಿಸಲು ಮತ್ತು ವಿಶ್ರಾಂತಿ ಪಡೆಯಲು ಸಮಯ ತೆಗೆದುಕೊಳ್ಳುತ್ತೇನೆ.',0,'kn'),(30,'ನಾನು ವಾರಕ್ಕೊಮ್ಮೆಯಾದರೂ ವ್ಯಾಯಾಮ ಮಾಡುತ್ತೇನೆ ',0,'kn');
/*!40000 ALTER TABLE `masterlifestylequestion` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:46:42
