-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `completedtest`
--

DROP TABLE IF EXISTS `completedtest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `completedtest` (
  `employeeName` varchar(45) NOT NULL,
  `employeeID` varchar(45) NOT NULL,
  `clientID` varchar(45) DEFAULT NULL,
  `locationID` varchar(45) NOT NULL,
  `branchID` varchar(45) NOT NULL,
  `projectID` varchar(45) NOT NULL,
  `teamID` varchar(45) NOT NULL,
  `groupPathID` varchar(45) DEFAULT NULL,
  `testDate` date DEFAULT NULL,
  KEY `fk_c_r_idx` (`clientID`),
  KEY `fk_b_rr_idx` (`branchID`),
  KEY `fk_p_rr_idx` (`projectID`),
  KEY `fk_t_rr_idx` (`teamID`),
  KEY `fk_g_rr_idx` (`groupPathID`),
  CONSTRAINT `fk_b_rr` FOREIGN KEY (`branchID`) REFERENCES `masterclientbranch` (`branchID`),
  CONSTRAINT `fk_c_r` FOREIGN KEY (`clientID`) REFERENCES `masterclient` (`clientID`),
  CONSTRAINT `fk_g_rr` FOREIGN KEY (`groupPathID`) REFERENCES `mastergrouppath` (`groupPathID`),
  CONSTRAINT `fk_p_rr` FOREIGN KEY (`projectID`) REFERENCES `masterproject` (`projectID`),
  CONSTRAINT `fk_t_rr` FOREIGN KEY (`teamID`) REFERENCES `masterteam` (`teamID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `completedtest`
--

LOCK TABLES `completedtest` WRITE;
/*!40000 ALTER TABLE `completedtest` DISABLE KEYS */;
INSERT INTO `completedtest` VALUES ('Emp1L1','Pramodh','AD163P','553471','M742187','B1744','AT77S','IF51J','2019-01-10'),('Emp2L2','Rajesh','AD163P','553471','M742187','B1744','AT77S','IF51J','2019-01-10'),('Emp3L3','Puneeth','AD163P','553471','M742187','B1744','AT77S','IF51J','2019-01-10'),('Emp4L4','Chadan','AD163P','553471','M742187','B1744','AT77S','IF51J','2019-01-10'),('Emp5L5','Shivaraj','AD163P','553471','M742187','B1744','AT77S','IF51J','2019-01-10'),('Emp6L6','Kiran','AD163P','553471','M742187','B1744','AT77S','IF51J','2019-01-10'),('Emp7L7','Siddu','AD163P','553471','M742187','B1744','AT77S','IF51J','2019-01-10'),('Emp8L8','Suresh','AD163P','553471','M742187','B1744','AT77S','IF51J','2019-01-10'),('Emp9L9','Anil','AD163P','553471','M742187','B1744','AT77S','IF51J','2019-01-10'),('Emp10L10','Ravi','AD163P','553471','M742187','B1744','AT77S','IF51J','2019-01-10'),('Emp11L11','Aravind','AD163P','553471','M742187','B1744','AT97I','ASP32K','2019-01-11'),('Emp12L12','Hari ','AD163P','553471','M742187','B1744','AT97I','ASP32K','2019-01-11'),('Emp13L13','Sachin','AD163P','553471','M742187','B1744','AT97I','ASP32K','2019-01-11'),('Emp14L14','Arun','AD163P','553471','M742187','B1744','AT97I','ASP32K','2019-01-11'),('Emp15L15','Aditya','AD163P','553471','M742187','B1744','AT97I','ASP32K','2019-01-11'),('Emp16L16','Santhosh','AD163P','553471','M742187','B1744','AT97I','ASP32K','2019-01-11'),('Emp17L17','Vinod','AD163P','553471','M742187','B1744','AT97I','ASP32K','2019-01-11'),('Emp18L18','Laxmikanth','AD163P','553471','M742187','B1744','AT97I','ASP32K','2019-01-11'),('Emp19L19','Vijay','AD163P','553471','M742187','B1744','AT97I','ASP32K','2019-01-11'),('Emp20L20','Karthik','AD163P','553471','M742187','B1744','AT97I','ASP32K','2019-01-12');
/*!40000 ALTER TABLE `completedtest` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:45:08
