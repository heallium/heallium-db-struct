-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `masterbloodtestparameters`
--

DROP TABLE IF EXISTS `masterbloodtestparameters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `masterbloodtestparameters` (
  `parameterID` varchar(45) NOT NULL,
  `parameterType` varchar(45) NOT NULL,
  `minValue` int(20) NOT NULL,
  `maxValue` int(20) NOT NULL,
  `description` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `masterbloodtestparameters`
--

LOCK TABLES `masterbloodtestparameters` WRITE;
/*!40000 ALTER TABLE `masterbloodtestparameters` DISABLE KEYS */;
INSERT INTO `masterbloodtestparameters` VALUES ('B25','D BP',60,60,'GHV'),('BT1','Heamoglobin',11,141,'BA'),('BT11','SGPT',0,0,'F'),('BT12','TSH',0,0,'F'),('BT13','T4',0,0,'QW'),('BT14','T3',0,0,'D'),('BT15','B12',300,10000,'D'),('BT16','Uric Acid',7,9,'D'),('BT17','S BP',90,140,'D'),('BT2','LDL',70,100,'BB'),('BT20','Total cholestrol',100,200,'BJ'),('BT21','SGOT',0,0,'W'),('BT22','BMI',25,25,'IB'),('BT3','HDL',20,50,'BC'),('BT4','Triglyceride',100,150,'BD'),('BT5','FBS',80,105,'BE'),('BT6','Hb1c',6,7,'BF'),('BT7','Creatinine',0,1,'BG'),('BT8','Bilirubin',0,2,'BH'),('BT9','Alakaline phosphatis',0,200,'BI');
/*!40000 ALTER TABLE `masterbloodtestparameters` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:45:48
