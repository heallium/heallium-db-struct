-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lifestylemeasures`
--

DROP TABLE IF EXISTS `lifestylemeasures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `lifestylemeasures` (
  `lifestylemeasuresID` varchar(45) DEFAULT NULL,
  `lifestylemeasuresDetaills` varchar(1050) DEFAULT NULL,
  `clientID` varchar(45) DEFAULT NULL,
  `employeeID` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lifestylemeasures`
--

LOCK TABLES `lifestylemeasures` WRITE;
/*!40000 ALTER TABLE `lifestylemeasures` DISABLE KEYS */;
INSERT INTO `lifestylemeasures` VALUES ('1','Kick your Alcohol and Smoking habit. ','AD163P','emp1'),('2','Reduce soda, salt and sugar intake.','AD163P','emp1'),('3','Have a Protein-Rich Breakfast and make your breakfast mandatory.','AD163P','emp1'),('4','Fill up on Fiber which includes vegetables, fruits, beans and whole grains.','AD163P','emp1'),('5','Drink atleast 3 litres of water a day.','AD163P','emp1'),('6','Cut down on processed food and avoid trigger foods. ','AD163P','emp2'),('7','Base your diet on plenty of foods rich in carbohydrates.','AD163P','emp2'),('8','Replace saturated with unsaturated fat.','AD163P','emp2'),('9','Enjoy plenty of fruits and vegetables.','AD163P','emp2'),('10','Reduce salt and sugar intake.','AD163P','emp3'),('11','Eat regularly, control the portion size.','AD163P','emp3'),('12','Drink plenty of fluids.','AD163P','emp3'),('13','Maintain a healthy body weight.','AD163P','emp3');
/*!40000 ALTER TABLE `lifestylemeasures` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:45:54
