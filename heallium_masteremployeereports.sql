-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `masteremployeereports`
--

DROP TABLE IF EXISTS `masteremployeereports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `masteremployeereports` (
  `clientID` varchar(45) DEFAULT NULL,
  `employeeID` varchar(45) DEFAULT NULL,
  `reportID` varchar(45) DEFAULT NULL,
  `labName` varchar(45) DEFAULT NULL,
  `labID` varchar(45) DEFAULT NULL,
  `reportDate` varchar(45) DEFAULT NULL,
  `reportDescription` varchar(45) DEFAULT NULL,
  `languageID` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `masteremployeereports`
--

LOCK TABLES `masteremployeereports` WRITE;
/*!40000 ALTER TABLE `masteremployeereports` DISABLE KEYS */;
INSERT INTO `masteremployeereports` VALUES ('AD163P','emp1','r1','BatlaLab','lab1','2019-05-19','report1','en'),('AD163P','emp1','r2','ShankarLab','lab2','2019-06-25','report2','en'),('AD163P','emp1','r3','AbsoulteLab','lab3','2019-07-15','report3','en'),('AD163P','emp1','r4','Veerlab','lab4','2019-07-30','report4','en'),('AD163P','emp1','r5','KishoreLab','lab5','2019-04-15','report5','en'),('AD163P','emp1','r6','MohitLab','lab6','2019-07-19','report6','en'),('AD163P','emp1','r7','ZariyaLab','lab7','2019-07-15','report7','en'),('AD163P','emp1','r8','JindalLab','lab8','2019-08-15','report8','en'),('AD163P','emp1','r9','I&T Lab','lab9','2019-09-15','report9','en'),('AD163P','emp1','r10','WiproLab','lab10','2019-03-15','report10','en'),('AD163P','emp1','r1','ಬಟ್ಲಾಲ್ಯಾಬ್','lab1','2019-05-19','ವರದಿ 1','kn'),('AD163P','emp1','r2','ಶಂಕರ್ಲ್ಯಾಬ್','lab2','2019-06-25','ವರದಿ 2','kn'),('AD163P','emp1','r3','ಅಬ್ಸೌಲ್ಟೆಲ್ಯಾಬ್','lab3','2019-07-15','ವರದಿ 3','kn'),('AD163P','emp1','r4','ವೆರ್ಲಾಬ್','lab4','2019-07-30','ವರದಿ 4','kn'),('AD163P','emp1','r5','ಕಿಶೋರ್ಲ್ಯಾಬ್','lab5','2019-04-15','ವರದಿ 5','kn'),('AD163P','emp1','r6','ಮೊಹಿತ್ಲ್ಯಾಬ್','lab6','2019-07-19','ವರದಿ 6','kn'),('AD163P','emp1','r7','ಜರಿಯಾಲಾಬ್','lab7','2019-07-15','ವರದಿ 7','kn'),('AD163P','emp1','r8','ಜಿಂದಾಲ್ ಲ್ಯಾಬ್','lab8','2019-08-15','ವರದಿ 8','kn'),('AD163P','emp1','r9','ಎಲ್ & ಟಿ ಲ್ಯಾಬ್','lab9','2019-09-15','ವರದಿ 9','kn'),('AD163P','emp1','r10','ವಿಪ್ರೊಲ್ಯಾಬ್','lab10','2019-03-15','ವರದಿ 10','kn');
/*!40000 ALTER TABLE `masteremployeereports` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:45:41
