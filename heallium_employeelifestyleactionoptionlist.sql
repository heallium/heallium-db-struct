-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employeelifestyleactionoptionlist`
--

DROP TABLE IF EXISTS `employeelifestyleactionoptionlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `employeelifestyleactionoptionlist` (
  `actionID` int(11) NOT NULL,
  `actionOptionID` varchar(45) NOT NULL,
  `clientID` varchar(45) NOT NULL,
  KEY `fk_clientID_lifestylelist_idx` (`clientID`),
  CONSTRAINT `fk_clientID_lifestylelist` FOREIGN KEY (`clientID`) REFERENCES `masterclient` (`clientID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employeelifestyleactionoptionlist`
--

LOCK TABLES `employeelifestyleactionoptionlist` WRITE;
/*!40000 ALTER TABLE `employeelifestyleactionoptionlist` DISABLE KEYS */;
INSERT INTO `employeelifestyleactionoptionlist` VALUES (1,'1','AD163P'),(1,'2','AD163P'),(2,'3','AD163P'),(2,'4','AD163P'),(3,'5','AD163P'),(3,'6','AD163P'),(3,'7','AD163P'),(3,'8','AD163P'),(4,'9','AD163P'),(5,'10','AD163P'),(5,'11','AD163P'),(6,'12','AD163P'),(6,'13','AD163P'),(6,'14','AD163P'),(6,'15','AD163P'),(7,'16','AD163P'),(8,'17','AD163P'),(8,'18','AD163P'),(9,'19','AD163P'),(9,'20','AD163P'),(10,'21','AD163P'),(10,'22','AD163P'),(10,'23','AD163P'),(10,'24','AD163P'),(11,'25','AD163P'),(12,'26','AD163P'),(12,'27','AD163P'),(13,'28','AD163P'),(13,'29','AD163P'),(14,'30','AD163P'),(14,'31','AD163P'),(15,'32','AD163P'),(15,'33','AD163P'),(16,'34','AD163P'),(16,'35','AD163P'),(17,'36','AD163P'),(17,'37','AD163P'),(18,'38','AD163P'),(18,'39','AD163P'),(18,'40','AD163P'),(19,'41','AD163P'),(19,'42','AD163P'),(20,'43','AD163P'),(20,'44','AD163P'),(21,'45','AD163P'),(21,'46','AD163P'),(22,'47','AD163P'),(22,'48','AD163P'),(23,'49','AD163P'),(23,'50','AD163P'),(23,'51','AD163P'),(23,'52','AD163P'),(23,'53','AD163P'),(24,'54','AD163P'),(24,'55','AD163P'),(25,'56','AD163P'),(25,'57','AD163P'),(26,'58','AD163P'),(27,'59','AD163P'),(27,'60','AD163P'),(28,'61','AD163P'),(28,'62','AD163P'),(29,'63','AD163P'),(29,'64','AD163P'),(30,'65','AD163P'),(30,'66','AD163P'),(31,'67','AD163P'),(31,'68','AD163P'),(32,'69','AD163P'),(32,'70','AD163P'),(32,'71','AD163P'),(33,'72','AD163P'),(33,'73','AD163P'),(34,'74','AD163P'),(34,'75','AD163P'),(35,'76','AD163P'),(36,'77','AD163P'),(36,'78','AD163P'),(37,'79','AD163P'),(37,'80','AD163P'),(37,'81','AD163P'),(38,'82','AD163P'),(38,'83','AD163P'),(39,'84','AD163P'),(39,'85','AD163P'),(40,'86','AD163P'),(40,'87','AD163P'),(41,'88','AD163P'),(41,'89','AD163P'),(42,'90','AD163P'),(42,'91','AD163P'),(43,'92','AD163P'),(43,'93','AD163P'),(43,'94','AD163P'),(43,'95','AD163P'),(44,'96','AD163P'),(44,'97','AD163P'),(45,'98','AD163P'),(45,'99','AD163P'),(46,'100','AD163P'),(46,'101','AD163P'),(47,'102','AD163P'),(47,'103','AD163P'),(48,'104','AD163P'),(48,'105','AD163P'),(49,'106','AD163P'),(50,'107','AD163P'),(50,'108','AD163P'),(51,'109','AD163P'),(51,'110','AD163P'),(52,'111','AD163P'),(52,'112','AD163P'),(53,'113','AD163P'),(53,'114','AD163P'),(54,'115','AD163P'),(54,'116','AD163P'),(54,'117','AD163P'),(54,'118','AD163P'),(55,'119','AD163P'),(55,'120','AD163P'),(56,'122','AD163P'),(56,'123','AD163P'),(57,'124','AD163P'),(57,'125','AD163P'),(58,'126','AD163P'),(58,'127','AD163P'),(58,'128','AD163P'),(58,'129','AD163P'),(59,'130','AD163P'),(60,'131','AD163P'),(60,'132','AD163P'),(61,'133','AD163P'),(61,'134','AD163P'),(62,'135','AD163P'),(62,'136','AD163P'),(63,'137','AD163P'),(63,'138','AD163P'),(64,'139','AD163P'),(64,'140','AD163P'),(65,'141','AD163P'),(65,'142','AD163P'),(66,'143','AD163P'),(66,'144','AD163P'),(67,'145','AD163P'),(68,'146','AD163P'),(68,'147','AD163P'),(69,'148','AD163P'),(69,'149','AD163P'),(70,'150','AD163P'),(70,'151','AD163P'),(71,'152','AD163P'),(71,'153','AD163P'),(72,'154','AD163P'),(72,'155','AD163P'),(73,'156','AD163P'),(73,'157','AD163P'),(74,'158','AD163P'),(74,'159','AD163P'),(75,'160','AD163P');
/*!40000 ALTER TABLE `employeelifestyleactionoptionlist` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:45:30
