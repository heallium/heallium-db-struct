-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employeehealthindexscoretestresult`
--

DROP TABLE IF EXISTS `employeehealthindexscoretestresult`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `employeehealthindexscoretestresult` (
  `employeeID` varchar(45) NOT NULL,
  `clientID` varchar(45) NOT NULL,
  `testStatus` varchar(45) NOT NULL,
  `testScore` double NOT NULL,
  `testDate` date NOT NULL,
  `locationID` varchar(45) NOT NULL,
  `branchID` varchar(45) NOT NULL,
  `projectID` varchar(45) NOT NULL,
  `teamID` varchar(45) NOT NULL,
  `groupPathID` varchar(45) NOT NULL,
  KEY `fk_client_HISResult_idx` (`clientID`),
  KEY `fk_branchID_HISResult_idx` (`branchID`),
  KEY `fk_pojectID_HISResult_idx` (`projectID`),
  KEY `fk_teamID_HISResult_idx` (`teamID`),
  KEY `fk_groupPathID_HISResult_idx` (`groupPathID`),
  CONSTRAINT `fk_branchID_HISResult` FOREIGN KEY (`branchID`) REFERENCES `masterclientbranch` (`branchID`),
  CONSTRAINT `fk_client_HISResult` FOREIGN KEY (`clientID`) REFERENCES `masterclient` (`clientID`),
  CONSTRAINT `fk_groupPathID_HISResult` FOREIGN KEY (`groupPathID`) REFERENCES `mastergrouppath` (`groupPathID`),
  CONSTRAINT `fk_pojectID_HISResult` FOREIGN KEY (`projectID`) REFERENCES `masterproject` (`projectID`),
  CONSTRAINT `fk_teamID_HISResults` FOREIGN KEY (`teamID`) REFERENCES `masterteam` (`teamID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employeehealthindexscoretestresult`
--

LOCK TABLES `employeehealthindexscoretestresult` WRITE;
/*!40000 ALTER TABLE `employeehealthindexscoretestresult` DISABLE KEYS */;
INSERT INTO `employeehealthindexscoretestresult` VALUES ('Emp1','AD163P','Completed',55,'2019-06-29','553471','M742187','B1744','AT77S','IF51J'),('Emp2','AD163P','Completed',45,'2019-06-29','553471','M742187','B1744','AT77S','IF51J'),('Emp3','AD163P','Completed',30,'2019-06-29','553471','M742187','B1744','AT77S','IF51J'),('Emp4','AD163P','Completed',60,'2019-06-29','553471','M742187','B1744','AT77S','IF51J'),('Emp5','AD163P','Completed',56,'2019-06-29','553471','M742187','B1744','AT77S','IF51J'),('Emp6','AD163P','Completed',40,'2019-06-29','553471','M742187','B1744','AT77S','IF51J'),('Emp7','AD163P','Completed',65,'2019-06-29','553471','M742187','B1744','AT77S','IF51J'),('Emp8','AD163P','Completed',70,'2019-06-29','553471','M742187','B1744','AT77S','IF51J'),('Emp9','AD163P','Completed',63,'2019-06-29','553471','M742187','B1744','AT77S','IF51J'),('Emp10','AD163P','Completed',32,'2019-06-29','553471','M742187','B1744','AT77S','IF51J'),('Emp11','AD163P','Completed',54,'2019-06-29','553471','M742187','B1744','AT97I','ASP32K'),('Emp12','AD163P','Completed',40,'2019-06-29','553471','M742187','B1744','AT97I','ASP32K'),('Emp13','AD163P','Completed',42,'2019-06-29','553471','M742187','B1744','AT97I','ASP32K'),('Emp14','AD163P','Completed',50,'2019-06-29','553471','M742187','B1744','AT97I','ASP32K'),('Emp15','AD163P','Completed',45,'2019-06-29','553471','M742187','B1744','AT97I','ASP32K'),('Emp16','AD163P','Completed',62,'2019-06-29','553471','M742187','B1744','AT97I','ASP32K'),('Emp17','AD163P','Completed',48,'2019-06-29','553471','M742187','B1744','AT97I','ASP32K'),('Emp18','AD163P','Completed',49,'2019-06-29','553471','M742187','B1744','AT97I','ASP32K'),('Emp19','AD163P','Completed',46,'2019-06-29','553471','M742187','B1744','AT97I','ASP32K'),('Emp20','AD163P','Completed',43,'2019-06-29','553471','M742187','B1744','AT97I','ASP32K');
/*!40000 ALTER TABLE `employeehealthindexscoretestresult` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:45:57
