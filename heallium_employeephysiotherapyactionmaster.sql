-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employeephysiotherapyactionmaster`
--

DROP TABLE IF EXISTS `employeephysiotherapyactionmaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `employeephysiotherapyactionmaster` (
  `actionID` int(11) NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `description` varchar(45) NOT NULL,
  `duration` varchar(45) DEFAULT NULL,
  `plannerType` datetime DEFAULT NULL,
  `score` double DEFAULT NULL,
  `actionOptionID` varchar(45) NOT NULL,
  PRIMARY KEY (`actionOptionID`),
  KEY `fk_actionID_physiotheraphy_idx` (`actionID`),
  CONSTRAINT `fk_actionID_physiotheraphy` FOREIGN KEY (`actionID`) REFERENCES `employeephysiotheraphyactionmasterlist` (`actionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employeephysiotherapyactionmaster`
--

LOCK TABLES `employeephysiotherapyactionmaster` WRITE;
/*!40000 ALTER TABLE `employeephysiotherapyactionmaster` DISABLE KEYS */;
INSERT INTO `employeephysiotherapyactionmaster` VALUES (1,'null','Yes','null','0000-00-00 00:00:00',0.1,'1'),(5,'null','No','null','0000-00-00 00:00:00',0,'10'),(50,'null','No','null','0000-00-00 00:00:00',0,'100'),(51,'null','Yes','null','0000-00-00 00:00:00',0.1,'101'),(51,'null','No','null','0000-00-00 00:00:00',0,'102'),(52,'null','Yes','null','0000-00-00 00:00:00',0.1,'103'),(52,'null','No','null','0000-00-00 00:00:00',0,'104'),(53,'null','Yes','null','0000-00-00 00:00:00',0.1,'105'),(53,'null','No','null','0000-00-00 00:00:00',0,'106'),(54,'null','Yes','null','0000-00-00 00:00:00',0.1,'107'),(54,'null','No','null','0000-00-00 00:00:00',0,'108'),(55,'null','Yes','null','0000-00-00 00:00:00',0.1,'109'),(6,'null','Yes','null','0000-00-00 00:00:00',0.1,'11'),(55,'null','No','null','0000-00-00 00:00:00',0,'110'),(56,'null','Yes','null','0000-00-00 00:00:00',0.1,'111'),(56,'null','No','null','0000-00-00 00:00:00',0,'112'),(57,'null','Yes','null','0000-00-00 00:00:00',0.1,'113'),(57,'null','No','null','0000-00-00 00:00:00',0,'114'),(58,'null','Yes','null','0000-00-00 00:00:00',0.1,'115'),(58,'null','No','null','0000-00-00 00:00:00',0,'116'),(59,'null','Yes','null','0000-00-00 00:00:00',0.1,'117'),(59,'null','No','null','0000-00-00 00:00:00',0,'118'),(60,'null','Yes','null','0000-00-00 00:00:00',0.1,'119'),(6,'null','No','null','0000-00-00 00:00:00',0,'12'),(60,'null','No','null','0000-00-00 00:00:00',0,'120'),(7,'null','Yes','null','0000-00-00 00:00:00',0.1,'13'),(7,'null','No','null','0000-00-00 00:00:00',0,'14'),(8,'null','Yes','null','0000-00-00 00:00:00',0.1,'15'),(8,'null','No','null','0000-00-00 00:00:00',0,'16'),(9,'null','Yes','null','0000-00-00 00:00:00',0.1,'17'),(9,'null','No','null','0000-00-00 00:00:00',0,'18'),(10,'null','Yes','null','0000-00-00 00:00:00',0.1,'19'),(1,'null','No','null','0000-00-00 00:00:00',0,'2'),(10,'null','No','null','0000-00-00 00:00:00',0,'20'),(11,'null','Yes','null','0000-00-00 00:00:00',0.1,'21'),(11,'null','No','null','0000-00-00 00:00:00',0,'22'),(12,'null','Yes','null','0000-00-00 00:00:00',0.1,'23'),(12,'null','No','null','0000-00-00 00:00:00',0,'24'),(13,'null','Yes','null','0000-00-00 00:00:00',0.1,'25'),(13,'null','No','null','0000-00-00 00:00:00',0,'26'),(14,'null','Yes','null','0000-00-00 00:00:00',0.1,'27'),(14,'null','No','null','0000-00-00 00:00:00',0,'28'),(15,'null','Yes','null','0000-00-00 00:00:00',0.1,'29'),(2,'null','Yes','null','0000-00-00 00:00:00',0.1,'3'),(15,'null','No','null','0000-00-00 00:00:00',0,'30'),(16,'null','Yes','null','0000-00-00 00:00:00',0.1,'31'),(16,'null','No','null','0000-00-00 00:00:00',0,'32'),(17,'null','Yes','null','0000-00-00 00:00:00',0.1,'33'),(17,'null','No','null','0000-00-00 00:00:00',0,'34'),(18,'null','Yes','null','0000-00-00 00:00:00',0.1,'35'),(18,'null','No','null','0000-00-00 00:00:00',0,'36'),(19,'null','Yes','null','0000-00-00 00:00:00',0.1,'37'),(19,'null','No','null','0000-00-00 00:00:00',0,'38'),(20,'null','Yes','null','0000-00-00 00:00:00',0.1,'39'),(2,'null','No','null','0000-00-00 00:00:00',0,'4'),(20,'null','No','null','0000-00-00 00:00:00',0,'40'),(21,'null','Yes','null','0000-00-00 00:00:00',0.1,'41'),(21,'null','No','null','0000-00-00 00:00:00',0,'42'),(22,'null','Yes','null','0000-00-00 00:00:00',0.1,'43'),(22,'null','No','null','0000-00-00 00:00:00',0,'44'),(23,'null','Yes','null','0000-00-00 00:00:00',0.1,'45'),(23,'null','No','null','0000-00-00 00:00:00',0,'46'),(24,'null','Yes','null','0000-00-00 00:00:00',0.1,'47'),(24,'null','No','null','0000-00-00 00:00:00',0,'48'),(25,'null','Yes','null','0000-00-00 00:00:00',0.1,'49'),(3,'null','Yes','null','0000-00-00 00:00:00',0.1,'5'),(25,'null','No','null','0000-00-00 00:00:00',0,'50'),(26,'null','Yes','null','0000-00-00 00:00:00',0.1,'51'),(26,'null','No','null','0000-00-00 00:00:00',0,'52'),(27,'null','Yes','null','0000-00-00 00:00:00',0.1,'53'),(27,'null','No','null','0000-00-00 00:00:00',0,'54'),(28,'null','Yes','null','0000-00-00 00:00:00',0.1,'55'),(28,'null','No','null','0000-00-00 00:00:00',0,'56'),(29,'null','Yes','null','0000-00-00 00:00:00',0.1,'57'),(29,'null','No','null','0000-00-00 00:00:00',0,'58'),(30,'null','Yes','null','0000-00-00 00:00:00',0.1,'59'),(3,'null','No','null','0000-00-00 00:00:00',0,'6'),(30,'null','No','null','0000-00-00 00:00:00',0,'60'),(31,'null','Yes','null','0000-00-00 00:00:00',0.1,'61'),(31,'null','No','null','0000-00-00 00:00:00',0,'62'),(32,'null','Yes','null','0000-00-00 00:00:00',0.1,'63'),(32,'null','No','null','0000-00-00 00:00:00',0,'64'),(33,'null','Yes','null','0000-00-00 00:00:00',0.1,'65'),(33,'null','No','null','0000-00-00 00:00:00',0,'66'),(34,'null','Yes','null','0000-00-00 00:00:00',0.1,'67'),(34,'null','No','null','0000-00-00 00:00:00',0,'68'),(35,'null','Yes','null','0000-00-00 00:00:00',0.1,'69'),(4,'null','Yes','null','0000-00-00 00:00:00',0.1,'7'),(35,'null','No','null','0000-00-00 00:00:00',0,'70'),(36,'null','Yes','null','0000-00-00 00:00:00',0.1,'71'),(36,'null','No','null','0000-00-00 00:00:00',0,'72'),(37,'null','Yes','null','0000-00-00 00:00:00',0.1,'73'),(37,'null','No','null','0000-00-00 00:00:00',0,'74'),(38,'null','Yes','null','0000-00-00 00:00:00',0.1,'75'),(38,'null','No','null','0000-00-00 00:00:00',0,'76'),(39,'null','Yes','null','0000-00-00 00:00:00',0.1,'77'),(39,'null','No','null','0000-00-00 00:00:00',0,'78'),(40,'null','Yes','null','0000-00-00 00:00:00',0.1,'79'),(4,'null','No','null','0000-00-00 00:00:00',0,'8'),(40,'null','No','null','0000-00-00 00:00:00',0,'80'),(41,'null','Yes','null','0000-00-00 00:00:00',0.1,'81'),(41,'null','No','null','0000-00-00 00:00:00',0,'82'),(42,'null','Yes','null','0000-00-00 00:00:00',0.1,'83'),(42,'null','No','null','0000-00-00 00:00:00',0,'84'),(43,'null','Yes','null','0000-00-00 00:00:00',0.1,'85'),(43,'null','No','null','0000-00-00 00:00:00',0,'86'),(44,'null','Yes','null','0000-00-00 00:00:00',0.1,'87'),(44,'null','No','null','0000-00-00 00:00:00',0,'88'),(45,'null','Yes','null','0000-00-00 00:00:00',0.1,'89'),(5,'null','Yes','null','0000-00-00 00:00:00',0.1,'9'),(45,'null','No','null','0000-00-00 00:00:00',0,'90'),(46,'null','Yes','null','0000-00-00 00:00:00',0.1,'91'),(46,'null','No','null','0000-00-00 00:00:00',0,'92'),(47,'null','Yes','null','0000-00-00 00:00:00',0.1,'93'),(47,'null','No','null','0000-00-00 00:00:00',0,'94'),(48,'null','Yes','null','0000-00-00 00:00:00',0.1,'95'),(48,'null','No','null','0000-00-00 00:00:00',0,'96'),(49,'null','Yes','null','0000-00-00 00:00:00',0.1,'97'),(49,'null','No','null','0000-00-00 00:00:00',0,'98'),(50,'null','Yes','null','0000-00-00 00:00:00',0.1,'99');
/*!40000 ALTER TABLE `employeephysiotherapyactionmaster` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:45:51
