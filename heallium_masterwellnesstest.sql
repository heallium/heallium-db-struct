-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `masterwellnesstest`
--

DROP TABLE IF EXISTS `masterwellnesstest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `masterwellnesstest` (
  `wellNessTestID` varchar(45) NOT NULL,
  `wellNessTestQuestion` varchar(350) NOT NULL,
  `wellNessTestQuestionOption1` varchar(250) NOT NULL,
  `wellNessTestQuestionOption2` varchar(250) NOT NULL,
  `wellNessTestQuestionOption3` varchar(250) NOT NULL,
  `wellNessTestQuestionOption4` varchar(250) NOT NULL,
  `wellNessTestQuestionOption5` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`wellNessTestID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `masterwellnesstest`
--

LOCK TABLES `masterwellnesstest` WRITE;
/*!40000 ALTER TABLE `masterwellnesstest` DISABLE KEYS */;
INSERT INTO `masterwellnesstest` VALUES ('WTID1','The lowest comfort I would consider for sleeping is','Outside on the ground','A tent','An RV or camper','A motel','A bed and breakfast'),('WTID10','Even if I dont intend to, I raise my voice when I get upset','Most of the time','Often','Sometimes','Rarely','Almost never'),('WTID2','Having a daily routine is','Oppressive and stifling','Annoying and limiting','Sometimes a good thing, sometimes not','Helpful and comforting','Totally necessary'),('WTID3','The furthest you have traveled is outside of your','Zip code','Area code','Time zone','Hemisphere','Stratosphere'),('WTID4','In your life, being spontaneous meansOppressive and stifling','Wearing a different colored pair of socks, besides my standard black or white','Trying the special of the day instead of my usual','Buying a book or CD without knowing much about it','Making weekend travel plans on a Thursday night','Going to the airport with a suitcase and buy my ticket on-the-spot'),('WTID5','Its your birthday - party time! You want to spend it with','Me, myself, and I','My partner','A few close friends/family members','A big group of people','Everyone I know, and probably even a few people I dont know'),('WTID6','A friend mentions a new boot-camp class that opened up at the gym. The trainer is a former army sergeant, determined to make his \"troops\" sweat every pound of fat off. Your friend mentions a colleague whos done it - and got amazing results real quick. What would you do','Decline - I have my own butt-kicking routine anyway','Join the class - this is just the kind of push I need!','Decide to try it out, at least for a little while. I hope he doesnt make me cry','Refuse - Ill take the nice and steady treadmill, thank you','Tell my friend \"no\" as I shift position on the sofa. I already surf - channels, that is'),('WTID7','Youve come across a gap in the space/time continuum - who are you','An ancient philosopher','A Templar knight','An artist of the Renaissance','An explorer of the new world','A time traveler in the year 3000'),('WTID8','Youre going to a friends dinner party - and he or she always tries new foods from different cultures. You never quite know which country youre in for! You:','Decline the invitation altogether','Skip lunch - you know youre going to be eating a lot','Eat as usual and look forward to the meal','Have a little snack before heading over','Eat a full meal before going as a preventative measure'),('WTID9','When I realize that I was wrong, I admit it to my partner','Completely true','Mostlytrue','Somewhat true/false','Mostlyfalse','Completely false');
/*!40000 ALTER TABLE `masterwellnesstest` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:45:06
