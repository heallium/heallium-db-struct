-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: heallium
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `masterlabel`
--

DROP TABLE IF EXISTS `masterlabel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `masterlabel` (
  `languageID` varchar(45) NOT NULL,
  `labelName` varchar(500) DEFAULT NULL,
  `labelValue` varchar(500) DEFAULT NULL,
  `clientID` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `masterlabel`
--

LOCK TABLES `masterlabel` WRITE;
/*!40000 ALTER TABLE `masterlabel` DISABLE KEYS */;
INSERT INTO `masterlabel` VALUES ('en','_task','Task','AD163P'),('en','_stressTest','Stress Test','AD163P'),('en','_lifestyleTest','LifeStyle Test','AD163P'),('en','_physicalTest','Physical Test','AD163P'),('en','_wellnessTest','Wellness Test','AD163P'),('en','_appointment','Appointment For Annual Checkup','AD163P'),('en','_heallium','Heallium','AD163P'),('en','_rememberme','Remember Me','AD163P'),('en','_signin','Sign in','AD163P'),('en','_forgot_password','Forgot password','AD163P'),('en','_labs','Labs','AD163P'),('en','_apCenter','Appointment Centers','AD163P'),('en','_apDate','Appontment Date','AD163P'),('en','_bookAppointmenet','Book Appointment','AD163P'),('en','_book ','Book Appointment','AD163P'),('en','_timeSlots','TimeSlots','AD163P'),('en','_slot/Hour','Slots/Hour','AD163P'),('en','_chosseTimeSlot','Choose the timeslot to Book the appointment','AD163P'),('en','_apSchedule','Appointment Schedule','AD163P'),('en','_labName','LabName','AD163P'),('en','_apTime','AppointmentTime','AD163P'),('en','_done','Done','AD163P'),('en','_previous','Previous','AD163P'),('en','_saveForLater','Save for later','AD163P'),('en','_next','Next','AD163P'),('en','_ok','Ok','AD163P'),('en','_answerHaveBeenSaved','Answers have been saved','AD163P'),('en','_answerdSuccessfully','You have answered successfully and please proceed for next process','AD163P'),('en','_scores','Scores','AD163P'),('en','_reports','Reports','AD163P'),('en','_track','Track','AD163P'),('en','_leaderBoard','Leaderboard','AD163P'),('en','_testName','TestName','AD163P'),('en','_taskDetails','TaskDetails','AD163P'),('en','_overallRank','Overall rank','AD163P'),('en','_home','Home','AD163P'),('en','_profile','Profile','AD163P'),('en','_heallium','Heallium','AD163P'),('en','_myReports','My Reports','AD163P'),('en','_suggestions','Suggesrions','AD163P'),('en','_myPrescription','My Prescription','AD163P'),('en','_setting','Settings','AD163P'),('en','_supportAndFeedback','Support and feedback','AD163P'),('en','_logOut','Logout','AD163P'),('en','_AccountSeting','Account settings','AD163P'),('en','_profileSetting','Profile settings','AD163P'),('en','_NotificationSetting','Notification settings','AD163P'),('en','_name','Name','AD163P'),('en','_changePassword','Change password','AD163P'),('en','_language','Language','AD163P'),('en','_selectOne','Select one','AD163P'),('en','_cancle','Cancle','AD163P'),('en','_save','Save','AD163P'),('en','_firstName','First name','AD163P'),('en','_lastName','Last name','AD163P'),('en','_oldPassword','Old password','AD163P'),('en','_newPassword','New password','AD163P'),('en','_confirmPassword','Confirm password','AD163P'),('en','_blockNotification','Block notification','AD163P'),('en','_accessories','Accessories','AD163P'),('en','_suyncWithGoogleFit','Sync with Google-fit','AD163P'),('en','_myReports','MY Reports','AD163P'),('en','_myPrescription','My Prescription','AD163P'),('en','_suggestions','Suggestions','AD163P'),('en','_reminders','Reminders','AD163P'),('en','_myHealthRecord','My Health Record','AD163P'),('en','_notification','Notification','AD163P'),('en','_settings','Settings','AD163P'),('en','_support','Support','AD163P'),('en','_reportAnIssue','Report An Issue','AD163P'),('en','_aboutUs','About Us','AD163P'),('en','_account','Account','AD163P'),('en','_profile','Profile','AD163P'),('en','_notification','Notification','AD163P'),('en','_connectWith','Connect with','AD163P'),('en','_masterHealthCheckup','Master Health Checkup','AD163P');
/*!40000 ALTER TABLE `masterlabel` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 11:47:09
